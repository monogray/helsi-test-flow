<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/bootstrap.php';

// Start session end set default settings
session_start();
date_default_timezone_set('America/Los_Angeles');

// Define constants
define('APP_DIR', __DIR__);

// DB Connection
use RedBeanPHP\Facade as R;

if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    R::setup('mysql:host=127.0.0.1;dbname=auto_qa', 'root', '');
} else {
    R::setup('mysql:host=localhost;dbname=auto_qa', 'auto-qa', 'CmdjR28uaVeWWN4v');
}

R::freeze(TRUE);
R::ext('xdispense', function ($type) {
    return R::getRedBean()->dispense($type);
});

// Save cron log
$cronTask = R::xdispense('cron_tasks');
$cronTask->name = $_SERVER['REMOTE_ADDR'];
$cronTask->dCreate = date('Y-m-d H:i:s');
$cronTask->dUpdate = date('Y-m-d H:i:s');
R::store($cronTask);


