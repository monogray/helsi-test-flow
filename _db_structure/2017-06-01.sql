-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 01, 2017 at 02:13 PM
-- Server version: 5.7.17-log
-- PHP Version: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auto_qa`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `is_favorite` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `name`, `description`, `is_favorite`, `status`, `d_create`, `d_update`, `d_delete`) VALUES
(3, 'Helsi.pro', NULL, 0, 1, '2016-12-27 00:00:00', '2016-12-27 00:00:00', '0000-00-00 00:00:00'),
(4, 'Helsi.me', 'https://helsi.me/', 0, 1, '2016-12-27 00:00:00', '2017-04-13 18:47:46', '0000-00-00 00:00:00'),
(5, '25h8', NULL, 0, 1, '2017-04-18 00:00:00', '2017-04-18 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `app_runs`
--

CREATE TABLE `app_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `app_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED DEFAULT NULL,
  `samples` int(10) UNSIGNED NOT NULL,
  `average_time` decimal(10,0) UNSIGNED NOT NULL,
  `min_time` int(10) UNSIGNED NOT NULL,
  `max_time` int(10) UNSIGNED NOT NULL,
  `pass_count` int(10) UNSIGNED NOT NULL,
  `error_count` int(10) UNSIGNED NOT NULL,
  `error_percent` decimal(10,0) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `app_runs_to_runs`
--

CREATE TABLE `app_runs_to_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_run_id` int(10) UNSIGNED DEFAULT NULL,
  `run_id` int(10) UNSIGNED DEFAULT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `description`, `status`, `d_create`, `d_update`, `d_delete`) VALUES
(6, 'Helsi.ua', '', 1, '2016-12-27 13:57:53', '2016-12-27 13:57:53', '0000-00-00 00:00:00'),
(7, 'Helsi', '', 1, '2017-03-01 13:17:46', '2017-03-01 13:17:46', '0000-00-00 00:00:00'),
(8, 'Helsi', '', 1, '2017-03-01 13:19:04', '2017-03-01 13:19:04', '0000-00-00 00:00:00'),
(9, 'Helsi', '', 1, '2017-03-01 13:20:59', '2017-03-01 13:20:59', '0000-00-00 00:00:00'),
(10, 'Helsi', '', 1, '2017-03-01 13:44:09', '2017-03-01 13:44:09', '0000-00-00 00:00:00'),
(11, 'Helsi', '', 1, '2017-03-01 14:37:52', '2017-03-01 14:37:52', '0000-00-00 00:00:00'),
(12, 'Helsi', '', 1, '2017-03-17 08:14:09', '2017-03-17 08:14:09', '0000-00-00 00:00:00'),
(13, 'hels', '', 1, '2017-03-17 13:38:50', '2017-03-17 13:38:50', '0000-00-00 00:00:00'),
(14, 'StudyDive', '', 1, '2017-04-03 14:12:54', '2017-04-03 14:12:54', '0000-00-00 00:00:00'),
(15, '25h8', '', 1, '2017-04-13 10:54:58', '2017-04-13 10:54:58', '0000-00-00 00:00:00'),
(16, '25/8', '', 1, '2017-04-13 10:55:05', '2017-04-13 10:55:05', '0000-00-00 00:00:00'),
(17, '25h8', '', 1, '2017-04-13 10:55:24', '2017-04-13 10:55:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cron_tasks`
--

CREATE TABLE `cron_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `executors`
--

CREATE TABLE `executors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `ico` varchar(50) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `config` varchar(2000) DEFAULT NULL,
  `is_default` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `executors`
--

INSERT INTO `executors` (`id`, `name`, `ico`, `description`, `config`, `is_default`, `app_id`, `status`, `d_create`, `d_update`, `d_delete`) VALUES
(4, 'qa2.helsi.pro | Chrome', 'fa fa-chrome', '', 'C:\\Python27\\python.exe -m robot.run --console none --outputdir C:\\inetpub\\wwwroot\\automation.helsi.pro\\jmeter-src\\results\\robot\\${appId}\\${executorId}\\${currentDate} --variable \"BROWSER:Chrome\" --variable \"SERVER:https://qa2.helsi.pro\" --variable \"SERVERPIS:https://qa2.helsi.me\" ${tests}', 0, 3, 1, '2016-12-28 14:13:21', '2017-05-03 14:58:45', '0000-00-00 00:00:00'),
(5, 'staging.helsi.pro | Firefox', 'fa fa-firefox', '', 'C:\\Python27\\python.exe -m robot.run --outputdir C:\\inetpub\\wwwroot\\automation.helsi.pro\\jmeter-src\\results\\robot\\${appId}\\${executorId}\\${currentDate} --variable \"BROWSER:Firefox\" --variable \"SERVER:https://staging.helsi.pro\" C:\\Project\\helsi-autoqa-gui\\ims_mis\\acceptance_tests\\acceptance_1_test.robot C:\\Project\\helsi-autoqa-gui\\ims_mis\\acceptance_tests\\acceptance_2_test.robot', 0, 3, 0, '2016-12-28 14:26:22', '2017-02-16 11:12:53', '2017-02-16 11:31:55'),
(6, 'staging.helsi.pro | Firefox', 'fa fa-firefox', '', 'C:\\Python27\\python.exe -m robot.run --outputdir C:\\inetpub\\wwwroot\\automation.helsi.pro\\jmeter-src\\results\\robot\\${appId}\\${executorId}\\${currentDate} --variable \"BROWSER:Firefox\" --variable \"SERVER:https://staging.helsi.pro\"  ${tests}', 0, 3, 1, '2016-12-29 21:45:49', '2017-02-16 16:36:28', '0000-00-00 00:00:00'),
(7, 'staging.helsi.pro | Chrome', 'fa fa-chrome', '', 'C:\\Python27\\python.exe -m robot.run --console none --outputdir C:\\inetpub\\wwwroot\\automation.helsi.pro\\jmeter-src\\results\\robot\\${appId}\\${executorId}\\${currentDate} --variable \"BROWSER:Chrome\" --variable \"SERVER:https://staging.helsi.pro\" --variable \"SERVERPIS:https://staging.helsi.me\" ${tests}', 0, 3, 1, '2016-12-30 16:57:26', '2017-03-07 08:24:01', '0000-00-00 00:00:00'),
(8, 'staging.helsi.pro | IE', 'fa fa-internet-explorer', '', 'C:\\Python27\\python.exe -m robot.run --outputdir C:\\inetpub\\wwwroot\\automation.helsi.pro\\jmeter-src\\results\\robot\\${appId}\\${executorId}\\${currentDate} --variable \"BROWSER:IE\" --variable \"SERVER:https://staging.helsi.pro\"  ${tests}', 0, 3, 1, '2017-01-03 15:44:36', '2017-02-16 11:19:18', '0000-00-00 00:00:00'),
(9, 'New executor', '', NULL, NULL, 0, 3, 0, '2017-01-26 16:19:57', '2017-01-26 16:19:57', '2017-02-16 11:30:56'),
(10, 'qa.helsi.pro | Chrome', 'fa fa-chrome', '', 'C:\\Python27\\python.exe -m robot.run --console none --outputdir C:\\inetpub\\wwwroot\\automation.helsi.pro\\jmeter-src\\results\\robot\\${appId}\\${executorId}\\${currentDate} --variable \"BROWSER:Chrome\" --variable \"SERVER:https://qa.helsi.pro\" --variable \"SERVERPIS:https://qa.helsi.me\" ${tests}', 0, 3, 1, '2017-01-26 16:42:16', '2017-03-07 09:16:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `executors_runs`
--

CREATE TABLE `executors_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `exec_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `executors_runs_data`
--

CREATE TABLE `executors_runs_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `exec_run_id` int(10) UNSIGNED DEFAULT NULL,
  `run_config` text,
  `console_output` longtext,
  `report_path` varchar(255) DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `http_codes`
--

CREATE TABLE `http_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `http_methods`
--

CREATE TABLE `http_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `is_favorite` int(1) UNSIGNED DEFAULT '0',
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_stages`
--

CREATE TABLE `project_stages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `fields` text NOT NULL,
  `is_default` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `p_id` int(10) UNSIGNED DEFAULT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_stages`
--

INSERT INTO `project_stages` (`id`, `name`, `fields`, `is_default`, `p_id`, `app_id`, `status`, `d_create`, `d_update`, `d_delete`) VALUES
(23, 'stagingapi.helsi.pro', '{\"host\":\"stagingapi.helsi.pro\",\"api_host\":\"192.168.1.131\",\"api_version\":\"v1\",\"api_protocol\":\"https\",\"api_login\":\"i.trotsyuk+4@helsi.me\",\"api_pass\":\"Qwer12\",\"loop\":1,\"users\":1,\"ramp_up_period\":1,\"api_email\":\"ims.test.dep@gmail.com\",\"api_firstName\":\"Jack\",\"api_lastName\":\"Sparrow\",\"api_nameSchedule\":\"Schedule123\",\"api_timeStart\":\"18:30\",\"api_timeEnd\":\"18:00\",\"api_comment\":\"\\u043a\\u043e\\u043c\\u043c\\u0435\\u043d\\u0442\"}', 0, NULL, 3, 1, '2016-12-27 15:19:45', '2016-12-28 18:02:20', '0000-00-00 00:00:00'),
(24, 'New stage', '{\"host\":\"www.wikipedia.org\",\"search1\":\"qa\",\"tt\":10802,\"loop\":1,\"users\":1,\"ramp_up_period\":1}', 1, 16, NULL, 1, '2016-12-27 15:34:24', '2016-12-27 15:34:42', '0000-00-00 00:00:00'),
(25, 'stagingapi.helsi.me', '{\"host\":\"stagingapi.helsi.me\",\"api_port\":443,\"port\":80,\"api_host\":\"stagingapi.helsi.me\",\"api_version\":\"v1\",\"api_protocol\":\"https\",\"loop\":1,\"users\":1,\"ramp_up_period\":1}', 0, NULL, 4, 1, '2016-12-27 19:04:21', '2016-12-27 20:51:04', '0000-00-00 00:00:00'),
(26, 'staging.helsi.me', '{\"host\":\"stagingapi.helsi.me\",\"api_port\":443,\"port\":80,\"api_host\":\"stagingapi.helsi.me\",\"api_version\":\"v1\",\"api_protocol\":\"https\",\"loop\":1,\"users\":1,\"ramp_up_period\":1}', 1, 19, NULL, 1, '2016-12-27 20:28:13', '2016-12-27 20:28:36', '0000-00-00 00:00:00'),
(27, '192.168.1.131:10812', '{\"host\":\"192.168.1.131\",\"api_port\":10812,\"port\":80,\"api_host\":\"192.168.1.131\",\"api_version\":\"v1\",\"api_protocol\":\"http\",\"loop\":1,\"users\":1,\"ramp_up_period\":1}', 0, NULL, 4, 1, '2017-01-03 11:57:16', '2017-01-03 11:58:01', '0000-00-00 00:00:00'),
(28, 'qapi.helsi.me', '{\"host\":\"qapi.helsi.me\",\"api_port\":443,\"port\":80,\"api_host\":\"qapi.helsi.me\",\"api_version\":\"v1\",\"api_protocol\":\"https\",\"loop\":1,\"users\":1,\"ramp_up_period\":1}', 0, NULL, 4, 1, '2017-02-03 13:25:44', '2017-02-03 13:41:20', '0000-00-00 00:00:00'),
(29, 'New stage', '', 0, 23, NULL, 1, '2017-04-14 18:12:13', '2017-04-14 18:12:13', '0000-00-00 00:00:00'),
(30, 'stage.25h8.exchange', '{\"host\":\"stage.25h8.exchange\",\"protocol\":\"https\",\"login\":\"andrewlikesgood@gmail.com\",\"pass\":\"andrewqa2\"}', 0, NULL, 5, 1, '2017-04-18 11:35:26', '2017-04-18 12:24:36', '0000-00-00 00:00:00'),
(31, 'stage.25h8.exchange', '{\"host\":\"stage.25h8.exchange\",\"protocol\":\"https\",\"login\":\"andrewlikesgood@gmail.com\",\"pass\":\"andrewqa2\"}', 0, 24, NULL, 1, '2017-04-18 11:41:06', '2017-04-18 12:19:32', '0000-00-00 00:00:00'),
(32, 'stage.25h8.exchange', '{\"host\":\"stage.25h8.exchange\",\"protocol\":\"https\",\"login\":\"andrewlikesgood@gmail.com\",\"pass\":\"andrewqa2\"}', 0, 25, NULL, 1, '2017-04-18 11:42:00', '2017-04-18 12:19:44', '0000-00-00 00:00:00'),
(33, 'qapi.helsi.pro', '{\"host\":\"qapi.helsi.pro\",\"protocol\":\"https\",\"login\":\"galacan@ims.co.ua\",\"pass\":\"Asclepius1\",\"port\":443}', 0, 26, NULL, 1, '2017-04-18 15:47:18', '2017-04-18 15:47:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `runs`
--

CREATE TABLE `runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `p_id` int(10) UNSIGNED DEFAULT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `stage_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(10) UNSIGNED DEFAULT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `runs_data`
--

CREATE TABLE `runs_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `r_id` int(10) UNSIGNED NOT NULL,
  `exec` text,
  `console_output` mediumtext,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `runs_samples`
--

CREATE TABLE `runs_samples` (
  `id` int(10) UNSIGNED NOT NULL,
  `summ_report_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(500) NOT NULL,
  `method` int(10) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `request_body` longtext NOT NULL,
  `request_headers` mediumtext NOT NULL,
  `response_code` int(10) UNSIGNED NOT NULL,
  `response_body` longtext NOT NULL,
  `response_headers` mediumtext NOT NULL,
  `error_message` mediumtext NOT NULL,
  `response_time` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `runs_statuses`
--

CREATE TABLE `runs_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `runs_summary_reports`
--

CREATE TABLE `runs_summary_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `r_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(500) NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `samples` int(10) UNSIGNED NOT NULL,
  `average_time` decimal(10,0) UNSIGNED NOT NULL,
  `min_time` int(10) UNSIGNED NOT NULL,
  `max_time` int(10) UNSIGNED NOT NULL,
  `error_count` int(11) UNSIGNED NOT NULL,
  `error_percent` decimal(10,0) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `login` varchar(255) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `is_verified` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `token` varchar(255) NOT NULL,
  `token_expired` datetime NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `login`, `description`, `email`, `password`, `company_id`, `is_verified`, `token`, `token_expired`, `status`, `d_create`, `d_update`, `d_delete`) VALUES
(4, 'Eugene Gubich', 'e.gubich', '', 'e.gubich@helsi.me', '751f599ab487fc0dcfbdb980b5558166', 6, 1, 'd55c2db41683d4e8ad206856a0ceebb7', '2016-12-27 13:57:53', 1, '2016-12-27 13:57:53', '2016-12-27 13:57:53', '0000-00-00 00:00:00'),
(5, 'Oleh Hordienko', 'o.hordienko', '', 'hordienko.o@helsi.me', 'e1730bbb905ab20e90e5dfba19b3c072', 6, 1, '0f086538c01d82502862a43c064f87fb', '2016-12-28 13:38:12', 1, '2016-12-28 13:38:12', '2016-12-28 13:38:12', '0000-00-00 00:00:00'),
(6, 'Anton Lyubovskoy', 'a.lyubovskoy', '', 'lyubovskoy@helsi.me', '18964f5bcd3df3ee91950b2dd59a488c', 6, 0, '6c5c7bd43d849aabe988056f939dfeb8', '2016-12-29 11:00:48', 1, '2016-12-29 11:00:48', '2016-12-29 11:00:48', '0000-00-00 00:00:00'),
(7, 'Yura Roza', 'y.roza', '', 'roza.y@helsi.me', '5b0d07ec30a9c6416e519917a836b4e6', 6, 1, '725048debb5039366506153c0d2d1057', '2016-12-30 08:37:15', 1, '2016-12-30 08:37:15', '2016-12-30 08:37:15', '0000-00-00 00:00:00'),
(8, 'Oksana', 'o.tanasiychuk', '', 'o.tanasiychuk@helsi.me', '0826432d53bf37dc2b7becfde6da93b7', 6, 1, '7279920d1cabed8707be43c8a34207af', '2016-12-30 10:29:33', 1, '2016-12-30 10:29:33', '2016-12-30 10:29:33', '0000-00-00 00:00:00'),
(9, 'Nataly', 'n.pytsko', '', 'n.pytsko@helsi.me', '76506502a1988a86c8630a15b7db0ade', 6, 1, '1e859b08c2f52ef6ec501691f2f46f2a', '2016-12-30 10:45:43', 1, '2016-12-30 10:45:43', '2016-12-30 10:45:43', '0000-00-00 00:00:00'),
(10, 'Ivanna Trotsyuk', 'i.trotsyuk', '', 'i.trotsyuk@helsi.me', '0e62868acedc97b0b907f06dcd133baa', 6, 1, '36a99ae2659d49e9d80a8d29c135cb9c', '2016-12-30 10:55:17', 1, '2016-12-30 10:55:17', '2016-12-30 10:55:17', '0000-00-00 00:00:00'),
(11, 'Andrey Minchenko', 'a.minchenko', '', 'a.minchenko@helsi.ua', '6c4486aa357e8ea418a1e43de7a79508', 7, 1, '2f9d4faa770b1c520f44e17f169666da', '2017-03-01 13:17:46', 1, '2017-03-01 13:17:46', '2017-03-01 13:17:46', '0000-00-00 00:00:00'),
(12, 'Sergii', 's.sidoruk', '', 's.sidoruk@helsi.me', '89868b56f37136b411e560f2aaa92579', 8, 1, 'f6a6b103ef47102d02f2baa530413f4f', '2017-03-01 13:19:04', 1, '2017-03-01 13:19:04', '2017-03-01 13:19:04', '0000-00-00 00:00:00'),
(13, 'Anastasia', 'a.kalyuchenko', '', 'a.kalyuchenko@helsi.me', '15c78930ba0dbfa072ca6ad18e5ad1a9', 9, 1, '63247ddf9c839d33822891025f54f0fe', '2017-03-01 13:20:59', 1, '2017-03-01 13:20:59', '2017-03-01 13:20:59', '0000-00-00 00:00:00'),
(14, 'Yuriy', 'yuriy.akht', '', 'yuriy.akht@helsi.me', '9613b41cc9063949fd3f73ed951a3bfc', 10, 1, '880ea6c3837c01f80070c81a31edd523', '2017-03-01 13:44:09', 1, '2017-03-01 13:44:09', '2017-03-01 13:44:09', '0000-00-00 00:00:00'),
(15, 'Vladimir', 'vladimir.akht', '', 'vladimir.akht.ims@gmail.com', '7fb0150302fbcd7c32e73317b7d2eac3', 11, 1, 'a645093b602f49a8718c0c4620624b26', '2017-03-01 14:37:52', 1, '2017-03-01 14:37:52', '2017-03-01 14:37:52', '0000-00-00 00:00:00'),
(16, 'ShwetsAndrey', 'a.shwets', '', 'shwetsandrey.ims@gmail.com', '5a3af073dd4f00b0b54cc0271d47f9d6', 12, 1, '2b349e1995451c951830a6b97affcbb2', '2017-03-17 08:14:09', 1, '2017-03-17 08:14:09', '2017-03-17 08:14:09', '0000-00-00 00:00:00'),
(17, 'Oleg', 'o.avgustinov', '', 'o.avgustinov@helsi.me', '2d2df251259e723d6ac2829f408405ef', 13, 1, 'f2f87594cdbfbe27d7f3a2e65ec89924', '2017-03-17 13:38:50', 1, '2017-03-17 13:38:50', '2017-03-17 13:38:50', '0000-00-00 00:00:00'),
(18, 'StudyDive', 'a.taranenko', '', 'a.taranenko@studydive.com', '51e2ccbe175c7854bf2e60f694046e77', 14, 1, '27cfda929dce822d4cf95c3cb15bf74e', '2017-04-03 14:12:54', 1, '2017-04-03 14:12:54', '2017-04-03 14:12:54', '0000-00-00 00:00:00'),
(19, 'Andrew', 'andrewlikesgood', '', 'andrewlikesgood@gmail.com', 'dd03e7a05e6a07a5aaaaa049adc3414b', 15, 1, '3a00adcef8fa73ed5a522e9c2e5a707a', '2017-04-13 10:54:58', 1, '2017-04-13 10:54:58', '2017-04-13 10:54:58', '0000-00-00 00:00:00'),
(20, 'Olga', 'ol.zelenskaya', '', 'ol.zelenskaya77@gmail.com', 'ac524d7b876e7db6019f2939f2b5f92b', 16, 1, '688b703e64396c39d4de9500b75f1afa', '2017-04-13 10:55:05', 1, '2017-04-13 10:55:05', '2017-04-13 10:55:05', '0000-00-00 00:00:00'),
(21, 'Svitlana', 'svitlana.sku', '', 'svitlana.sku@gmail.com', 'e71380e8a96eb365dfdab0546b1916ce', 17, 1, '28f6d6a52a263bf1995857787588d0ca', '2017-04-13 10:55:24', 1, '2017-04-13 10:55:24', '2017-04-13 10:55:24', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_runs`
--
ALTER TABLE `app_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`stage_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`app_run_id`),
  ADD KEY `app_id` (`run_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cron_tasks`
--
ALTER TABLE `cron_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `executors`
--
ALTER TABLE `executors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `executors_runs`
--
ALTER TABLE `executors_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`exec_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`exec_run_id`);

--
-- Indexes for table `http_codes`
--
ALTER TABLE `http_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `http_methods`
--
ALTER TABLE `http_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `project_stages`
--
ALTER TABLE `project_stages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `runs`
--
ALTER TABLE `runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`stage_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `runs_data`
--
ALTER TABLE `runs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_id` (`r_id`);

--
-- Indexes for table `runs_samples`
--
ALTER TABLE `runs_samples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `summ_report_id` (`summ_report_id`),
  ADD KEY `method` (`method`),
  ADD KEY `status` (`status`),
  ADD KEY `response_code` (`response_code`);

--
-- Indexes for table `runs_statuses`
--
ALTER TABLE `runs_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `r_id` (`r_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `app_runs`
--
ALTER TABLE `app_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `cron_tasks`
--
ALTER TABLE `cron_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `executors`
--
ALTER TABLE `executors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `executors_runs`
--
ALTER TABLE `executors_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=417;
--
-- AUTO_INCREMENT for table `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=404;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `project_stages`
--
ALTER TABLE `project_stages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `runs`
--
ALTER TABLE `runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;
--
-- AUTO_INCREMENT for table `runs_data`
--
ALTER TABLE `runs_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;
--
-- AUTO_INCREMENT for table `runs_samples`
--
ALTER TABLE `runs_samples`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2575;
--
-- AUTO_INCREMENT for table `runs_statuses`
--
ALTER TABLE `runs_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1339;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `app_runs`
--
ALTER TABLE `app_runs`
  ADD CONSTRAINT `app_runs_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `app_runs_ibfk_2` FOREIGN KEY (`stage_id`) REFERENCES `project_stages` (`id`);

--
-- Constraints for table `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  ADD CONSTRAINT `app_runs_to_runs_ibfk_1` FOREIGN KEY (`app_run_id`) REFERENCES `app_runs` (`id`),
  ADD CONSTRAINT `app_runs_to_runs_ibfk_2` FOREIGN KEY (`run_id`) REFERENCES `runs` (`id`);

--
-- Constraints for table `executors`
--
ALTER TABLE `executors`
  ADD CONSTRAINT `executors_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Constraints for table `executors_runs`
--
ALTER TABLE `executors_runs`
  ADD CONSTRAINT `executors_runs_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `executors_runs_ibfk_2` FOREIGN KEY (`exec_id`) REFERENCES `executors` (`id`);

--
-- Constraints for table `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  ADD CONSTRAINT `executors_runs_data_ibfk_1` FOREIGN KEY (`exec_run_id`) REFERENCES `executors_runs` (`id`);

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Constraints for table `project_stages`
--
ALTER TABLE `project_stages`
  ADD CONSTRAINT `project_stages_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `project_stages_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Constraints for table `runs`
--
ALTER TABLE `runs`
  ADD CONSTRAINT `runs_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `runs_ibfk_2` FOREIGN KEY (`stage_id`) REFERENCES `project_stages` (`id`),
  ADD CONSTRAINT `runs_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Constraints for table `runs_data`
--
ALTER TABLE `runs_data`
  ADD CONSTRAINT `runs_data_ibfk_1` FOREIGN KEY (`r_id`) REFERENCES `runs` (`id`);

--
-- Constraints for table `runs_samples`
--
ALTER TABLE `runs_samples`
  ADD CONSTRAINT `runs_samples_ibfk_1` FOREIGN KEY (`summ_report_id`) REFERENCES `runs_summary_reports` (`id`),
  ADD CONSTRAINT `runs_samples_ibfk_2` FOREIGN KEY (`status`) REFERENCES `runs_statuses` (`id`);

--
-- Constraints for table `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  ADD CONSTRAINT `runs_summary_reports_ibfk_2` FOREIGN KEY (`status`) REFERENCES `runs_statuses` (`id`),
  ADD CONSTRAINT `runs_summary_reports_ibfk_3` FOREIGN KEY (`r_id`) REFERENCES `runs` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
