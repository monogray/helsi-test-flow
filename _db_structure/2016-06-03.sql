-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июн 03 2016 г., 09:43
-- Версия сервера: 5.5.40
-- Версия PHP: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `auto_qa`
--

-- --------------------------------------------------------

--
-- Структура таблицы `af_environments`
--

CREATE TABLE `af_environments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `p_id` int(10) UNSIGNED NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `is_primary` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_global_variables`
--

CREATE TABLE `af_global_variables` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `p_id` int(10) UNSIGNED NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_post_processors_types`
--

CREATE TABLE `af_post_processors_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `af_post_processors_types`
--

INSERT INTO `af_post_processors_types` (`id`, `name`, `description`, `status`, `d_create`, `d_update`, `d_delete`) VALUES
(1, 'JSON Schema validator', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'JSON Data validator', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `af_projects`
--

CREATE TABLE `af_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `preview` varchar(800) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `is_favorite` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_sampler_http_requests`
--

CREATE TABLE `af_sampler_http_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `t_id` int(10) UNSIGNED NOT NULL COMMENT 'Test id',
  `method_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(2000) NOT NULL,
  `headers` mediumtext NOT NULL,
  `body` longtext NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_tests`
--

CREATE TABLE `af_tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `fold_id` int(10) UNSIGNED NOT NULL COMMENT 'Folder id',
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `test_type` int(10) UNSIGNED NOT NULL,
  `ord` int(10) UNSIGNED NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_test_post_processors`
--

CREATE TABLE `af_test_post_processors` (
  `id` int(10) UNSIGNED NOT NULL,
  `t_id` int(10) UNSIGNED NOT NULL COMMENT 'Test id',
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `post_processor_type` int(10) UNSIGNED NOT NULL,
  `data` longtext NOT NULL,
  `ord` int(10) UNSIGNED NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_test_suites`
--

CREATE TABLE `af_test_suites` (
  `id` int(10) UNSIGNED NOT NULL,
  `p_id` int(10) UNSIGNED NOT NULL COMMENT 'Project id',
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `ord` int(10) UNSIGNED NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_test_suite_folders`
--

CREATE TABLE `af_test_suite_folders` (
  `id` int(10) UNSIGNED NOT NULL,
  `suite_id` int(10) UNSIGNED NOT NULL COMMENT 'Test suite id',
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `ord` int(10) UNSIGNED NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_test_types`
--

CREATE TABLE `af_test_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `af_test_types`
--

INSERT INTO `af_test_types` (`id`, `name`, `description`, `status`, `d_create`, `d_update`, `d_delete`) VALUES
(1, 'Http Request', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Http Default Request', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `http_codes`
--

CREATE TABLE `http_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `http_methods`
--

CREATE TABLE `http_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `http_methods`
--

INSERT INTO `http_methods` (`id`, `name`, `description`, `d_create`, `d_update`, `d_delete`) VALUES
(1, 'GET', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'POST', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `is_favorite` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `project_stages`
--

CREATE TABLE `project_stages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `fields` text NOT NULL,
  `is_default` int(1) UNSIGNED NOT NULL,
  `p_id` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `runs`
--

CREATE TABLE `runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `p_id` int(10) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `runs_data`
--

CREATE TABLE `runs_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `r_id` int(10) UNSIGNED NOT NULL,
  `exec` text NOT NULL,
  `console_output` mediumtext NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `runs_samples`
--

CREATE TABLE `runs_samples` (
  `id` int(10) UNSIGNED NOT NULL,
  `summ_report_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(500) NOT NULL,
  `method` int(10) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `request_body` longtext NOT NULL,
  `request_headers` mediumtext NOT NULL,
  `response_code` int(10) UNSIGNED NOT NULL,
  `response_body` longtext NOT NULL,
  `response_headers` mediumtext NOT NULL,
  `error_message` mediumtext NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `runs_statuses`
--

CREATE TABLE `runs_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `runs_statuses`
--

INSERT INTO `runs_statuses` (`id`, `name`, `description`, `d_create`, `d_update`, `d_delete`) VALUES
(1, 'pass', '', '2016-05-16 00:00:00', '2016-05-16 00:00:00', '0000-00-00 00:00:00'),
(2, 'fail', '', '2016-05-16 00:00:00', '2016-05-16 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `runs_summary_reports`
--

CREATE TABLE `runs_summary_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `r_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(500) NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `samples` int(10) UNSIGNED NOT NULL,
  `average_time` decimal(10,0) UNSIGNED NOT NULL,
  `min_time` int(10) UNSIGNED NOT NULL,
  `max_time` int(10) UNSIGNED NOT NULL,
  `error_percent` decimal(10,0) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `af_environments`
--
ALTER TABLE `af_environments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`);

--
-- Индексы таблицы `af_global_variables`
--
ALTER TABLE `af_global_variables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`);

--
-- Индексы таблицы `af_post_processors_types`
--
ALTER TABLE `af_post_processors_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `af_projects`
--
ALTER TABLE `af_projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `af_sampler_http_requests`
--
ALTER TABLE `af_sampler_http_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`t_id`),
  ADD KEY `method` (`method_id`);

--
-- Индексы таблицы `af_tests`
--
ALTER TABLE `af_tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`fold_id`),
  ADD KEY `test_type` (`test_type`);

--
-- Индексы таблицы `af_test_post_processors`
--
ALTER TABLE `af_test_post_processors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`t_id`),
  ADD KEY `test_type` (`post_processor_type`);

--
-- Индексы таблицы `af_test_suites`
--
ALTER TABLE `af_test_suites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`);

--
-- Индексы таблицы `af_test_suite_folders`
--
ALTER TABLE `af_test_suite_folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`suite_id`);

--
-- Индексы таблицы `af_test_types`
--
ALTER TABLE `af_test_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `http_codes`
--
ALTER TABLE `http_codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `http_methods`
--
ALTER TABLE `http_methods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `project_stages`
--
ALTER TABLE `project_stages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`);

--
-- Индексы таблицы `runs`
--
ALTER TABLE `runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `runs_data`
--
ALTER TABLE `runs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_id` (`r_id`);

--
-- Индексы таблицы `runs_samples`
--
ALTER TABLE `runs_samples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `summ_report_id` (`summ_report_id`),
  ADD KEY `method` (`method`),
  ADD KEY `status` (`status`),
  ADD KEY `response_code` (`response_code`);

--
-- Индексы таблицы `runs_statuses`
--
ALTER TABLE `runs_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `r_id` (`r_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `af_environments`
--
ALTER TABLE `af_environments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `af_global_variables`
--
ALTER TABLE `af_global_variables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `af_post_processors_types`
--
ALTER TABLE `af_post_processors_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `af_projects`
--
ALTER TABLE `af_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `af_sampler_http_requests`
--
ALTER TABLE `af_sampler_http_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `af_tests`
--
ALTER TABLE `af_tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `af_test_post_processors`
--
ALTER TABLE `af_test_post_processors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `af_test_suites`
--
ALTER TABLE `af_test_suites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `af_test_suite_folders`
--
ALTER TABLE `af_test_suite_folders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT для таблицы `af_test_types`
--
ALTER TABLE `af_test_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `project_stages`
--
ALTER TABLE `project_stages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `runs`
--
ALTER TABLE `runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `runs_data`
--
ALTER TABLE `runs_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `runs_samples`
--
ALTER TABLE `runs_samples`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=280;
--
-- AUTO_INCREMENT для таблицы `runs_statuses`
--
ALTER TABLE `runs_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `af_environments`
--
ALTER TABLE `af_environments`
  ADD CONSTRAINT `af_environments_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `af_projects` (`id`);

--
-- Ограничения внешнего ключа таблицы `af_global_variables`
--
ALTER TABLE `af_global_variables`
  ADD CONSTRAINT `af_global_variables_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `af_projects` (`id`);

--
-- Ограничения внешнего ключа таблицы `af_sampler_http_requests`
--
ALTER TABLE `af_sampler_http_requests`
  ADD CONSTRAINT `af_sampler_http_requests_ibfk_2` FOREIGN KEY (`method_id`) REFERENCES `http_methods` (`id`),
  ADD CONSTRAINT `af_sampler_http_requests_ibfk_1` FOREIGN KEY (`t_id`) REFERENCES `af_tests` (`id`);

--
-- Ограничения внешнего ключа таблицы `af_tests`
--
ALTER TABLE `af_tests`
  ADD CONSTRAINT `af_tests_ibfk_1` FOREIGN KEY (`fold_id`) REFERENCES `af_test_suite_folders` (`id`),
  ADD CONSTRAINT `af_tests_ibfk_2` FOREIGN KEY (`test_type`) REFERENCES `af_test_types` (`id`);

--
-- Ограничения внешнего ключа таблицы `af_test_post_processors`
--
ALTER TABLE `af_test_post_processors`
  ADD CONSTRAINT `af_test_post_processors_ibfk_3` FOREIGN KEY (`t_id`) REFERENCES `af_tests` (`id`),
  ADD CONSTRAINT `af_test_post_processors_ibfk_2` FOREIGN KEY (`post_processor_type`) REFERENCES `af_post_processors_types` (`id`);

--
-- Ограничения внешнего ключа таблицы `af_test_suites`
--
ALTER TABLE `af_test_suites`
  ADD CONSTRAINT `af_test_suites_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `af_projects` (`id`);

--
-- Ограничения внешнего ключа таблицы `af_test_suite_folders`
--
ALTER TABLE `af_test_suite_folders`
  ADD CONSTRAINT `af_test_suite_folders_ibfk_1` FOREIGN KEY (`suite_id`) REFERENCES `af_test_suites` (`id`);

--
-- Ограничения внешнего ключа таблицы `project_stages`
--
ALTER TABLE `project_stages`
  ADD CONSTRAINT `project_stages_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`id`);

--
-- Ограничения внешнего ключа таблицы `runs`
--
ALTER TABLE `runs`
  ADD CONSTRAINT `runs_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`id`);

--
-- Ограничения внешнего ключа таблицы `runs_data`
--
ALTER TABLE `runs_data`
  ADD CONSTRAINT `runs_data_ibfk_1` FOREIGN KEY (`r_id`) REFERENCES `runs` (`id`);

--
-- Ограничения внешнего ключа таблицы `runs_samples`
--
ALTER TABLE `runs_samples`
  ADD CONSTRAINT `runs_samples_ibfk_1` FOREIGN KEY (`summ_report_id`) REFERENCES `runs_summary_reports` (`id`),
  ADD CONSTRAINT `runs_samples_ibfk_2` FOREIGN KEY (`status`) REFERENCES `runs_statuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  ADD CONSTRAINT `runs_summary_reports_ibfk_2` FOREIGN KEY (`status`) REFERENCES `runs_statuses` (`id`),
  ADD CONSTRAINT `runs_summary_reports_ibfk_3` FOREIGN KEY (`r_id`) REFERENCES `runs` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
