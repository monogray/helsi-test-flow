-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 31, 2017 at 05:37 PM
-- Server version: 5.7.17-log
-- PHP Version: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auto_qa`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `is_favorite` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `app_runs`
--

CREATE TABLE `app_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `app_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED DEFAULT NULL,
  `samples` int(10) UNSIGNED NOT NULL,
  `average_time` decimal(10,0) UNSIGNED NOT NULL,
  `min_time` int(10) UNSIGNED NOT NULL,
  `max_time` int(10) UNSIGNED NOT NULL,
  `pass_count` int(10) UNSIGNED NOT NULL,
  `error_count` int(10) UNSIGNED NOT NULL,
  `error_percent` decimal(10,0) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `app_runs_to_runs`
--

CREATE TABLE `app_runs_to_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_run_id` int(10) UNSIGNED DEFAULT NULL,
  `run_id` int(10) UNSIGNED DEFAULT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cron_tasks`
--

CREATE TABLE `cron_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `executors`
--

CREATE TABLE `executors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `config` varchar(2000) DEFAULT NULL,
  `is_default` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `executors_runs`
--

CREATE TABLE `executors_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `exec_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `executors_runs_data`
--

CREATE TABLE `executors_runs_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `exec_run_id` int(10) UNSIGNED DEFAULT NULL,
  `run_config` text,
  `console_output` longtext,
  `report_path` varchar(255) DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `http_codes`
--

CREATE TABLE `http_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `http_methods`
--

CREATE TABLE `http_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `is_favorite` int(1) UNSIGNED DEFAULT '0',
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_stages`
--

CREATE TABLE `project_stages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `fields` text NOT NULL,
  `is_default` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `p_id` int(10) UNSIGNED DEFAULT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `runs`
--

CREATE TABLE `runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `p_id` int(10) UNSIGNED DEFAULT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `stage_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(10) UNSIGNED DEFAULT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `runs_data`
--

CREATE TABLE `runs_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `r_id` int(10) UNSIGNED NOT NULL,
  `exec` text,
  `console_output` mediumtext,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `runs_samples`
--

CREATE TABLE `runs_samples` (
  `id` int(10) UNSIGNED NOT NULL,
  `summ_report_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(500) NOT NULL,
  `method` int(10) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `request_body` longtext NOT NULL,
  `request_headers` mediumtext NOT NULL,
  `response_code` int(10) UNSIGNED NOT NULL,
  `response_body` longtext NOT NULL,
  `response_headers` mediumtext NOT NULL,
  `error_message` mediumtext NOT NULL,
  `response_time` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `runs_statuses`
--

CREATE TABLE `runs_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `runs_statuses`
--

INSERT INTO `runs_statuses` (`id`, `name`, `description`, `d_create`, `d_update`, `d_delete`) VALUES
(1, 'pass', '', '2016-05-16 00:00:00', '2016-05-16 00:00:00', '0000-00-00 00:00:00'),
(2, 'fail', '', '2016-05-16 00:00:00', '2016-05-16 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `runs_summary_reports`
--

CREATE TABLE `runs_summary_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `r_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(500) NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `samples` int(10) UNSIGNED NOT NULL,
  `average_time` decimal(10,0) UNSIGNED NOT NULL,
  `min_time` int(10) UNSIGNED NOT NULL,
  `max_time` int(10) UNSIGNED NOT NULL,
  `error_count` int(11) UNSIGNED NOT NULL,
  `error_percent` decimal(10,0) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `login` varchar(255) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `is_verified` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `token` varchar(255) NOT NULL,
  `token_expired` datetime NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_runs`
--
ALTER TABLE `app_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`stage_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`app_run_id`),
  ADD KEY `app_id` (`run_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cron_tasks`
--
ALTER TABLE `cron_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `executors`
--
ALTER TABLE `executors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `executors_runs`
--
ALTER TABLE `executors_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`exec_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`exec_run_id`);

--
-- Indexes for table `http_codes`
--
ALTER TABLE `http_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `http_methods`
--
ALTER TABLE `http_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `project_stages`
--
ALTER TABLE `project_stages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `runs`
--
ALTER TABLE `runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`stage_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `runs_data`
--
ALTER TABLE `runs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_id` (`r_id`);

--
-- Indexes for table `runs_samples`
--
ALTER TABLE `runs_samples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `summ_report_id` (`summ_report_id`),
  ADD KEY `method` (`method`),
  ADD KEY `status` (`status`),
  ADD KEY `response_code` (`response_code`);

--
-- Indexes for table `runs_statuses`
--
ALTER TABLE `runs_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `r_id` (`r_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `app_runs`
--
ALTER TABLE `app_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cron_tasks`
--
ALTER TABLE `cron_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `executors`
--
ALTER TABLE `executors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `executors_runs`
--
ALTER TABLE `executors_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `project_stages`
--
ALTER TABLE `project_stages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `runs`
--
ALTER TABLE `runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;
--
-- AUTO_INCREMENT for table `runs_data`
--
ALTER TABLE `runs_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT for table `runs_samples`
--
ALTER TABLE `runs_samples`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2337;
--
-- AUTO_INCREMENT for table `runs_statuses`
--
ALTER TABLE `runs_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1183;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `app_runs`
--
ALTER TABLE `app_runs`
  ADD CONSTRAINT `app_runs_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `app_runs_ibfk_2` FOREIGN KEY (`stage_id`) REFERENCES `project_stages` (`id`);

--
-- Constraints for table `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  ADD CONSTRAINT `app_runs_to_runs_ibfk_1` FOREIGN KEY (`app_run_id`) REFERENCES `app_runs` (`id`),
  ADD CONSTRAINT `app_runs_to_runs_ibfk_2` FOREIGN KEY (`run_id`) REFERENCES `runs` (`id`);

--
-- Constraints for table `executors`
--
ALTER TABLE `executors`
  ADD CONSTRAINT `executors_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Constraints for table `executors_runs`
--
ALTER TABLE `executors_runs`
  ADD CONSTRAINT `executors_runs_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `executors_runs_ibfk_2` FOREIGN KEY (`exec_id`) REFERENCES `executors` (`id`);

--
-- Constraints for table `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  ADD CONSTRAINT `executors_runs_data_ibfk_1` FOREIGN KEY (`exec_run_id`) REFERENCES `executors_runs` (`id`);

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Constraints for table `project_stages`
--
ALTER TABLE `project_stages`
  ADD CONSTRAINT `project_stages_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `project_stages_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Constraints for table `runs`
--
ALTER TABLE `runs`
  ADD CONSTRAINT `runs_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `runs_ibfk_2` FOREIGN KEY (`stage_id`) REFERENCES `project_stages` (`id`),
  ADD CONSTRAINT `runs_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Constraints for table `runs_data`
--
ALTER TABLE `runs_data`
  ADD CONSTRAINT `runs_data_ibfk_1` FOREIGN KEY (`r_id`) REFERENCES `runs` (`id`);

--
-- Constraints for table `runs_samples`
--
ALTER TABLE `runs_samples`
  ADD CONSTRAINT `runs_samples_ibfk_1` FOREIGN KEY (`summ_report_id`) REFERENCES `runs_summary_reports` (`id`),
  ADD CONSTRAINT `runs_samples_ibfk_2` FOREIGN KEY (`status`) REFERENCES `runs_statuses` (`id`);

--
-- Constraints for table `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  ADD CONSTRAINT `runs_summary_reports_ibfk_2` FOREIGN KEY (`status`) REFERENCES `runs_statuses` (`id`),
  ADD CONSTRAINT `runs_summary_reports_ibfk_3` FOREIGN KEY (`r_id`) REFERENCES `runs` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
