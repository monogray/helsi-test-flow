-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 27 2016 г., 13:47
-- Версия сервера: 5.5.40
-- Версия PHP: 5.6.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `auto_qa`
--

-- --------------------------------------------------------

--
-- Структура таблицы `applications`
--

CREATE TABLE `applications` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `is_favorite` int(1) UNSIGNED NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `app_runs`
--

CREATE TABLE `app_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `app_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED DEFAULT NULL,
  `samples` int(10) UNSIGNED NOT NULL,
  `average_time` decimal(10,0) UNSIGNED NOT NULL,
  `min_time` int(10) UNSIGNED NOT NULL,
  `max_time` int(10) UNSIGNED NOT NULL,
  `pass_count` int(10) UNSIGNED NOT NULL,
  `error_count` int(10) UNSIGNED NOT NULL,
  `error_percent` decimal(10,0) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `app_runs_to_runs`
--

CREATE TABLE `app_runs_to_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_run_id` int(10) UNSIGNED DEFAULT NULL,
  `run_id` int(10) UNSIGNED DEFAULT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cron_tasks`
--

CREATE TABLE `cron_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `executors`
--

CREATE TABLE `executors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `config` varchar(2000) NOT NULL,
  `is_default` int(1) UNSIGNED NOT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `executors_runs`
--

CREATE TABLE `executors_runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `exec_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `executors_runs_data`
--

CREATE TABLE `executors_runs_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `exec_run_id` int(10) UNSIGNED DEFAULT NULL,
  `console_output` text NOT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `http_codes`
--

CREATE TABLE `http_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `http_methods`
--

CREATE TABLE `http_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `is_favorite` int(1) UNSIGNED NOT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `project_stages`
--

CREATE TABLE `project_stages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `fields` text NOT NULL,
  `is_default` int(1) UNSIGNED NOT NULL,
  `p_id` int(10) UNSIGNED DEFAULT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `runs`
--

CREATE TABLE `runs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `p_id` int(10) UNSIGNED DEFAULT NULL,
  `app_id` int(10) UNSIGNED DEFAULT NULL,
  `stage_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `runs_data`
--

CREATE TABLE `runs_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `r_id` int(10) UNSIGNED NOT NULL,
  `exec` text NOT NULL,
  `console_output` mediumtext NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `runs_samples`
--

CREATE TABLE `runs_samples` (
  `id` int(10) UNSIGNED NOT NULL,
  `summ_report_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(500) NOT NULL,
  `method` int(10) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `request_body` longtext NOT NULL,
  `request_headers` mediumtext NOT NULL,
  `response_code` int(10) UNSIGNED NOT NULL,
  `response_body` longtext NOT NULL,
  `response_headers` mediumtext NOT NULL,
  `error_message` mediumtext NOT NULL,
  `response_time` int(10) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `runs_statuses`
--

CREATE TABLE `runs_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `runs_statuses`
--

INSERT INTO `runs_statuses` (`id`, `name`, `description`, `d_create`, `d_update`, `d_delete`) VALUES
(1, 'pass', '', '2016-05-16 00:00:00', '2016-05-16 00:00:00', '0000-00-00 00:00:00'),
(2, 'fail', '', '2016-05-16 00:00:00', '2016-05-16 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `runs_summary_reports`
--

CREATE TABLE `runs_summary_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `r_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(500) NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `samples` int(10) UNSIGNED NOT NULL,
  `average_time` decimal(10,0) UNSIGNED NOT NULL,
  `min_time` int(10) UNSIGNED NOT NULL,
  `max_time` int(10) UNSIGNED NOT NULL,
  `error_count` int(11) UNSIGNED NOT NULL,
  `error_percent` decimal(10,0) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `login` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `is_verified` int(1) UNSIGNED NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_expired` datetime NOT NULL,
  `status` int(1) UNSIGNED NOT NULL,
  `d_create` datetime NOT NULL,
  `d_update` datetime NOT NULL,
  `d_delete` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `app_runs`
--
ALTER TABLE `app_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`stage_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Индексы таблицы `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`app_run_id`),
  ADD KEY `app_id` (`run_id`);

--
-- Индексы таблицы `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cron_tasks`
--
ALTER TABLE `cron_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `executors`
--
ALTER TABLE `executors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_id` (`app_id`);

--
-- Индексы таблицы `executors_runs`
--
ALTER TABLE `executors_runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`exec_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Индексы таблицы `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`exec_run_id`);

--
-- Индексы таблицы `http_codes`
--
ALTER TABLE `http_codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `http_methods`
--
ALTER TABLE `http_methods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_id` (`app_id`);

--
-- Индексы таблицы `project_stages`
--
ALTER TABLE `project_stages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Индексы таблицы `runs`
--
ALTER TABLE `runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `status` (`status`),
  ADD KEY `stage_id` (`stage_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Индексы таблицы `runs_data`
--
ALTER TABLE `runs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_id` (`r_id`);

--
-- Индексы таблицы `runs_samples`
--
ALTER TABLE `runs_samples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `summ_report_id` (`summ_report_id`),
  ADD KEY `method` (`method`),
  ADD KEY `status` (`status`),
  ADD KEY `response_code` (`response_code`);

--
-- Индексы таблицы `runs_statuses`
--
ALTER TABLE `runs_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `r_id` (`r_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `app_runs`
--
ALTER TABLE `app_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `cron_tasks`
--
ALTER TABLE `cron_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `executors`
--
ALTER TABLE `executors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `executors_runs`
--
ALTER TABLE `executors_runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `project_stages`
--
ALTER TABLE `project_stages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `runs`
--
ALTER TABLE `runs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT для таблицы `runs_data`
--
ALTER TABLE `runs_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT для таблицы `runs_samples`
--
ALTER TABLE `runs_samples`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2085;
--
-- AUTO_INCREMENT для таблицы `runs_statuses`
--
ALTER TABLE `runs_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1033;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `app_runs`
--
ALTER TABLE `app_runs`
  ADD CONSTRAINT `app_runs_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`),
  ADD CONSTRAINT `app_runs_ibfk_2` FOREIGN KEY (`stage_id`) REFERENCES `project_stages` (`id`);

--
-- Ограничения внешнего ключа таблицы `app_runs_to_runs`
--
ALTER TABLE `app_runs_to_runs`
  ADD CONSTRAINT `app_runs_to_runs_ibfk_1` FOREIGN KEY (`app_run_id`) REFERENCES `app_runs` (`id`),
  ADD CONSTRAINT `app_runs_to_runs_ibfk_2` FOREIGN KEY (`run_id`) REFERENCES `runs` (`id`);

--
-- Ограничения внешнего ключа таблицы `executors`
--
ALTER TABLE `executors`
  ADD CONSTRAINT `executors_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Ограничения внешнего ключа таблицы `executors_runs`
--
ALTER TABLE `executors_runs`
  ADD CONSTRAINT `executors_runs_ibfk_2` FOREIGN KEY (`exec_id`) REFERENCES `executors` (`id`),
  ADD CONSTRAINT `executors_runs_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Ограничения внешнего ключа таблицы `executors_runs_data`
--
ALTER TABLE `executors_runs_data`
  ADD CONSTRAINT `executors_runs_data_ibfk_1` FOREIGN KEY (`exec_run_id`) REFERENCES `executors_runs` (`id`);

--
-- Ограничения внешнего ключа таблицы `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Ограничения внешнего ключа таблицы `project_stages`
--
ALTER TABLE `project_stages`
  ADD CONSTRAINT `project_stages_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `project_stages_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Ограничения внешнего ключа таблицы `runs`
--
ALTER TABLE `runs`
  ADD CONSTRAINT `runs_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `runs_ibfk_2` FOREIGN KEY (`stage_id`) REFERENCES `project_stages` (`id`),
  ADD CONSTRAINT `runs_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`);

--
-- Ограничения внешнего ключа таблицы `runs_data`
--
ALTER TABLE `runs_data`
  ADD CONSTRAINT `runs_data_ibfk_1` FOREIGN KEY (`r_id`) REFERENCES `runs` (`id`);

--
-- Ограничения внешнего ключа таблицы `runs_samples`
--
ALTER TABLE `runs_samples`
  ADD CONSTRAINT `runs_samples_ibfk_1` FOREIGN KEY (`summ_report_id`) REFERENCES `runs_summary_reports` (`id`),
  ADD CONSTRAINT `runs_samples_ibfk_2` FOREIGN KEY (`status`) REFERENCES `runs_statuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `runs_summary_reports`
--
ALTER TABLE `runs_summary_reports`
  ADD CONSTRAINT `runs_summary_reports_ibfk_2` FOREIGN KEY (`status`) REFERENCES `runs_statuses` (`id`),
  ADD CONSTRAINT `runs_summary_reports_ibfk_3` FOREIGN KEY (`r_id`) REFERENCES `runs` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
