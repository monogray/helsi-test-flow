<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    if (is_file($file)) {
        return FALSE;
    }
}

// Composer. Vendors autoload
require __DIR__ . '/vendor/autoload.php';

// Load resources. TODO: Move to autoload
require_once __DIR__ . '/bootstrap.php';

// Define constants
define('APP_DIR', __DIR__);
define('APP_PUBLIC_KEY', '1_iyBhvvU6FI0QMN_zeHpbWtykJ1Y4ZRTrdpRggUN7N0');

if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    define('STAGE', 'Dev');
} else {
    define('STAGE', 'Prod');
}

if (STAGE == 'Dev') {
    date_default_timezone_set('Europe/Kiev');

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    define('ROOT_PATH', 'http://localhost/~user/helsi-test-flow/');
    define('BASE_PATH', '/~user/helsi-test-flow');
    define('API_PREFIX', '/api/v1');
    define('BASE_API_PATH', BASE_PATH . API_PREFIX);
    define('BASE_APP_PATH', '/~user/helsi-test-flow');

    // JMeter
    define('JMETER_PATH', __DIR__ . '/bin/apache-jmeter-3.2/bin/jmeter');
    define('JMETER_REPORTS_PATH', __DIR__ . '/jmeter-src/results');

    // Robot Framework
    define('ROBOT_TESTS_PATH', 'C:\Project\helsi-autoqa-gui\ims_mis\acceptance_tests');
} else {
    ini_set('session.save_path', __DIR__ . DIRECTORY_SEPARATOR . 'sessions');
    set_time_limit(10800);  // 3h

    define('ROOT_PATH', '/');
    define('BASE_PATH', '');
    define('API_PREFIX', '/api/v1');
    define('BASE_API_PATH', BASE_PATH . API_PREFIX);
    define('BASE_APP_PATH', '');

    // JMeter
    define('JMETER_PATH',  'C:' . DIRECTORY_SEPARATOR . 'Project' . DIRECTORY_SEPARATOR . 'jmeter-src' . DIRECTORY_SEPARATOR . 'apache-jmeter-3.1' . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'jmeter');
    define('JMETER_REPORTS_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'jmeter-src' . DIRECTORY_SEPARATOR . 'results');

    // Robot Framework
    define('ROBOT_TESTS_PATH', 'C:\Project\helsi-autoqa-gui\ims_mis\acceptance_tests');
}

// Start session end set default settings
session_start();

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];

    return $protocol . $domainName;
}
define('BASE_URL', siteURL());

// Set up Database connection
use RedBeanPHP\Facade as R;

if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    R::setup('mysql:host=127.0.0.1;dbname=auto_qa', 'root', '');
} else {
    R::setup('mysql:host=127.0.0.1;dbname=auto_qa', 'root', 'rBirdC0nq');
}

// Configure RedBean
R::freeze(TRUE);
R::ext('xdispense', function ($type) {
    return R::getRedBean()->dispense($type);
});

// Init the application
$settings = require __DIR__ . '/application/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/application/dependencies.php';

// Register middleware
require __DIR__ . '/application/middleware.php';

// Register routes
require __DIR__ . '/application/routes.php';

// Run app
$app->run();
