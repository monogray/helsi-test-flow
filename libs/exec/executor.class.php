<?php

/**
 * Class Executor
 *
 * @author Alchemist
 */

namespace AutoQa\Libs;

class Executor
{

    protected $desc, $path;

    /**
     * Executor
     *
     * @param string $path
     */
    function Executor($path = "./")
    {
        $this->desc = array(
            0 => array("pipe", "r"),
            1 => array("pipe", "w"),
            2 => array("pipe", "w")
        );
        $this->path = $path;
    }

    /**
     * Full control run
     *
     * @param string $cmd
     * @param bool $comments
     * @param bool $execTime
     * @return string
     */
    function run($cmd = '', $comments = TRUE, $execTime = TRUE)
    {
        $out = "Error: Empty cmd";
        if ($cmd) {
            $time = microtime(TRUE);

            $process = proc_open($cmd, $this->desc, $pipes, $this->path);

            if (is_resource($process)) {
                $result = stream_get_contents($pipes[1]);
                $error = stream_get_contents($pipes[2]);

                fclose($pipes[1]);
                fclose($pipes[2]);
                proc_close($process);

                $time = round((microtime(TRUE) - $time), 4);
                if ($comments) {
                    $out = (($comments) ? "# cmd:\n{$cmd} \n\n" : "")
                        . (($execTime) ? "# time:\n{$time}s \n\n" : "")
                        . (($error) ? "# error:\n{$error} \n\n" : "")
                        . (($result) ? "# out:\n{$result}\n" : "");
                } else {
                    $out = (($result) ? "{$result}\n" : "")
                        . (($error) ? "{$error}\n" : "");
                }
            }
        }

        return $out;
    }

    function run2($cmd = '', $comments = TRUE, $execTime = TRUE) {
        $out = "Error: Empty cmd";
        if ($cmd) {
            $time = microtime(TRUE);

            echo microtime(TRUE) . '<br>';
            $process = proc_open($cmd, $this->desc, $pipes, $this->path);

            var_dump($process);

            $pid = proc_get_status ($process)['pid'];
            //var_dump($pid);

            $pStatus = posix_getpgid($pid);
            var_dump($pStatus);

            if (is_resource($process)) {
                /*$result = stream_get_contents($pipes[1]);
                $error = stream_get_contents($pipes[2]);*/

                //fclose($pipes[1]);
                //fclose($pipes[2]);
                //proc_close($process);

                $time = round((microtime(TRUE) - $time), 4);
                /*if ($comments) {
                    $out = (($comments) ? "# cmd:\n{$cmd} \n\n" : "")
                        . (($execTime) ? "# time:\n{$time}s \n\n" : "")
                        . (($error) ? "# error:\n{$error} \n\n" : "")
                        . (($result) ? "# out:\n{$result}\n" : "");
                } else {
                    $out = (($result) ? "{$result}\n" : "")
                        . (($error) ? "{$error}\n" : "");
                }*/

                $pStatus = posix_getpgid($pid);
                var_dump($pStatus); die;
            }
        }

        return $out;
    }

    /**
     * Simple shell exec
     *
     * @param string $cmd
     * @return bool|string
     */
    function shell($cmd = '')
    {
        if ($cmd) {
            return shell_exec($cmd);
        }

        return FALSE;
    }
}