<?php

date_default_timezone_set('Europe/Kiev');

// Composer. Vendors autoload
require __DIR__ . '/../../vendor/autoload.php';

// Set up Database connection
use RedBeanPHP\Facade as R;

if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    R::setup('mysql:host=127.0.0.1;dbname=helsi_auto', 'root', '');
} else {
}

// Configure RedBean
R::freeze(TRUE);
R::ext('xdispense', function ($type) {
    return R::getRedBean()->dispense($type);
});
// END Set up Database connection

function fillDb($patientsIds, $from, $to)
{
    $i = 0;
    foreach ($patientsIds as $patientId) {

        if ($i >= $from && $i < $to) {
            $patient = R::getRow('
                SELECT * FROM patients WHERE sys_id = \'' . $patientId . '\'
            ');

            if (is_null($patient)) {
                $patientEnt = R::xdispense('patients');
                $patientEnt->sysId = $patientId;
                R::store($patientEnt);
            }
        }

        $i++;
    }
}

/* function deleteDuplicates($from, $to)
{
    $patients = R::getAll('
        SELECT * FROM patients LIMIT ?, ?
    ', [$from, $to]);

    foreach ($patients as $patient) {
        $id = $patient['id'];
        $sysId = $patient['sys_id'];

        $patientsCount = R::getRow('
                SELECT COUNT(id) AS pat_count FROM patients WHERE sys_id = \'' . $sysId . '\'
            ');

        if ((int)$patientsCount['pat_count'] == 2) {

            echo $sysId . '<br>';

            R::exec(
                'DELETE FROM patients WHERE id = ?',
                [$id]
            );
        }
    }
} */

function getNextNotCheckedPatients($from, $count)
{
    $count = (int)$count;
    $from = (int)$from;

    $patients = R::getAll('
            SELECT * FROM patients WHERE search_phone_status IS NULL OR search_fio_status IS NULL ORDER BY id ASC LIMIT ?, ?
        ', [$from, $count]);

    return $patients;
}

use Helsi\HelsiApiFactory;

function testSearch($patients)
{
    require_once __DIR__ . '/HelsiApiFactory.php';

    $helsiApiFactory = new HelsiApiFactory();
    $helsiApiFactory->login();

    foreach ($patients as $patient) {

        $patientData = $helsiApiFactory->getPatientData($patient['sys_id']);
        $fio = $patientData['lastName'] . ' ' . $patientData['firstName'] . ' ' . $patientData['middleName'];

        // Search by FIO
        $res = $helsiApiFactory->search($fio);

        $resultFio = FALSE;
        if (!is_null($res)) {
            foreach ($res as $ids) {
                if ($ids == $patient['sys_id']) {
                    $resultFio = TRUE;
                    break;
                }
            }
        }

        if (!$resultFio) {
            $patientEnt = R::xdispense('patients');
            $patientEnt->id = $patient['id'];
            $patientEnt->searchFioStatus = 0;
            $patientEnt->searchFioLog = $fio;
            R::store($patientEnt);
        } else {
            $patientEnt = R::xdispense('patients');
            $patientEnt->id = $patient['id'];
            $patientEnt->searchFioStatus = 1;
            $patientEnt->searchFioLog = NULL;
            R::store($patientEnt);
        }

        // Search by Phone
        $res = $helsiApiFactory->search($patientData['phone']);

        $resultPhone = FALSE;
        if (!is_null($res)) {
            foreach ($res as $ids) {
                if ($ids == $patient['sys_id']) {
                    $resultPhone = TRUE;
                    break;
                }
            }
        }

        if (!$resultPhone) {
            $patientEnt = R::xdispense('patients');
            $patientEnt->id = $patient['id'];
            $patientEnt->searchPhoneStatus = 0;
            $patientEnt->searchPhoneLog = $patientData['phone'];
            R::store($patientEnt);
        } else {
            $patientEnt = R::xdispense('patients');
            $patientEnt->id = $patient['id'];
            $patientEnt->searchPhoneStatus = 1;
            $patientEnt->searchPhoneLog = NULL;
            R::store($patientEnt);
        }

    }
}

function getErrors()
{
    $patients = R::getAll('
            SELECT * FROM patients WHERE search_phone_status = 0 OR search_fio_status = 0
        ');

    $result = array(
        'errors'    => array(),
        'all_count' => 0,
        'errors_count' => 0,
    );

    foreach ($patients as $patient) {
        $result['errors'][$patient['sys_id']] = array(
            'phoneSearchStatus' => $patient['search_phone_status'] == 0 ? FALSE : TRUE,
            'phoneSearchLog'    => $patient['search_phone_log'],
            'nameStatus'        => $patient['search_fio_status'] == 0 ? FALSE : TRUE,
            'nameLog'           => $patient['search_fio_log'],
        );
    }

    $allCount = R::getRow('
            SELECT COUNT(id) AS all_count FROM patients
        ');

    $result['all_count'] = $allCount['all_count'];
    $result['errors_count'] = count($result['errors']);

    $checkedCount = R::getRow('
            SELECT COUNT(id) AS checked_count FROM patients
            WHERE
              search_phone_status IS NOT NULL AND
              search_fio_status IS NOT NULL
        ');
    $result['checked_count'] = $checkedCount['checked_count'];

    $checkedCount = R::getRow('
            SELECT COUNT(id) AS non_checked_count FROM patients
            WHERE
              search_phone_status IS NULL OR
              search_fio_status IS NULL
        ');
    $result['non_checked_count'] = $checkedCount['non_checked_count'];

    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($result, JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE);
}

// deleteDuplicates(60000, 65000);

if ($_GET['mode'] == 'check') {

    if (isset($_GET['from']) && isset($_GET['to'])) {
        $patients = getNextNotCheckedPatients((int)$_GET['from'], (int)$_GET['to']);
        testSearch($patients);
    } else {
        echo 'Error. Not set "from/to" params.';
        die;
    }

} else if ($_GET['mode'] == 'stat') {
    getErrors();
} else if ($_GET['mode'] == 'update-db') {
    $patientsIds = file_get_contents(__DIR__ . '/patientId.json');
    $patientsIds = explode("\n", $patientsIds);

    $from = 2000;
    $to = 6000;

    echo 'Update DB start: ' . $from . ' - ' . $to . '<br/>';
    fillDb($patientsIds, $from, $to);
    echo 'Update DB ended: ' . $from . ' - ' . $to;

    // 0 - 6000
    // 122900 - 135000
} else {
    echo 'Error';
    die;
}