<?php

namespace Helsi;

use GuzzleHttp;

class HelsiApiFactory
{
    // private $apiBaseUrl = 'https://stagingapi.helsi.pro';
    // private $login = '';
    // private $pass = '';

    private $apiBaseUrl = 'https://api.helsi.pro';
    private $login = 'monogray@gmail.com';
    private $pass = 'J6c0nRp';

    private $client = NULL;

    function __construct()
    {
        $this->client = new GuzzleHttp\Client(
            ['cookies' => TRUE]
        );
    }

    public function login()
    {
        try {
            $res = $this->client->request('POST', "{$this->apiBaseUrl}/login", [
                'body' => '{"username":"' . $this->login . '","password":"' . $this->pass . '"}'
            ]);
        } catch (\Exception $e) {
            return NULL;
        }

        return $res;
    }

    public function getPatient($patientId)
    {
        try {
            $res = $this->client->request('GET', "{$this->apiBaseUrl}/patient/{$patientId}", []);
            $res = $res->getBody();
        } catch (\Exception $e) {
            return NULL;
        }

        return $res;
    }

    public function getPatientData($patientId)
    {
        $patientRawData = $this->getPatient($patientId);

        if (is_null($patientRawData)) {
            return NULL;
        }

        $patientRawData = json_decode($patientRawData, TRUE);

        $phones = [];
        foreach ($patientRawData['phones'] as $phoneObj) {
            array_push($phones, $phoneObj['phone']);
        }

        return [
            'firstName'        => $patientRawData['firstName'],
            'lastName'         => $patientRawData['lastName'],
            'middleName'       => $patientRawData['middleName'],
            'additionalPhones' => $phones,
            'phone'            => $patientRawData['phone'],
            'primaryEmail'     => $patientRawData['primaryEmail'],
        ];
    }

    public function search($request)
    {
        try {
            $patientRawData = $this->client->request('GET', "{$this->apiBaseUrl}/patient?search=" . urlencode($request), []);
        } catch (\Exception $e) {
            return NULL;
        }

        $patientRawData = json_decode($patientRawData->getBody(), TRUE);

        $patientIds = array();
        foreach ($patientRawData as $patientData) {
            array_push($patientIds, $patientData['patientId']);
        }

        return $patientIds;
    }
}