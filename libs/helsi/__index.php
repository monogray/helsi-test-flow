<?php

date_default_timezone_set('Europe/Kiev');

// Composer. Vendors autoload
require __DIR__ . '/../../vendor/autoload.php';

$patientsIds = file_get_contents(__DIR__ . '/patient.json');
$patientsIds = explode("\n", $patientsIds);
unset($patientsIds[0]);

echo (count($patientsIds) . '<br>-----------<br>');

$from = 40001;
$to = 44000;
// 5000
// 5000 - 10100
// 10100 - 15000
// 15000 - 19983

// 19982 - 24000 - gc
// 24000 - 28714 - yan

// 28714 - 32000 - gc
// 32000 - 36000 - yan

// 36001 - 40000 - gc
// 40001 - 44000 - yan


file_put_contents(__DIR__ . '/log_pass.dat', '');
// file_put_contents(__DIR__ . '/log_error.dat', '');

function printLog($str, $mode) {
    if ($mode == 'PASS') {
        file_put_contents(__DIR__ . '/log_pass.dat', $str, FILE_APPEND);
    } else if ($mode == 'FAIL') {
        file_put_contents(__DIR__ . '/log_error.dat', $str, FILE_APPEND);
    }
}

require_once __DIR__ . '/HelsiApiFactory.php';
use Helsi\HelsiApiFactory;
$helsiApiFactory = new HelsiApiFactory();
$helsiApiFactory->login();

$i = 0;
foreach ($patientsIds as $patientId) {
    if ($i >= $from && $i <= $to) {
        $globalResult = TRUE;

        printLog($patientId . "\n", 'PASS');

        $patientData = $helsiApiFactory->getPatientData($patientId);

        $fio = $patientData['lastName'] . ' ' . $patientData['firstName'] . ' ' . $patientData['middleName'];

        printLog($fio . "\n", 'PASS');

        $res = $helsiApiFactory->search($fio);

        $resultFio = FALSE;
        foreach ($res as $ids) {
            if ($ids == $patientId) {
                $resultFio = TRUE;
                break;
            }
        }
        if (!$resultFio) {
            $globalResult = FALSE;
        }

        printLog(($resultFio ? 'Pass' : 'Fail') . "\n", 'PASS');

        $res = $helsiApiFactory->search($patientData['phone']);

        printLog($patientData['phone'] . "\n", 'PASS');

        $resultPhone = FALSE;
        foreach ($res as $ids) {
            if ($ids == $patientId) {
                $resultPhone = TRUE;
                break;
            }
        }
        if (!$resultPhone) {
            $globalResult = FALSE;
        }

        printLog(($resultPhone ? 'Pass' : 'Fail') . "\n", 'PASS');
        printLog("\n", 'PASS');

        if (!$globalResult) {
            printLog($patientId . "\n", 'FAIL');

            if (!$resultFio) {
                printLog($fio . "\n", 'FAIL');
            }
            if (!$resultPhone) {
                printLog($patientData['phone'] . "\n", 'FAIL');
            }
            printLog("\n", 'FAIL');
        }
    }
    $i++;
}