<?php

namespace Jmeter\Reports;

class Request
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $url;

    /**
     * @var int
     */
    private $method;

    /**
     * @var string
     */
    private $queryString;

    /**
     * @var string
     */
    private $requestHeaders;

    /**
     * @var string
     */
    private $responseHeaders;

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var int
     */
    private $responseTime;

    /**
     * @var int
     */
    private $bites;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var array
     */
    private $failureMessages = array();

    /**
     * @var string
     */
    private $responseData;

    function __construct($label, $code) {
        $this->setLabel($label);
        $this->setStatusCode($code);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = strval($label);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = strval($url);
    }

    /**
     * @return int
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        require_once __DIR__ . '/HttpMethods.php';
        $httpMethods = new HttpMethods();
        $this->method = $httpMethods->getMethods()[strval($method)];
    }

    /**
     * @return string
     */
    public function getRequestHeaders()
    {
        return $this->requestHeaders;
    }

    /**
     * @param string $requestHeaders
     */
    public function setRequestHeaders($requestHeaders)
    {
        $this->requestHeaders = strval($requestHeaders);
    }

    /**
     * @return string
     */
    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    /**
     * @param string $responseHeaders
     */
    public function setResponseHeaders($responseHeaders)
    {
        $this->responseHeaders = strval($responseHeaders);
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
        return $this->queryString;
    }

    /**
     * @param string $queryString
     */
    public function setQueryString($queryString)
    {
        $this->queryString = strval($queryString);
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = (int)$statusCode;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $responseTime
     */
    public function setResponseTime($responseTime)
    {
        $this->responseTime = (int)$responseTime;
    }

    /**
     * @return int
     */
    public function getResponseTime()
    {
        return $this->responseTime;
    }

    /**
     * @param int $bites
     */
    public function setBites($bites)
    {
        $this->bites = (int)$bites;
    }

    /**
     * @return int
     */
    public function getBites()
    {
        return $this->bites;
    }

    /**
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->status = (int)$status;
    }

    /**
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $responseData
     */
    public function setResponseData($responseData)
    {
        $this->responseData = $responseData;
    }

    /**
     * @return string
     */
    public function getResponseData()
    {
        return $this->responseData;
    }

    /**
     * @param string $failureMessage
     */
    public function addFailureMessage($failureMessage)
    {
        array_push($this->failureMessages, $failureMessage);
    }

    /**
     * @return string
     */
    public function getFailureMessages()
    {
        return $this->failureMessages;
    }
}