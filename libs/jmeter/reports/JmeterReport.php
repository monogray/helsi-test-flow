<?php

namespace Jmeter\Reports;

use Jmeter\Reports\Request;

require_once __DIR__ . '/Request.php';

class JmeterReport
{
    private $requests = array();
    private $summaryReport = array();

    function __construct()
    {
    }

    public function parseReport($reportPath, $format = 'XML')
    {
        $report = file_get_contents($reportPath, TRUE);

        if ($format == 'XML') {
            return $this->parseXmlReport($report);
        } else if ($format == 'CSV') {
            return $this->parseCsvReport($report);
        } else {
            return NULL;
        }
    }

    protected function parseCsvReport($csvReport)
    {
        $requests = explode("\n", $csvReport);

        $i = 0;
        foreach ($requests as $request) {
            if ($request != '') {
                $requestArr = explode(",", $request);
                $this->requests[$i] = new Request($requestArr[2], $requestArr[3]);
                $this->requests[$i]->setResponseTime($requestArr[1]);
                $this->requests[$i]->setBites($requestArr[8]);

                $i++;
            }
        }

        return $this->requests;
    }

    protected function parseXmlReport($xmlReport)
    {
        $report = new \SimpleXMLElement($xmlReport);

        $i = 0;
        foreach ($report->httpSample as $sample) {

            if (count($sample->httpSample) == 0) {
                $request = new Request($sample->attributes()->lb, $sample->attributes()->rc);

                $request->setResponseTime(strval($sample->attributes()->t));
                $request->setBites(strval($sample->attributes()->by));
                $request->setStatus($sample->attributes()->s == 'true' ? TRUE : FALSE);

                $request->setResponseData($sample->responseData);

                $urlName = 'java.net.URL';
                $request->setUrl($sample->$urlName);

                $request->setMethod($sample->method);
                $request->setQueryString($sample->queryString);
                $request->setRequestHeaders($sample->requestHeader);
                $request->setResponseHeaders($sample->responseHeader);

                foreach ($sample->assertionResult as $assertionResult) {
                    if ($assertionResult->failure == 'true') {
                        $request->addFailureMessage($assertionResult->failureMessage);
                        $request->setStatus(FALSE); // Fix bug of jMeter
                    }
                }

                $this->requests[$i] = $request;

                $i++;
            } else {
                foreach ($sample->httpSample as $sampleInner) {
                    $request = new Request($sample->attributes()->lb, $sampleInner->attributes()->rc);

                    $request->setResponseTime(strval($sampleInner->attributes()->t));
                    $request->setBites(strval($sampleInner->attributes()->by));
                    $request->setStatus($sampleInner->attributes()->s == 'true' ? TRUE : FALSE);

                    $request->setResponseData($sampleInner->responseData);

                    $urlName = 'java.net.URL';
                    $request->setUrl($sampleInner->$urlName);

                    $request->setMethod($sampleInner->method);
                    $request->setQueryString($sampleInner->queryString);
                    $request->setRequestHeaders($sampleInner->requestHeader);
                    $request->setResponseHeaders($sampleInner->responseHeader);

                    foreach ($sample->assertionResult as $assertionResult) {
                        if ($assertionResult->failure == 'true') {
                            $request->addFailureMessage($assertionResult->failureMessage);
                            $request->setStatus(FALSE); // Fix bug of jMeter
                        }
                    }

                    $this->requests[$i] = $request;
                    $i++;
                }
            }
        }

        return $this->requests;
    }

    public function getSummaryReport()
    {
        $this->summaryReport['GraphResult'] = array();

        foreach ($this->requests as $request) {

            if (!isset($this->summaryReport[$request->getLabel()])) {
                $this->summaryReport[$request->getLabel()] = array(
                    'requests'        => array(),
                    'samples'         => 0,
                    'errorCount'      => 0,
                    'errorPercent'    => 0,
                    'minResponse'     => 9999999999999,
                    'maxResponse'     => 0,
                    'sumResponse'     => 0,
                    'averageResponse' => 0,
                    'responsesTime'   => array(),
                );
            }

            $samplerRequest = &$this->summaryReport[$request->getLabel()];

            array_push($samplerRequest['requests'], $request);
            $samplerRequest['samples']++;

            if (!$request->getStatus()) {
                $samplerRequest['errorCount']++;
            }

            if ($samplerRequest['errorCount'] != 0) { // TODO. Optimize. Do not calculate each iteration
                $samplerRequest['errorPercent'] = $samplerRequest['samples'] / $samplerRequest['errorCount'] * 100;
            } else {
                $samplerRequest['errorPercent'] = 0;
            }

            if ($samplerRequest['minResponse'] > $request->getResponseTime()) {
                $samplerRequest['minResponse'] = $request->getResponseTime();
            }

            if ($samplerRequest['maxResponse'] < $request->getResponseTime()) {
                $samplerRequest['maxResponse'] = $request->getResponseTime();
            }

            $samplerRequest['sumResponse'] += $request->getResponseTime();
            $samplerRequest['responsesTime'][$samplerRequest['samples']] = $request->getResponseTime();

            if ($samplerRequest['averageResponse'] != 0) { // TODO. Optimize. Do not calculate aech iteration
                $samplerRequest['averageResponse'] = $samplerRequest['sumResponse'] / $samplerRequest['samples'];
            } else {
                $samplerRequest['averageResponse'] = $samplerRequest['sumResponse'];
            }

            array_push($this->summaryReport['GraphResult'], $request->getResponseTime());
        }

        // Add Total row in report
        $this->summaryReport['Total'] = array(
            'samples'         => 0,
            'errorCount'      => 0,
            'errorPercent'    => 0,
            'minResponse'     => 9999999999999,
            'maxResponse'     => 0,
            'sumResponse'     => 0,
            'averageResponse' => 0,
            'responsesTime'   => array(),
        );
        $total = &$this->summaryReport['Total'];

        $averageResponseSum = 0;
        $maxResponseSum = 0;
        $minResponseSum = 0;
        $errorPercentSum = 0;

        $i = 0;
        foreach ($this->summaryReport as $label => $summaryReportRow) {
            if ($label != 'Total' && $label != 'GraphResult') {
                $total['samples'] += $summaryReportRow['samples'];
                $total['errorCount'] += $summaryReportRow['errorCount'];

                $averageResponseSum += $summaryReportRow['averageResponse'];
                $maxResponseSum += $summaryReportRow['maxResponse'];
                $minResponseSum += $summaryReportRow['minResponse'];
                $errorPercentSum += $summaryReportRow['errorPercent'];

                $i++;
            }
        }

        $total['averageResponse'] = round($averageResponseSum / $i, 3);
        $total['maxResponse'] = round($maxResponseSum / $i, 3);
        $total['minResponse'] = round($minResponseSum / $i, 3);
        $total['errorPercent'] = round($errorPercentSum / $i, 3);

        return $this->summaryReport;
    }
}