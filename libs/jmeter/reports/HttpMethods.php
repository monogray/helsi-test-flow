<?php

namespace Jmeter\Reports;

class HttpMethods
{
    private $methods = array(
        'GET'     => 1,
        'HEAD'    => 2,
        'POST'    => 3,
        'PUT'     => 4,
        'DELETE'  => 5,
        'CONNECT' => 6,
        'OPTIONS' => 7,
        'TRACE'   => 8,
    );

    private $methodIds = array(
        1 => 'GET',
        2 => 'HEAD',
        3 => 'POST',
        4 => 'PUT',
        5 => 'DELETE',
        6 => 'CONNECT',
        7 => 'OPTIONS',
        8 => 'TRACE',
    );

    /**
     * @return array
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * @return array
     */
    public function getMethodIds()
    {
        return $this->methodIds;
    }

    /**
     * @param int $methodId
     * @return string
     */
    public function getMethodNameById($methodId)
    {
        return $this->methodIds[$methodId];
    }
}