<?php

namespace Mono\Fixtures;

use Mono\Fixtures\DataSets\NamesEn;

class MonoFixtures
{
    public function __construct()
    {

    }

    public function generateData ($config) {
        // TODO Validate config

        $result = array();

        for ($i = 0; $i < $config['count']; $i++) {
            $currentItem = array();

            foreach ($config['dataSet'] as $set) {
                $setName = $setAlias = $set;

                $setParsed = explode("=", $set);
                if (count($setParsed) == 2) {
                    $setAlias = $setParsed[0];
                    $setName = $setParsed[1];
                }

                if ($setName == 'name') {
                    require_once __DIR__ . '/DataSets/NamesEn.php';
                    $setCollection = new DataSets\NamesEn();

                    $currentItem[$setAlias] = $setCollection->getRandom();
                } else if ($setName == 'lastname') {
                    require_once __DIR__ . '/DataSets/SurnamesEn.php';
                    $setCollection = new DataSets\SurnamesEn();

                    $currentItem[$setAlias] = $setCollection->getRandom();
                } else if ($setName == 'email') {
                    require_once __DIR__ . '/DataSets/Emails.php';
                    $setCollection = new DataSets\Emails();

                    $currentItem[$setAlias] = $setCollection->getRandom(1, array('RANDOMIZE'));
                } else if ($setName == 'md5') {
                    require_once __DIR__ . '/DataSets/Md5Hash.php';
                    $setCollection = new DataSets\Md5Hash();

                    $currentItem[$setAlias] = $setCollection->getRandom(1);
                } else if ($setName == 'integer') {
                    require_once __DIR__ . '/DataSets/Integers.php';
                    $setCollection = new DataSets\Integers();

                    $currentItem[$setAlias] = $setCollection->getRandom(1);
                }
            }

            $result[$i] = $currentItem;
        }

        return $result;
    }

    public function generateNames($count = 1, $lang = 'EN')
    {
        $id = mt_rand(0, count($this->namesEn) - 1);
        return $this->namesEn[$id];
    }

    public function generateSurnames($count = 1, $lang = 'EN')
    {
        $id = mt_rand(0, count($this->surnamesEn) - 1);
        return $this->surnamesEn[$id];
    }
}