<?php

namespace Mono\Fixtures\DataSets;

use Stringy\Stringy as S;

class SurnamesEn
{
    private $set = array(
        'Walker',
        'Thompson',
        'Anderson',
        'Johnson',
        'Tremblay',
        'Peltier',
        'Cunningham',
        'Simpson',
        'Mercado',
        'Mercado II',
        'Sellers',
    );

    private $setLength = NULL;

    public function __construct()
    {
        $this->setLength = count($this->set);
    }

    public function getRandom($count = 1, $config = array())
    {
        while (TRUE) {
            $id = mt_rand(0, $this->setLength - 1);
            $data = $this->set[$id];

            if (in_array('WITHOUT_SPACES', $config)) {
                if (preg_match('/\s/', $data)) {
                    continue;
                }
            }

            if (in_array('DELETE_SPACES', $config)) {
                $data = str_replace(' ', '', $data);
            }

            if (in_array('TO_LOWER_CASE', $config)) {
                $data = S::create($data)->toLowerCase();
            }

            return $data;
        }
    }
}