<?php

namespace Mono\Fixtures\DataSets;

class Md5Hash
{

    public function __construct()
    {
    }

    public function getRandom($count = 1)
    {
        return md5(date('l jS \of F Y h:i:s A') . mt_rand() . mt_rand());
    }

}