<?php

namespace Mono\Fixtures\DataSets;

class AlphabetEn
{
    private $small = array(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    );
    private $caps = array(
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    );

    private $smallLength = NULL;
    private $capsLength = NULL;

    public function __construct()
    {
        $this->smallLength = count($this->small);
        $this->capsLength = count($this->caps);
    }

    public function getRandom($count = 1)
    {
        // TODO Implement
    }

    public function getRandomSmall($count = 1)
    {
        $id = mt_rand(0, $this->smallLength - 1);

        return $this->small[$id];
    }

    public function getRandomCaps($count = 1)
    {
        $id = mt_rand(0, $this->capsLength - 1);

        return $this->caps[$id];
    }
}