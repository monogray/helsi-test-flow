<?php

namespace Mono\Fixtures\DataSets;

class NamesEn
{
    private $set = array(
        'Christopher',
        'Ryan',
        'Ethan',
        'John',
        'Zoey',
        'Sarah',
        'Michelle',
        'Samantha',
        'Ivan'
    );

    private $setLength = NULL;

    public function __construct()
    {
        $this->setLength = count($this->set);
    }

    public function getRandom($count = 1)
    {
        $id = mt_rand(0, $this->setLength - 1);
        return $this->set[$id];
    }
}