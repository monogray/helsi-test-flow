<?php

namespace Mono\Fixtures\DataSets;

class Emails
{
    public function __construct()
    {
    }

    public function getRandom($count = 1, $config = array())
    {
        require_once __DIR__ . '/SurnamesEn.php';
        $surnamesCollection = new SurnamesEn();

        require_once __DIR__ . '/AlphabetEn.php';
        $alphabetCollection = new AlphabetEn();

        require_once __DIR__ . '/Md5Hash.php';
        $md5Collection = new Md5Hash();

        $email = '';
        if (in_array('RANDOMIZE', $config)) {
            $email =  $alphabetCollection->getRandomCaps() . '.' . $surnamesCollection->getRandom(1, array('DELETE_SPACES')) . '_' . $md5Collection->getRandom() . '@' . 'test.com';
        } else {
            $email =  $alphabetCollection->getRandomCaps() . '.' . $surnamesCollection->getRandom(1, array('DELETE_SPACES')) . '@' . 'test.com';
        }

        return $email;
    }
}