<?php

// Controllers
require_once __DIR__ . '/application/controllers/BaseAppController.php';
foreach (glob(__DIR__ . '/application/controllers/*.php') as $file) {
    require_once $file;
}
foreach (glob(__DIR__ . '/application/controllers/api/*.php') as $file) {
    require_once $file;
}
foreach (glob(__DIR__ . '/application/controllers/portal/*.php') as $file) {
    require_once $file;
}

// Models
require_once __DIR__ . '/application/models/CoreModel.php';
foreach (glob(__DIR__ . '/application/models/*.php') as $file) {
    require_once $file;
}

// Libs
require_once __DIR__ . '/libs/exec/executor.class.php';