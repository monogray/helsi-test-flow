<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="grid-x grid-padding-x">
        <div class="cell align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>"><i class="fa fa-cubes"></i> App: <?= $application->name  ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>/robot"><i class="fa fa-gamepad"></i> Robot</a></li>
                <li class="current"><a href="#">Executors</a></li>
            </ul>

            <div class="grid-x grid-padding-x">

                <div class="small-12 large-12 cell">
                    <h1><i class="fa fa-code"></i> Executors (<?= count($executors) ?>)</h1>
                </div> <!-- .cell -->

            </div> <!-- .grid-x -->

            <div class="grid-x grid-padding-x">

                <div class="small-12 large-12 cell">
                    <div style="margin-top: 15px;">
                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors/create"
                           class="button small secondary">
                            Create new
                        </a>
                    </div>
                    <div class="separator-20"></div>
                </div> <!-- .cell -->

            </div> <!-- .grid-x -->

            <div class="card">

                <table>

                    <thead>
                    <tr>
                        <td class="width-1 text-right text-muted">#</td>
                        <td class="width-1"></td>
                        <td>Name</td>
                        <td class="width-15 text-right">Actions</td>
                    </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($executors as $id => $executor) { ?>
                            <tr>
                                <td class="text-right text-muted text-small width-1">
                                    <?= $id ?>
                                </td>
                                <td class="text-center text-muted width-1">
                                    <i class="<?= $executor['ico'] ?>"></i>
                                </td>
                                <td class="text-muted">
                                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors/<?= $id ?>/run">
                                        <?= $executor['name'] ?>
                                    </a>
                                </td>
                                <td class="text-right">
                                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors/<?= $id ?>/run"
                                       class="run-bt small button">
                                        Run / Edit...
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div> <!-- .card -->

        </div> <!-- .cell -->
    </div> <!-- .grid-x -->

    <div class="separator-100"></div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>