<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>"><i class="fa fa-cubes"></i> App: <?= $application->name  ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>/robot"><i class="fa fa-gamepad"></i> Robot</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>/robot/executors"><i class="fa fa fa-code"></i> Executors</a></li>
                <li class="current"><a href="#">#<?= $executor->id ?>. <?= $executor->name ?></a></li>
            </ul>

            <h1><i class="fa fa-terminal"></i> Executor: <?= $executor->name ?></h1>
            <hr/>

            <div class="row">
                <div class="small-9 large-9 columns">
                    <form action="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors/<?= $executor->id ?>/update" method="post"
                          enctype="application/x-www-form-urlencoded">

                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" value="<?= $executor->name ?>">

                        <label for="description">Description</label>
                        <textarea id="description" name="description"><?= $executor->description ?></textarea>

                        <label for="config">Config</label>
                        <textarea id="config" name="config"><?= $executor->config ?></textarea>

                        <input type="submit" class="small button" id="submit" value="Save">

                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom hidden" id="submit-preloader"></i>
                        <span class="sr-only">Loading...</span>

                    </form>
                </div>

                <div class="small-3 large-3 columns text-right">
                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors"
                       class="button small alert" id="delete-bt">
                        Delete
                    </a>
                </div>
            </div>

        </div>
    </div>

    <div class="separator-100"></div>

    <script src="<?= BASE_PATH ?>/libs/js/autosize/autosize.min.js"></script>
    <script>
        $(function() {
            autosize($('textarea'));

            $("#submit").click(function(event) {
                $(this).addClass('hidden');
                $("#submit-preloader").removeClass('hidden');
            });
        });
    </script>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>