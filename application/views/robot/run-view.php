<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name  ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot"><i class="fa fa-gamepad"></i> Robot</a></li>
                <li class="current"><a href="#">Run report</a></li>
            </ul>

            <div class="row">

                <div class="small-12 large-12 columns">
                    <h1><i class="fa fa-file-text-o"></i> Run report</h1>
                </div> <!-- .columns -->

            </div> <!-- .row -->

            <div class="row hidden" id="run-preloader">

                <div class="small-12 large-12 columns text-center">
                    <div style="font-size: 2em; color: #a5a5a5; margin-top: 50px; padding-top: 20px; border-top: 1px solid #dcdcdc;">
                        <i class="fa fa-spinner fa-pulse fa-fw"></i>
                        <span class="sr-only">Loading...</span> Run in process
                    </div>
                    <div style="font-size: 1em; color: #a5a5a5; padding-bottom: 20px; border-bottom: 1px solid #dcdcdc; margin-top: 10px;">
                        Please wait
                    </div>
                </div> <!-- .columns -->

            </div> <!-- .row -->

            <div id="run-info" class="hidden">
                <div class="card">

                    <iframe src="<?= BASE_PATH ?>/jmeter-src/results/robot/<?= $run['report_path'] ?>/report.html"
                            style="float=left; width: 100%; height: 600px;"
                    ></iframe>

                    <div class="separator-20"></div>

                    <div>
                        <?php foreach ($run['screenshots'] as $screenshot) { ?>
                            <img class="run-screenshot" src="<?= BASE_PATH ?>/jmeter-src/results/robot/<?= $run['report_path'] ?>/<?= $screenshot ?>">
                        <?php } ?>
                    </div>

                </div> <!-- .card -->

                <div class="separator-20"></div>

                <div class="card">
                    <h5>Console Output</h5>
                    <div class="samplers-data" style="float: none;"><?= $run['console_output'] ?></div>
                    <div class="separator-20"></div>
                    <h5>Run Config</h5>
                    <div class="samplers-data" style="float: none;"><?= $run['run_config'] ?></div>
                </div> <!-- .card -->
            </div>



        </div> <!-- .column -->
    </div> <!-- .row -->

    <div class="separator-100"></div>

    <script>
        var BASE_API_PATH = "<?= BASE_API_PATH ?>";
        var appId = <?= $application->id ?>;
        var runId = <?= $run['id'] ?>;

        var checkRunStatus = function () {

            $.get( BASE_API_PATH + "/applications/" + appId + "/robot/runs/" + runId + "/get-status", function( data ) {
                var currentStatus = data.data.status.status;

                if (currentStatus == 1 || currentStatus == 2) {
                    $("#run-info").removeClass("hidden");
                } else {
                    $("#run-preloader").removeClass("hidden");
                    $("#global-preloader").removeClass("hidden");
                    setTimeout('location.reload();', 10000)
                }
            });

        };

        $(function() {
            checkRunStatus();

            $(".run-screenshot").click(function(event) {
                $(this).toggleClass("full-screen");
            });
        });
    </script>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>