<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot"><i class="fa fa-gamepad"></i> robot</a></li>
                <li class="current"><a href="#"><i class="fa fa-pie-chart"></i> Statistics</a></li>
            </ul>

            <h1><i class="fa fa-pie-chart"></i> <?= $application->name ?></h1>

            <div class="row">

                <?php $i = 0; ?>
                <?php foreach ($executorsData as $stageName => $data) { ?>
                <div class="small-12 large-4 columns end">

                        <div class="card">

                            <table class="table-no-border table-no-background" style="margin: 0;">
                                <tr>
                                    <td>
                                        <h2><?= $stageName ?></h2>
                                    </td>
                                    <td class="text-center">
                                        <?php if (isset($data['stability'])) { ?>
                                            <div>
                                                <?php if ($data['stability'] == 100) { ?>
                                                    <i class="ion-ios-sunny" style="font-size: 60px; color: green;"></i>
                                                <?php } else if ($data['stability'] >= 66) { ?>
                                                    <i class="ion-ios-partlysunny" style="font-size: 60px; color: #009dff;"></i>
                                                <?php } else if ($data['stability'] >= 33 && $data['stability'] < 66) { ?>
                                                    <i class="ion-ios-rainy" style="font-size: 60px; color: #ffa900;"></i>
                                                <?php } else if ($data['stability'] < 33) { ?>
                                                    <i class="ion-ios-thunderstorm" style="font-size: 60px; color: red;"></i>
                                                <?php } ?>
                                            </div>
                                            <div>Stability: <?= $data['stability'] ?>%</div>
                                        <?php } else { ?>
                                            <div class="text-small text-muted">No data</div>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="separator-10"></div>

                </div>

                    <?php if ($i != 0 && $i++ % 3 == 0) { ?>
                        <div class="small-12 large-12 columns"></div>
                    <?php } ?>
                <?php } ?>

            </div>

        </div>
    </div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>