<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>"><i class="fa fa-cubes"></i> App: <?= $application->name  ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>/robot"><i class="fa fa-gamepad"></i> Robot</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>/robot/executors"><i class="fa fa fa-code"></i> Executors</a></li>
                <li class="current"><a href="#">#<?= $executor->id ?>. <?= $executor->name ?></a></li>
            </ul>

            <h1><i class="fa fa-terminal"></i> Executor: <?= $executor->name ?></h1>
            <hr/>

            <div class="row">

                <div class="small-9 large-9 columns">

                    <form action="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors/<?= $executor->id ?>/update" method="post"
                          enctype="application/x-www-form-urlencoded">

                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" value="<?= $executor->name ?>">

                        <?php
                        $icons = array(
                            '&#xf268 chrome' => 'fa fa-chrome',
                            '&#xf269 firefox' => 'fa fa-firefox',
                            '&#xf005 ie' => 'fa fa-internet-explorer',
                            '&#xf005 star' => 'fa fa-star',
                            '&#xf056 blocked' => 'fa fa-minus-circle',
                            '&#xf1b2 cube' => 'fa fa-cube',
                            '&#xf0ac globe' => 'fa fa-globe',
                        )
                        ?>
                        <label for="ico">Icon</label>
                        <select name="ico" id="ico" style="font-family: 'FontAwesome'">
                            <option value="">no icon</option>
                            <?php foreach ($icons as $icoName => $icoClass) { ?>
                                <option value="<?= $icoClass ?>" <?= $icoClass == $executor->ico ? 'selected' : '' ?>><?= $icoName ?></option>
                            <?php } ?>
                        </select>

                        <label for="description">Description</label>
                        <textarea id="description" name="description"><?= $executor->description ?></textarea>

                        <label for="config">Config</label>
                        <textarea id="config" name="config"><?= $executor->config ?></textarea>

                        <input type="submit" class="small button" id="submit" value="Save">

                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom hidden" id="submit-preloader"></i>
                        <span class="sr-only">Loading...</span>
                    </form>

                </div>

                <div class="small-3 large-3 columns text-right">
                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors"
                       class="button small alert" id="delete-bt">
                        Delete
                    </a>
                </div>

            </div>

            <hr/>

            <div class="row">

                <div class="small-12 large-12 columns">

                    <h2>Tests</h2>

                    <div class="row">
                        <div class="small-12 large-6 columns">
                            <div class="input-group">
                                <span class="input-group-label"><i class="fa fa-search"></i></span>
                                <input type="text" id="searchField" placeholder="Search test" class="input-group-field">
                            </div>
                        </div>
                    </div>

                    <table>

                        <thead>
                        <tr>
                            <th></th>
                            <th>Test</th>
                            <th>Description</th>
                        </tr>
                        </thead>

                        <tbody id="testsTable">
                        <tr>
                            <td class="width-1">
                                <input type="checkbox" value="all" id="all-check" class="test-checkbox" checked>
                            </td>
                            <td>
                                All
                            </td>
                            <td></td>
                        </tr>
                        <?php foreach ($tests as $test) { ?>
                            <tr>
                                <td>
                                    <input type="checkbox" class="test-checkbox" value="<?= $test['name'] ?>">
                                </td>
                                <td>
                                    <?= $test['name'] ?>
                                </td>
                                <td class="text-small text-muted">
                                    <pre><?= $test['description'] ?></pre>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>

                    </table>

                    <div>
                        <a href="#"
                           class="button small success" id="run-bt">
                            Run
                        </a>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div class="separator-100"></div>

    <script src="<?= BASE_PATH ?>/libs/js/autosize/autosize.min.js"></script>
    <script>
        $(function() {
            var BASE_PATH = "<?= BASE_PATH ?>";
            var appId = <?= $application->id ?>;
            var execId = <?= $executor->id ?>;

            autosize($('textarea'));

            $("#submit").click(function(event) {
                $(this).addClass('hidden');
                $("#submit-preloader").removeClass('hidden');
            });

            $(".copy-button").click(function(event) {
                $("input[name='" + $(this).data("key") + "']").val($(this).next("span").text());
            });

            $("#run-bt").click(function(event) {
                 event.preventDefault();

                 if (confirm("Run tests?")) {
                     var params = [];
                     var selectedTests = $(".test-checkbox");
                     for (var i = 0; i < selectedTests.length; i++) {
                         if ($(selectedTests[i]).is(':checked')) {
                             params.push($(selectedTests[i]).val());
                         }
                     }

                     $.get(BASE_PATH + "/applications/" + appId + "/robot/executors/" + execId + "/performRun", {"params": params}, function(data) {
                         window.location.href = BASE_PATH + "/applications/" + data.data.appId + "/robot/runs/" + data.data.execRunId;
                     });
                 }
            });

            $(".test-checkbox").click(function(event) {
                if ($(this).val() != 'all') {
                    $('#all-check').prop('checked', false);
                }

                var selectedTests = $(".test-checkbox");
                var isSelectedExist = false;
                for (var i = 0; i < selectedTests.length; i++) {
                    if ($(selectedTests[i]).is(':checked')) {
                        isSelectedExist = true;
                        break;
                    }
                }

                if (!isSelectedExist) {
                    $('#all-check').prop('checked', true);
                }
            });

            $("#delete-bt").click(function(event) {
                event.preventDefault();

                if (confirm("Delete executor?")) {
                    $.get(BASE_PATH + "/applications/" + appId + "/robot/executors/" + execId + "/delete", function(data) {
                        window.location.href = BASE_PATH + "/applications/" + appId + "/robot/executors";
                    });
                }
            });

            // Declare variables for search in tests table
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchField");
            table = document.getElementById("testsTable");
            tr = table.getElementsByTagName("tr");

            $("#searchField").keyup(function(event) {
                // Loop through all table rows, and hide those who don't match the search query

                filter = input.value.toUpperCase();
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[1];
                    if (td) {
                        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            });

        });
    </script>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>