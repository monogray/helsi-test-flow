<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="grid-x grid-padding-x">
        <div class="cell">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>"><i class="fa fa-cubes"></i> App: <?= $application->name  ?></a></li>
                <li class="current"><a href="#">Robot</a></li>
            </ul>

            <div class="grid-x grid-padding-x">

                <div class="small-12 large-12 cell">
                    <h1><i class="fa fa-gamepad"></i> Robot</h1>
                </div> <!-- .cell -->

            </div> <!-- .grid-x -->

            <hr>

            <div class="grid-x grid-padding-x">

                <div class="small-6 large-6 cell">
                    <div>
                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors"
                           class="button small success">
                            <i class="fa fa-code"></i> Executors
                        </a>
                    </div>
                </div> <!-- .cell -->

                <div class="small-6 large-6 cell">
                    <div class="text-right">
                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/dashboard"
                           class="button small secondary disabled">
                            <i class="fa fa-pie-chart"></i> Dashboard
                        </a>
                    </div>
                </div> <!-- .cell -->

            </div> <!-- .grid-x -->

            <hr>
            <h4>Run Reports</h4>

            <div class="card">

                <table>

                    <thead>
                    <tr>
                        <td class="width-3 text-right text-muted">#</td>
                        <td class="width-5">Status</td>
                        <td>Executor</td>
                        <td>Report</td>
                        <td class="width-15 text-right">Date</td>
                        <td class="width-15 text-right">Actions</td>
                    </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($runs as $id => $run) { ?>
                            <tr>
                                <td class="text-right text-muted text-small">
                                    <?= $id ?>
                                </td>
                                <td class="text-center">
                                    <?php
                                    if ($run['status'] == 2) {
                                        echo '<div class="success label" style="width: 100%">pass</div>';
                                    } else if ($run['status'] == 1) {
                                        echo '<div class="alert label" style="width: 100%">fail</div>';
                                    } else if ($run['status'] == 0) {
                                        echo '<div class="label secondary" style="width: 100%">n/a</div>';
                                    }
                                    ?>
                                </td>
                                <td class="text-muted">
                                    <?= $executors[$run['exec_id']]['name'] ?>
                                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/executors/<?= $run['exec_id'] ?>/edit"><i class="fa fa-external-link-square"></i></a>
                                </td>
                                <td class="text-muted">
                                    <?php if ($run['status'] != 0) { ?>
                                        <a href="<?= BASE_PATH ?>/jmeter-src/results/robot/<?= $executorsData[$id]['report_path'] ?>/report.html" target="_blank">link</a>
                                    <?php } ?>
                                </td>
                                <td class="text-muted text-small text-right">
                                    <?= date('D d-m-Y H:i:s', strtotime($run['d_create'])) ?>
                                </td>
                                <td class="text-right">
                                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot/runs/<?= $id ?>"
                                       class="secondary small button">View</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div> <!-- .card -->

        </div> <!-- .cell -->
    </div> <!-- .grid-x -->

    <div class="separator-100"></div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>