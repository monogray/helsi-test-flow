<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <section class="site-section qa-dashboard-slide-1">

        <div class="slide-container">
            <h1 class="caption">QA Team Dashboard</h1>
            <div class="caption" style="font-size: 1.1em">HELSI: HIS, PIS</div>
        </div>

    </section>

    <div class="separator-30"></div>

    <section>

        <div class="row">
            <div class="small-12 large-12 columns text-center">
                <a href="<?= BASE_PATH ?>/qa-dashboard" class="button small" style="width: 100px">QA</a>
                <a href="<?= BASE_PATH ?>/qa-dashboard" class="button small secondary" style="width: 100px">PM</a>
                <a href="<?= BASE_PATH ?>/qa-dashboard" class="button small secondary" style="width: 100px">Docs</a>
            </div>
        </div>

    </section>

    <div class="separator-30"></div>

    <section>

        <div class="row default-row">
            <div class="small-12 large-12 columns">
                <h1>Current sprint: #21</h1>
                <a href="https://miswiki.atlassian.net/wiki/pages/viewpage.action?pageId=72223977"
                   class="button secondary small"
                   target="_blank">Goals</a>
                <a href="https://miswiki.atlassian.net/secure/RapidBoard.jspa?rapidView=4"
                   class="button secondary small"
                   target="_blank">Board</a>

                <hr>

                <div class="row">
                    <div class="small-8 large-8 columns">
                        <h2>Sprint Manifest</h2>
                    </div>
                    <div class="small-4 large-4 columns text-right">
                        <a href="https://docs.google.com/document/d/1FWc5tANVifv_MmUXuw3i8H5aEs5-HXVLDKQaJPvyP6U/edit#"
                           class="button secondary small"
                           style="margin-right: -30px; margin-top: 3px;"
                           target="_blank">open <i class="fa fa-external-link"></i></a>
                    </div>
                </div>

                <iframe style="
                            width: 100%;
                            height: 500px;
                            border: none;
                            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);"
                        src="https://docs.google.com/document/d/1FWc5tANVifv_MmUXuw3i8H5aEs5-HXVLDKQaJPvyP6U/pub?embedded=true">
                </iframe>

                <div class="separator-20"></div>

                <div class="card">
                    <h2>Help Desk Engineers</h2>

                    <table>
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Engineer</th>
                            <th>Second Engineer</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>
                                27.02
                            </td>
                            <td>
                                Оксана
                            </td>
                            <td>
                                Іванка
                            </td>
                        </tr>
                        <tr>
                            <td>
                                27.02
                            </td>
                            <td>
                                Оксана
                            </td>
                            <td>
                                Іванка
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="card">
                    <h2>Documents</h2>

                    <table>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Link</th>
                            <th>Description</th>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <td>
                                HELSI QA Strategy
                            </td>
                            <td>
                                <a href="https://docs.google.com/document/d/1VqinDo1b8UZUCUaf5ZHbtRaWTMBhOunUjnB69sVmgI4/edit#"
                                   target="_blank">
                                    open
                                </a>
                            </td>
                            <td>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                QA Team Workflow
                            </td>
                            <td>
                                <a href="https://i.imgur.com/5may7rK.png"
                                   target="_blank">
                                    open
                                </a>
                            </td>
                            <td>

                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </section>

    <div class="separator-100"></div>


<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>