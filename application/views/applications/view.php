<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="grid-x grid-padding-x">
        <div class="cell">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li class="current"><a href="#">App: <?= $application->name ?></a></li>
            </ul>

            <div class="grid-x grid-padding-x">

                <div class="small-8 large-8 cell">
                    <a href="<?= BASE_PATH ?>/dashboard" class="back-button"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    <h1><i class="fa fa-cubes"></i> App: <?= $application->name ?></h1>
                </div> <!-- .cell -->

                <div class="small-4 large-4 cell text-right" style="padding-top: 15px">
                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/edit"
                       class="button small secondary">
                        Edit
                    </a>

                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/map"
                       class="button small secondary">
                        App Map
                    </a>
                </div> <!-- .cell -->

                <?php if ($application->description != '') { ?>
                <div class="small-12 large-12 cell">
                    <div class="text-small text-muted info-container">
                        <pre><?= $application->description ?></pre>
                    </div>
                </div> <!-- .cell -->
                <?php } ?>

            </div> <!-- .grid-x -->

            <div class="grid-x grid-padding-x">

                <div class="small-12 large-12 cell top-menu">
                    <hr>
                    <div>
                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/dashboard"
                           class="button small secondary disabled">
                            <i class="fa fa-tachometer" aria-hidden="true"></i> App Dashboard
                        </a>

                        <div class="button small menu-v-separator">&nbsp;</div>

                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/test-projects"
                           class="button small secondary">
                            <i class="fa fa-cube" aria-hidden="true"></i> Test Projects (<?= $testProjectsCount ?>)
                        </a>
                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/stages"
                           class="button small secondary">
                            <i class="fa fa-database" aria-hidden="true"></i> Stages (<?= $stagesCount ?>)
                        </a>

                        <div class="button small menu-v-separator">&nbsp;</div>

                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/components"
                           class="button small secondary disabled">
                            <i class="fa fa-puzzle-piece" aria-hidden="true"></i> Components
                        </a>

                        <div class="button small menu-v-separator">&nbsp;</div>

                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/robot"
                           class="button small">
                            <i class="fa fa-gamepad" aria-hidden="true"></i> Robot
                        </a>
                    </div>
                    <hr>
                </div> <!-- .cell -->

            </div> <!-- .grid-x -->

            <h2>Reports</h2>

            <div class="card">

                <table>

                    <thead>
                    <tr>
                        <td class="width-3 text-right text-muted">#</td>
                        <td class="width-5">Status</td>
                        <td class="width-10">Statistic</td>
                        <td>
                            <div class="rogrid-xw">

                                <div class="small-2 large-2 cell">
                                    <div style="margin-top: 10px">Stage</div>
                                </div> <!-- .cell -->

                                <!-- Stages filter -->
                                <div class="small-10 large-10 cell">
                                    <select id="stages-list">
                                        <option value="-1" selected>All stages</option>
                                        <?php foreach ($stages as $stage) { ?>
                                            <option value="<?= $stage['id'] ?>"><?= $stage['name'] ?></option>
                                        <?php } ?>
                                    </select>

                                </div> <!-- .cell -->
                                <!-- END Stages filter -->

                            </div> <!-- .grid-x -->
                        </td>
                        <td class="width-10 text-right">Avg Time, ms</td>
                        <td class="width-10 text-right">Min Time, ms</td>
                        <td class="width-10 text-right">Max Time, ms</td>
                        <td class="width-15 text-right">Date</td>
                        <td class="width-15 text-right">Actions</td>
                    </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($runs as $run) { ?>
                            <tr>
                                <td class="text-right text-muted text-small">
                                    <?= $run->id ?>
                                </td>
                                <td class="text-center">
                                    <?= $run->status == 1 ? '<div class="success label" style="width: 100%">pass</div>' : '<div class="alert label" style="width: 100%">fail</div>' ?>
                                </td>
                                <td style="opacity: .7">
                                    <div class="label info"><?= $run->samples ?></div>
                                    <div class="label <?= $run->passCount > 0 ? 'success' : 'secondary'?>"><?= $run->passCount ?></div>
                                    <div class="label <?= $run->errorCount > 0 ? 'alert' : 'secondary'?>"><?= $run->errorCount ?></div>
                                </td>
                                <td class="text-muted">
                                    <?php if (isset($stages[$run->stageId])) { ?>
                                        <?= $stages[$run->stageId]->name ?>
                                        <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/stages/<?= $run->stageId ?>/edit"><i class="fa fa-external-link-square"></i></a>
                                    <?php } else { ?>
                                        n/a
                                    <?php } ?>
                                </td>
                                <td class="text-muted text-right">
                                    <?= $run->averageTime ?>
                                </td>
                                <td class="text-muted text-right">
                                    <?= $run->minTime ?>
                                </td>
                                <td class="text-right">
                                    <?= $run->maxTime ?>
                                </td>
                                <td class="text-muted text-small text-right">
                                    <?= date('D d-m-Y H:i:s', strtotime($run->dCreate)) ?>
                                </td>
                                <td class="text-right">
                                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/runs/<?= $run->id ?>"
                                       class="secondary small button">View</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div> <!-- .card -->

        </div> <!-- .cell -->
    </div> <!-- .grid-x -->

    <div class="separator-100"></div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>