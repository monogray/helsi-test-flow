<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li class="current"><a href="#">Edit</a></li>
            </ul>

            <h1><i class="fa fa-sitemap"></i> App Map</h1>
            <hr/>

            <div class="row">
                <div class="small-12 large-12 columns">

                    <ul>

                        <li>
                            <i class="fa fa-cubes"></i> Application: <?= $application->name ?>
                            <ul>

                                <li>
                                    <i class="fa fa-cube"></i> Test Projects (<?= $testProjectsCount ?>)
                                    <ul>
                                        <?php foreach ($projects as $project) { ?>
                                            <li>
                                                <?= $project->name ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>

                                <li>
                                    <i class="fa fa-database" aria-hidden="true"></i> Stages (<?= $stagesCount ?>)
                                    <ul>
                                        <?php foreach ($stages as $stage) { ?>
                                            <li>
                                                <?= $stage->name ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>

                            </ul>
                        </li>

                    </ul>

                </div>
            </div>

        </div>
    </div>

    <div class="separator-100"></div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>