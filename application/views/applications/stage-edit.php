<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/stages"><i class="fa fa-database"></i> Stages</a></li>
                <li class="current"><a href="#">Stage: <?= $stage->name ?></a></li>
            </ul>

            <h1><i class="fa fa-database" aria-hidden="true"></i> Stage: <?= $stage->name ?></h1>

            <div class="row">
                <div class="small-12 large-12 columns">

                    <div class="card">

                        <form action="<?= BASE_PATH ?>/applications/<?= $application->id ?>/stages/<?= $stage->id ?>/update" method="post"
                              enctype="application/x-www-form-urlencoded">

                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" value="<?= $stage->name ?>">

                            <h4>Run parameters</h4>

                            <table>
                                <thead>
                                <tr>
                                    <td class="width-5">Key</td>
                                    <td>Stage value</td>
                                    <td class="width-33">Project's default values</td>
                                </tr>
                                </thead>

                                <tbody>

                                <?php $stage->fields = json_decode($stage->fields, TRUE) ?>
                                <?php foreach ($projectsVariables as $variableKey => $variable) { ?>
                                    <tr>
                                        <td class="text-right">
                                            <label for="<?= $variableKey ?>"><?= $variableKey ?></label>
                                        </td>
                                        <td>
                                            <input type="text" name="<?= $variableKey ?>" id="<?= $variableKey ?>" placeholder="<?= $variableKey ?>" value="<?= isset($stage->fields[$variableKey]) ? $stage->fields[$variableKey] : '' ?>" style="margin: 0">
                                        </td>
                                        <td>
                                            <?php foreach ($variable as $val) { ?>
                                                <div style="margin-bottom: 1px">
                                                    <i class="copy-button fa fa-angle-double-left label secondary"
                                                       data-key="<?= $variableKey ?>"
                                                       style="cursor: pointer">

                                                    </i>&nbsp;&nbsp;&nbsp;&nbsp;<span><?= $val ?></span>
                                                </div>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                            <input type="submit" class="small button" id="submit" value="Save">

                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom hidden" id="submit-preloader"></i>
                            <span class="sr-only">Loading...</span>

                        </form>

                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="separator-100"></div>

    <script>
        $(function() {
            $("#submit").click(function(event) {
                $(this).addClass('hidden');
                $("#submit-preloader").removeClass('hidden');
            });

            $(".copy-button").click(function(event) {
                $("input[name='" + $(this).data("key") + "']").val($(this).next("span").text());
            });
        });
    </script>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>