<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id  ?>"><i class="fa fa-cubes"></i> App: <?= $application->name  ?></a></li>
                <li class="current"><a href="#">#<?= $appRun->id ?>. Run</a></li>
            </ul>

            <div class="row">

                <div class="small-8 large-4 columns">
                    <h1>
                        <span class="label secondary" style="font-size: 2.5rem;">#<?= $appRun->id ?></span> Run
                    </h1>
                    <div class="text-muted text-small text-left">
                        <?= date('D d-m-Y H:i:s', strtotime($appRun->dCreate)) ?>
                    </div>
                </div> <!-- .columns -->

                <div class="small-4 large-8 columns">

                    <table>

                        <thead>
                        <tr>
                            <td class="width-5">Status</td>
                            <td class="width-15">Statistic</td>
                            <td>Stage</td>
                            <td class="width-15 text-right">Avg Time, ms</td>
                            <td class="width-15 text-right">Min Time, ms</td>
                            <td class="width-15 text-right">Max Time, ms</td>
                        </tr>
                        </thead>

                        <tbody>
                        <tr style="height: 40px">
                            <td class="text-center">
                                <?= $appRun->status == 1 ? '<div class="success label" style="width: 100%">pass</div>' : '<div class="alert label" style="width: 100%">fail</div>' ?>
                            </td>
                            <td style="opacity: .7">
                                <div class="label info"><?= $appRun->samples ?></div>
                                <div class="label <?= $appRun->passCount > 0 ? 'success' : 'secondary'?>"><?= $appRun->passCount ?></div>
                                <div class="label <?= $appRun->errorCount > 0 ? 'alert' : 'secondary'?>"><?= $appRun->errorCount ?></div>
                            </td>
                            <td class="text-muted">
                                <?= isset($stages[$appRun->stageId]) ? $stages[$appRun->stageId]->name : 'n/a' ?>
                            </td>
                            <td class="text-muted text-right">
                                <?= $appRun->averageTime ?>
                            </td>
                            <td class="text-muted text-right">
                                <?= $appRun->minTime ?>
                            </td>
                            <td class="text-muted text-right">
                                <?= $appRun->maxTime ?>
                            </td>
                        </tr>
                        </tbody>

                    </table>

                </div> <!-- .columns -->

            </div> <!-- .row -->

            <hr>

            <div class="card">

                <h4>Test Projects</h4>

                <table>

                    <thead>
                    <tr>
                        <td class="width-3 text-right">#</td>
                        <td class="width-5">Status</td>
                        <td class="width-10">Statistic</td>
                        <td>Test Projects</td>
                        <td class="width-10 text-right">Avg Time, ms</td>
                        <td class="width-10 text-right">Min Time, ms</td>
                        <td class="width-10 text-right">Max Time, ms</td>
                        <td class="width-15 text-right">Actions</td>
                    </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($runs as $run) { ?>
                            <?php $runId = $run['id'] ?>
                            <tr>
                                <td class="text-right text-muted text-small">
                                    <?= $runId ?>
                                </td>
                                <td class="text-center">
                                    <?= $run['status'] == 1 ? '<div class="success label" style="width: 100%">pass</div>' : '<div class="alert label" style="width: 100%">fail</div>' ?>
                                </td>
                                <td style="opacity: .7">
                                    <div class="label info"><?= $runsStatistics[$runId]['all'] ?></div>
                                    <div class="label <?= $runsStatistics[$runId]['pass'] > 0 ? 'success' : 'secondary'?>"><?= $runsStatistics[$runId]['pass'] ?></div>
                                    <div class="label <?= $runsStatistics[$runId]['fail'] > 0 ? 'alert' : 'secondary'?>"><?= $runsStatistics[$runId]['fail'] ?></div>
                                </td>
                                <td class="text-muted">
                                    <?= $projects[$run['p_id']]['name'] ?>
                                    <a href="<?= BASE_PATH ?>/projects/<?= $projects[$run['p_id']]['id'] ?>"><i class="fa fa-external-link-square"></i></a>
                                </td>
                                <td class="text-muted text-right">
                                    <?= $runsStatistics[$runId]['average_time'] ?>
                                </td>
                                <td class="text-muted text-right">
                                    <?= $runsStatistics[$runId]['min_time'] ?>
                                </td>
                                <td class="text-muted text-right">
                                    <?= $runsStatistics[$runId]['max_time'] ?>
                                </td>
                                <td class="text-right">
                                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/runs/<?= $appRun->id ?>/reports/<?= $runId ?>"
                                       class="secondary small button">View</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>

                </table>

            </div> <!-- .card -->

        </div> <!-- .column -->
    </div> <!-- .row -->

    <div class="separator-100"></div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>