<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li class="current"><a href="#">Edit</a></li>
            </ul>

            <h1><i class="fa fa-cubes"></i> App: <?= $application->name ?></h1>
            <hr/>

            <div class="row">
                <div class="small-12 large-12 columns">
                    <form action="<?= BASE_PATH ?>/applications/<?= $application->id ?>/edit" method="post"
                          enctype="application/x-www-form-urlencoded">

                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" value="<?= $application->name ?>">

                        <label for="description">Description</label>
                        <textarea name="description" id="description"><?= $application->description ?></textarea>

                        <input type="submit" class="small button" id="submit" value="Save">

                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom hidden" id="submit-preloader"></i>
                        <span class="sr-only">Loading...</span>

                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="separator-100"></div>

    <script>
        $(function() {
            $("#submit").click(function(event) {
                $(this).addClass('hidden');
                $("#submit-preloader").removeClass('hidden');
            });
        });
    </script>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>