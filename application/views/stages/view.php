<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="grid-x grid-padding-x">
        <div class="cell">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/test-projects"><i class="fa fa-cube"></i> Test projects</a></li>
                <li><a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>"><i class="fa fa-cube"></i> <?= $project->name ?></a></li>
                <li><a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>/stages"><i class="fa fa-database"></i> Stages</a></li>
                <li class="current"><a href="#">Stage: <?= $stage->name ?></a></li>
            </ul>

            <h1><i class="fa fa-database" aria-hidden="true"></i> Stage: <?= $stage->name ?></h1>
            <hr/>

            <div class="grid-x grid-padding-x">
                <div class="small-12 large-12 cell">
                    <form action="<?= BASE_PATH ?>/projects/<?= $project->id ?>/stages/<?= $stage->id ?>/update" method="post"
                          enctype="application/x-www-form-urlencoded">

                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" value="<?= $stage->name ?>">

                        <p>Run parameters</p>

                        <table>
                            <thead>
                            <tr>
                                <td class="width-5">Key</td>
                                <td>Stage value</td>
                                <td class="width-33">Project's default value</td>
                            </tr>
                            </thead>

                            <tbody>

                            <?php $stage->fields = json_decode($stage->fields, TRUE) ?>
                            <?php foreach ($projectVariables as $variableKey => $variable) { ?>
                                <tr>
                                    <td class="text-right">
                                        <label for="<?= $variableKey ?>"><?= $variableKey ?></label>
                                    </td>
                                    <td>
                                        <input type="text" name="<?= $variableKey ?>" id="<?= $variableKey ?>" placeholder="<?= $variableKey ?>" value="<?= isset($stage->fields[$variableKey]) ? $stage->fields[$variableKey] : '' ?>" style="margin: 0">
                                    </td>
                                    <td>
                                        <i class="copy-button fa fa-angle-double-left label secondary"
                                           data-key="<?= $variableKey ?>"
                                           style="cursor: pointer"></i>&nbsp;&nbsp;&nbsp;&nbsp;<span><?= $variable ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                        <input type="submit" class="small button" id="submit" value="Save">

                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom hidden" id="submit-preloader"></i>
                        <span class="sr-only">Loading...</span>

                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="separator-100"></div>

    <script>
        $(function() {
            $("#submit").click(function(event) {
                $(this).addClass('hidden');
                $("#submit-preloader").removeClass('hidden');
            });

            $(".copy-button").click(function(event) {
                $("input[name='" + $(this).data("key") + "']").val($(this).next("span").text());
            });
        });
    </script>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>