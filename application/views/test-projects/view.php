<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row">
        <div class="column align-self-middle">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li class="current"><a href="#">Test Projects</a></li>
            </ul>

            <div class="row">

                <div class="small-8 large-8 columns">
                    <a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>" class="back-button"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    <h1><i class="fa fa-cube"></i> Test Projects (<?= count($projects) ?>)</h1>
                </div> <!-- .columns -->

                <div class="small-4 large-4 columns text-right" style="padding-top: 15px">
                </div> <!-- .columns -->

                <div class="small-12 large-12 columns">
                    <div class="text-small text-muted info-container">
                        List of Test Projects for current application.
                    </div>
                </div> <!-- .columns -->

            </div> <!-- .row -->

            <div class="row">

                <div class="small-12 large-12 columns">
                    <hr>
                </div> <!-- .columns -->

            </div> <!-- .row -->

            <div class="card">

                <table>
                    <thead>
                    <tr>
                        <td class="width-2 text-right text-muted">#</td>
                        <td>Test Projects</td>
                        <td class="width-33 text-right">Actions</td>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($projects as $file) { ?>
                        <tr>
                            <td class="text-right text-small text-muted"><?= $file->id ?></td>
                            <td><a href="<?= BASE_PATH ?>/projects/<?= $file->id ?>"><?= $file->name ?></a></td>
                            <td class="text-right">
                                <a href="<?= BASE_PATH ?>/projects/<?= $file->id ?>" class="button small secondary">View</a>
                                <a href="<?= BASE_PATH ?>/projects/<?= $file->id ?>/reports/last/<?= APP_PUBLIC_KEY ?>" class="button small secondary">Last report</a>
                                <a href="<?= BASE_PATH ?>/projects/<?= $file->id ?>/perform-run" class="button small success">Run now</a>
                                <a href="<?= BASE_PATH ?>/projects/<?= $file->id ?>/run" class="button small">Run...</a>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>

            </div> <!-- .card -->

        </div> <!-- .column -->
    </div> <!-- .row -->

    <div class="separator-100"></div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>