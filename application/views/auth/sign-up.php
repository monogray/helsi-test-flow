<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="row default-row">
        <div class="column align-self-middle">

            <div class="row">

                <div class="small-12 medium-2 large-1 columns">
                    &nbsp;
                </div>

                <div class="small-12 medium-8 large-4 columns end">

                    <div class="separator-20"></div>

                    <div class="card">
                        <h1>Sign-Up</h1>

                        <form enctype="application/x-www-form-urlencoded" method="post">
                            <label for="name">E-mail</label>
                            <input type="email" placeholder="E-mail" id="email" name="email" maxlength="255">

                            <label for="password">Password</label>
                            <input type="password" placeholder="Password" id="password" name="password" maxlength="255">

                            <label for="repeat-password">Repeat password</label>
                            <input type="password" placeholder="Repeat password" id="repeat-password" name="repeat-password" maxlength="255">

                            <label for="name">Name</label>
                            <input type="text" placeholder="Name" id="name" name="name" maxlength="255">

                            <label for="company">Company</label>
                            <input type="text" placeholder="Company" id="company" name="company" maxlength="255">

                            <label for="alert-email">Alert e-mail</label>
                            <input type="email" placeholder="Alert e-mail" id="alert-email" name="alert-email" maxlength="255">

                            <a href="<?= BASE_PATH ?>/auth/login" style="float: left; margin-top: 9px">Sign-In</a>
                            <input type="submit" class="button" value="Sign-Up" style="float: right">
                        </form>

                        <div class="separator-50"></div>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div class="separator-100"></div>

    <style>
        body {
            background-image: url("<?= BASE_PATH ?>/public/images/backgrounds/quality-assurance-login-background.jpg");
            background-position: center;
            background-size: cover;
        }
    </style>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>