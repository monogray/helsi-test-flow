<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="grid-x grid-padding-x">
        <div class="cell">

            <div class="grid-x grid-padding-x">

                <div class="small-12 medium-2 large-1 cell">
                    &nbsp;
                </div>

                <div class="small-12 medium-8 large-4 cell">

                    <div class="separator-50"></div>

                    <div class="card">
                        <div class="wrap">
                            <h1>Sign-In</h1>

                            <form enctype="application/x-www-form-urlencoded" method="post">
                                <label for="login">E-mail</label>
                                <input type="text" placeholder="E-mail" name="login" id="login" maxlength="255" value="<?= isset($login) ? $login : '' ?>">

                                <label for="password">Password</label>
                                <input type="password" placeholder="password" name="password" id="password" maxlength="255">

                                <input class="secondary button" type="submit" value="Sign-In" style="float: right">

                                <a href="<?= BASE_PATH ?>/auth/sign-up" style="float: left; margin-top: 9px">Sign-Up</a>
                            </form>

                        </div>

                        <div class="separator-50"></div>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div class="separator-100"></div>

<style>
    body {
        background-image: url("<?= BASE_PATH ?>/public/images/backgrounds/quality-assurance-login-background.jpg");
        background-position: center;
        background-size: cover;
    }
</style>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>