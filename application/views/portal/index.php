<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <section class="site-section slide-1">

        <div class="slide-container">
            <h1 class="caption">Test Flow</h1>
            <div class="caption text-middle-size">Test Automation Platform</div>

            <div class="separator-60"></div>
            <div class="caption">
                <?php if (isset($_SESSION['Auth_Auto_App_Status']) && $_SESSION['Auth_Auto_App_Status'] != '') { ?>
                    <a href="<?= BASE_PATH ?>/dashboard" class="button secondary">To Dashboard</a>
                <?php } else { ?>
                    <a href="<?= BASE_PATH ?>/auth/login" class="button secondary">LOGIN</a>
                <?php } ?>
            </div>
        </div>

    </section>

    <section>
        <div class="separator-100"></div>

        <div class="grid-x grid-padding-x text-center">
            <div class="small-12 medium-6 large-4 cell">
                <img src="<?= BASE_PATH ?>/public/images/backgrounds/moon_3.png">
                <div class="separator-30"></div>
                <h5>API Automation</h5>
                <p class="text-muted">
                    Web tool for run automation tests<br> for API and Web Services
                </p>
            </div> <!-- .column -->
            <div class="small-12 medium-6 large-4 cell">
                <img src="<?= BASE_PATH ?>/public/images/backgrounds/moon_2.png">
                <div class="separator-30"></div>
                <h5>JMeter Based</h5>
                <p class="text-muted">
                    Lorem ipsum dolor sit amet, consectetur<br> adipiscing elit. Vivamus porta pretium ex non aliquam.
                </p>
            </div> <!-- .column -->
            <div class="small-12 medium-6 large-4 cell">
                <img src="<?= BASE_PATH ?>/public/images/backgrounds/moon_1.png">
                <div class="separator-30"></div>
                <h5>CI Integration</h5>
                <p class="text-muted">
                    Lorem ipsum dolor sit amet, consectetur<br> adipiscing elit. Vivamus porta pretium ex non aliquam.
                </p>
            </div> <!-- .column -->
        </div> <!-- .row -->
        <div class="separator-100"></div>
    </section>

    <section class="site-section slide-2">

        <div class="slide-container">
            <h1 class="caption"></h1>
            <div class="caption text-middle-size"></div>

            <div class="separator-60"></div>
            <div class="caption">

            </div>
        </div>

    </section>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>