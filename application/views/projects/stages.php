<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="grid-x grid-padding-x">
        <div class="cell">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/test-projects"><i class="fa fa-cube"></i> Test projects</a></li>
                <li><a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>"><i class="fa fa-cube"></i> <?= $project->name ?></a></li>
                <li class="current"><a href="#">Stages</a></li>
            </ul>

            <div class="grid-x grid-padding-x">

                <div class="small-8 large-8 cell">
                    <h1><i class="fa fa-database"></i> Stages</h1>
                </div> <!-- .cell -->

                <div class="small-4 large-4 cell text-right">
                    <div style="margin-top: 15px;">
                        <a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>/stages/add"
                           class="button small secondary"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>
                    </div>
                </div> <!-- .cell -->

            </div> <!-- .grid-x -->

            <div class="card">
                <table>

                    <thead>
                    <tr>
                        <td class="width-3 text-right text-muted">#</td>
                        <td>Name</td>
                        <td class="width-3">Default</td>
                        <td class="width-15 text-right">Actions</td>
                    </tr>
                    </thead>

                    <tbody>

                    <?php if (count($stages) == 0) { ?>
                        <tr>
                            <td colspan="4" class="text-muted">No stages created.</td>
                        </tr>
                    <?php } ?>

                    <?php foreach ($stages as $stage) { ?>
                        <tr>
                            <td class="text-right text-muted text-small">
                                <?= $stage->id ?>
                            </td>
                            <td>
                                <a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>/stages/<?= $stage->id ?>"><?= $stage->name ?></a>
                            </td>
                            <td class="text-center">
                                <input disabled type="radio" name="group1" value="<?= $stage->id ?>" <?= $stage->isDefault == 1 ? 'checked' : '' ?>>
                            </td>
                            <td class="text-right">
                                <a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>/stages/<?= $stage->id ?>"
                                   class="secondary small button">Edit</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>

                </table>
            </div> <!-- .card -->

        </div>
    </div>

    <div class="separator-100"></div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>