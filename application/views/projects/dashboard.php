<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="grid-x grid-padding-x">
        <div class="cell">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li><a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>"><i class="fa fa-cube"></i> <?= $project->name ?></a></li>
                <li class="current"><a href="#"><i class="fa fa-pie-chart"></i> Statistics</a></li>
            </ul>

            <h1><i class="fa fa-pie-chart"></i> <?= $project->name ?></h1>

            <div class="grid-x grid-padding-x">

                <?php $i = 0; ?>
                <?php foreach ($stages as $stage) { ?>
                <div class="small-12 large-4 cell">

                        <div class="card">

                            <table class="table-no-border table-no-background" style="margin: 0;">
                                <tr>
                                    <td>
                                        <h2><?= $stage->name ?></h2>
                                        <div class="text-small text-muted"><?= json_decode($stage->fields, TRUE)['host'] ?></div>
                                    </td>
                                    <td class="text-center">
                                        <?php if (isset($stagesStatistics[$stage->id])) { ?>
                                            <div>
                                                <?php if ($stagesStatistics[$stage->id]['statisticStatus'] == 1) { ?>
                                                    <i class="ion-ios-sunny" style="font-size: 60px; color: green;"></i>
                                                <?php } else if ($stagesStatistics[$stage->id]['statisticStatus'] == 2) { ?>
                                                    <i class="ion-ios-partlysunny" style="font-size: 60px; color: #009dff;"></i>
                                                <?php } else if ($stagesStatistics[$stage->id]['statisticStatus'] == 3) { ?>
                                                    <i class="ion-ios-rainy" style="font-size: 60px; color: #ffa900;"></i>
                                                <?php } else if ($stagesStatistics[$stage->id]['statisticStatus'] == 4) { ?>
                                                    <i class="ion-ios-thunderstorm" style="font-size: 60px; color: red;"></i>
                                                <?php } ?>
                                            </div>
                                            <div>Stability: <?= $stagesStatistics[$stage->id]['errorPercent'] ?>%</div>
                                        <?php } else { ?>
                                            <div class="text-small text-muted">No data</div>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="separator-10"></div>

                </div>

                    <?php if ($i != 0 && $i++ % 3 == 0) { ?>
                        <div class="small-12 large-12 cell"></div>
                    <?php } ?>
                <?php } ?>

            </div>

        </div>
    </div>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>