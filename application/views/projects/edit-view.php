<?php require_once APP_DIR . '/application/views/templates/header.phtml' ?>

    <div class="grid-x grid-padding-x">
        <div class="cell">

            <ul class="breadcrumbs">
                <li><a href="<?= BASE_PATH ?>/dashboard"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>"><i class="fa fa-cubes"></i> App: <?= $application->name ?></a></li>
                <li><a href="<?= BASE_PATH ?>/applications/<?= $application->id ?>/test-projects"><i class="fa fa-cube"></i> Test projects</a></li>
                <li><a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>"><i class="fa fa-cube"></i> <?= $project->name ?></a></li>
                <li class="current"><a href="#">Edit</a></li>
            </ul>

            <a href="<?= BASE_PATH ?>/projects/<?= $project->id ?>" class="back-button"><i class="fa fa-angle-left"></i></a>
            <h1><i class="fa fa-cube"></i> <?= $project->name ?></h1>
            <hr/>

            <div class="grid-x grid-padding-x">
                <div class="small-12 large-12 cell">
                    <form action="<?= BASE_PATH ?>/projects/<?= $project->id ?>/edit" method="post"
                          enctype="application/x-www-form-urlencoded">

                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" disabled value="<?= $project->name ?>">

                        <label for="description">Description</label>
                        <textarea name="description" id="description"><?= $project->description ?></textarea>

                        <label for="app">Application</label>
                        <select name="app" id="app">
                            <option value="null">Without Application</option>
                            <?php foreach ($applications as $application) { ?>
                                <option value="<?= $application->id ?>" <?= $application->id == $project->appId ? 'selected' : '' ?>><?= $application->name ?></option>
                            <?php } ?>
                        </select>

                        <input type="submit" class="small button" id="submit" value="Save">

                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom hidden" id="submit-preloader"></i>
                        <span class="sr-only">Loading...</span>

                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="separator-100"></div>

    <script>
        $(function() {
            $("#submit").click(function(event) {
                $(this).addClass('hidden');
                $("#submit-preloader").removeClass('hidden');
            });
        });
    </script>

<?php require_once APP_DIR . '/application/views/templates/footer.phtml' ?>