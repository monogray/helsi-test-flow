<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;

class HttpMethodsModel extends CoreModel
{

    public function getList()
    {
        return R::exportAll(R::find('http_methods'));
    }

    public function get($methodId)
    {
        $methodId = (int)$methodId;

        return R::findOne('http_methods', ' id = ? ', [$methodId])->export();
    }

    public function getMethodNameById($methodId)
    {
        $methodId = (int)$methodId;

        $method = R::findOne('http_methods', ' id = ? ', [$methodId])->export();

        return $method['name'];
    }

    public function getMethodIdByName($methodName)
    {
        $method = R::findOne('http_methods', ' name = ? ', [$methodName])->export();

        return $method['id'];
    }
}