<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;
use Stringy\Stringy as S;

class ExecutorRunsModel extends CoreModel
{
    public function get($runId)
    {
        $runId = (int)$runId;

        $run = R::getRow('
            SELECT * FROM executors_runs WHERE id = ?
        ', [$runId]);

        $data = R::getRow('
            SELECT console_output, report_path, run_config FROM executors_runs_data WHERE exec_run_id = ?
        ', [$runId]);

        $run['console_output'] = $data['console_output'];
        $run['report_path'] = $data['report_path'];
        $run['run_config'] = $data['run_config'];
        $run['screenshots'] = array();

        // Get screenshots
        $dir = APP_DIR . '/jmeter-src/results/robot/' . $run['report_path'];
        $files = scandir($dir);

        foreach ($files as $file) {
            if ($file != '.' && $file != '..' && S::create($file)->endsWith('.png') ) {
                array_push($run['screenshots'], $file);
            }
        }

        return $run;
    }

    public function getByApplicationId($appId)
    {
        $appId = (int)$appId;

        $executors = R::getAll('
            SELECT id, name, exec_id, status, d_create, d_update FROM executors_runs WHERE app_id = ? ORDER BY id DESC LIMIT 500
        ', [$appId]);

        $executors = $this->crateAssocArray($executors, 'id', ['id']);

        return $executors;
    }

    public function getDataByApplicationId($appId)
    {
        $appId = (int)$appId;

        $executorsData = R::getAll('
            SELECT exec_run_id, report_path FROM executors_runs_data WHERE exec_run_id IN (
                SELECT id FROM executors_runs WHERE app_id = ?
            ) AND status = ? ORDER BY id DESC LIMIT 500
        ', [$appId, 1]);

        $executorsData = $this->crateAssocArray($executorsData, 'exec_run_id', ['exec_run_id']);

        return $executorsData;
    }

    public function getDasboardDataByApplicationId($appId)
    {
        $appId = (int)$appId;

        $runs = $this->getByApplicationId($appId);

        $executorsData = R::getAll('
            SELECT exec_run_id, run_config FROM executors_runs_data WHERE exec_run_id IN (
                SELECT id FROM executors_runs WHERE app_id = ?
            ) AND status = ? ORDER BY id DESC LIMIT 100
        ', [$appId, 1]);

        $executorsData = $this->crateAssocArray($executorsData, 'exec_run_id', ['exec_run_id']);

        $result = array();
        foreach ($executorsData as $runId => $dat) {
            preg_match('/"SERVER:.+"/', $dat['run_config'], $matches);

            if (isset($matches[0])) {
                $res = str_replace("\"", "", $matches[0]);
                $res = str_replace("SERVER:", "", $res);

                if (!isset($result[$res])) {
                    $result[$res] = array(
                        'pass' => 0,
                        'fail' => 0,
                        'count' => 0,
                        'stability' => 0,
                    );
                }

                if ($runs[$runId]['status'] != 0) {
                    if($runs[$runId]['status'] == 2) {
                        $result[$res]['pass']++;
                    } else if ($runs[$runId]['status'] == 1) {
                        $result[$res]['fail']++;
                    }

                    $result[$res]['count']++;
                    if ($result[$res]['fail'] != 0) {
                        $result[$res]['stability'] = round(100 - $result[$res]['fail'] / $result[$res]['count'] * 100);
                    }
                }
            }
        }

        return $result;
    }

    public function getRunStatus($appId, $runId)
    {
        $appId = (int)$appId;
        $runId = (int)$runId;

        $run = $this->get($runId);

        $result = array(
            'status' => NULL,
        );

        $reportPath = JMETER_REPORTS_PATH . DIRECTORY_SEPARATOR . 'robot' . DIRECTORY_SEPARATOR . $run['report_path'] . DIRECTORY_SEPARATOR . 'report.html' ;
        if (file_exists($reportPath)) {

            $reportXml = file_get_contents(JMETER_REPORTS_PATH . DIRECTORY_SEPARATOR . 'robot' . DIRECTORY_SEPARATOR . $run['report_path'] . DIRECTORY_SEPARATOR . 'output.xml');
            $result['status'] = 2;
            $pos = strpos($reportXml, '"FAIL"');
            if ($pos) {
                $result['status'] = 1;
            }

            if ($run['status'] != $result['status']) {
                $executorRun = R::xdispense('executors_runs');
                $executorRun->id = $runId;
                $executorRun->status = $result['status'];
                $executorRun->dUpdate = date('Y-m-d H:i:s');
                R::store($executorRun);
            }

            return $result;
        } else {
            return $result;
        }
    }

}