<?php

namespace AutoQa\Models;

class CoreModel
{
    public function crateAssocArray ($srcArray, $keyName = 'id', $deleteKeys = NULL) {
        $result = array();

        foreach ($srcArray as $srcElement) {
            $id = $srcElement[$keyName];
            $result[$id] = $srcElement;

            if (!is_null($deleteKeys)) {
                foreach ($deleteKeys as $key) {
                    unset($result[$id][$key]);
                }
            }
            unset($srcArray[$id]);
        }

        return $result;
    }
}