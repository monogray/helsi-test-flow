<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;

class ScriptsModel extends CoreModel
{
    public function getStageConfigByApplicationStageId($stageId, $stageBean = NULL)
    {
        if (is_null($stageBean)) {
            // TODO Get $stageBean
        }

        return json_decode($stageBean->fields, TRUE);
    }

    public function runByApplicationConfig($appId, $stageId, $runConfig)
    {
        $runParams = '';
        foreach ($runConfig as $key => $value) {
            if ($value != "") {
                $runParams .= "-J{$key}=\"{$value}\" ";
            }
        }

        $testProjects = R::getAll('
            SELECT * FROM projects WHERE app_id = ? AND status = ?
        ', [$appId, 1]);

        $runIds = array();
        foreach ($testProjects as $testProject) {
            $runId = $this->performRun($testProject['name'], $runParams, $appId, $testProject['id'], $stageId, $runConfig);
            array_push($runIds, $runId);
        }

        $runs = R::getAll('
            SELECT * FROM runs_summary_reports WHERE r_id IN(' . implode(',', $runIds) . ') AND label = ?
        ', ['Total']);

        $arrRunData = array(
            'samples' => 0,
            'average_time' => 0,
            'min_time' => 999999999,
            'max_time' => 0,
            'pass_count' => 0,
            'error_count' => 0,
            'error_percent' => 0,
            'status' => 1,
        );
        foreach ($runs as $run) {
            if ($run['status'] == 2) {
                $arrRunData['status'] = 2;
            }
            $arrRunData['samples'] += $run['samples'];
            $arrRunData['average_time'] += $run['average_time'];

            if ($arrRunData['min_time'] > $run['min_time']) {
                $arrRunData['min_time'] = $run['min_time'];
            }

            if ($arrRunData['max_time'] < $run['max_time']) {
                $arrRunData['max_time'] = $run['max_time'];
            }

            $arrRunData['error_count'] += $run['error_count'];
            $arrRunData['error_percent'] += $run['error_percent'];

            $arrRunData['pass_count'] += $run['samples'] - $run['error_count'];
        }

        $arrRunData['average_time'] = $arrRunData['average_time'] / count($runs);
        $arrRunData['error_percent'] = $arrRunData['error_percent'] / count($runs);

        $runsData = R::xdispense('app_runs');
        $runsData->appId = $appId;
        $runsData->stageId = $stageId;
        $runsData->samples = $arrRunData['samples'];
        $runsData->averageTime = $arrRunData['average_time'];
        $runsData->minTime = $arrRunData['min_time'];
        $runsData->maxTime = $arrRunData['max_time'];
        $runsData->passCount = $arrRunData['pass_count'];
        $runsData->errorCount = $arrRunData['error_count'];
        $runsData->errorPercent = $arrRunData['error_percent'];
        $runsData->status = $arrRunData['status'];
        $runsData->dCreate = date('Y-m-d H:i:s');
        $runsData->dUpdate = date('Y-m-d H:i:s');
        $runsDataId = R::store($runsData);

        foreach ($runIds as $runId) {
            $appRunToRuns = R::xdispense('app_runs_to_runs');
            $appRunToRuns->appRunId = $runsDataId;
            $appRunToRuns->runId = $runId;
            $appRunToRuns->dCreate = date('Y-m-d H:i:s');
            $appRunToRuns->dUpdate = date('Y-m-d H:i:s');
            R::store($appRunToRuns);
        }

        return $runsDataId;
    }

    public function runProject ($projectId, $stageId = NULL)
    {
        $projectId = (int)$projectId;
        $stageId = is_null($stageId) ? NULL : (int)$stageId;

        $project = R::load('projects', $projectId);

        $runConfig = NULL;
        if (!is_null($stageId)) {
            $stage = R::load('project_stages', $stageId);

            $runConfig = json_decode($stage->fields, TRUE);
        }

        if (!is_null($runConfig)) {
            $runParams = '';
            foreach ($runConfig as $key => $value) {
                if ($value != "") {
                    $runParams .= "-J{$key}=\"{$value}\" ";
                }
            }

            $runId = $this->performRun($project->name, $runParams, NULL, $projectId, $stageId, $runConfig);

            var_dump($runId);
        }

        var_dump($stageId); die;

    }

    private function performRun ($jmeterProjectName, $runParams, $appId = NULL, $projectId = NULL, $stageId = NULL, $runConfig) {

        // Create necessary directories
        $reportsDir = APP_DIR . DIRECTORY_SEPARATOR . 'jmeter-src' . DIRECTORY_SEPARATOR . 'results';
        $currentReportDir = $reportsDir . DIRECTORY_SEPARATOR . $jmeterProjectName;
        $reportName = 'report_' . date('Y_m_d__h_i_s') . '.jtl';
        $reportPath = $currentReportDir . '/' . $reportName;

        // Create core reports directory if not exist
        if (!file_exists($reportsDir)) {
            if (!mkdir($reportsDir, 0773, TRUE)) {
                echo 'Error on directory creation: ' . $reportsDir;
                die;
            }
        }

        // Create core project (jMeter project) directory if not exist
        if (!file_exists($currentReportDir)) {
            if (!mkdir($currentReportDir, 0773, TRUE)) {
                echo 'Error on directory creation: ' . $currentReportDir;
                die;
            }
        }

        // Build
        $cmd = JMETER_PATH . " -n -t " . APP_DIR . DIRECTORY_SEPARATOR . "jmeter-src" . DIRECTORY_SEPARATOR . "scripts" . DIRECTORY_SEPARATOR . "{$jmeterProjectName} -l {$reportPath} " .
            "{$runParams} " .
            "-Jjmeter.save.saveservice.output_format=xml " .
            "-Jjmeter.save.saveservice.data_type=true " .
            "-Jjmeter.save.saveservice.label=true " .
            "-Jjmeter.save.saveservice.response_code=true " .
            "-Jjmeter.save.saveservice.response_data=true " .
            "-Jjmeter.save.saveservice.response_message=true " .
            "-Jjmeter.save.saveservice.successful=true " .
            "-Jjmeter.save.saveservice.thread_name=true " .
            "-Jjmeter.save.saveservice.assertion_results_failure_message=true " .
            "-Jjmeter.save.saveservice.responseHeaders=true " .
            "-Jjmeter.save.saveservice.time=true " .
            "-Jjmeter.save.saveservice.requestHeaders=true " .
            "-Jjmeter.save.saveservice.samplerData=true " .
            "-Jjmeter.save.saveservice.url=true ";

        $execOutput = shell_exec($cmd);

        $run = R::dispense('runs');
        $run->appId = $appId;
        $run->pId = $projectId;
        $run->name = $reportName;
        $run->stageId = $stageId;
        $run->dCreate = date('Y-m-d H:i:s');
        $run->dUpdate = date('Y-m-d H:i:s');
        $runId = R::store($run);

        $runsData = R::xdispense('runs_data');
        $runsData->rId = $runId;
        $runsData->exec = json_encode($runConfig, JSON_NUMERIC_CHECK);
        $runsData->consoleOutput = $execOutput;
        $runsData->dCreate = date('Y-m-d H:i:s');
        $runsData->dUpdate = date('Y-m-d H:i:s');
        $runsDataId = R::store($runsData);

        // Save Summary report
        require_once APP_DIR . '/libs/jmeter/reports/JmeterReport.php';

        $jmeterReport = new \Jmeter\Reports\JmeterReport();
        $jmeterReport->parseReport($reportPath);
        $this->data['summaryReport'] = $jmeterReport->getSummaryReport();

        $runStatus = 1;
        foreach ($this->data['summaryReport'] as $label => $reportRow) {
            if ($label != 'GraphResult') {
                $runsSummaryReports = R::xdispense('runs_summary_reports');
                $runsSummaryReports->rId = $runId;
                $runsSummaryReports->label = $label;
                $runsSummaryReports->status = $reportRow['errorPercent'] == 0 ? 1 : 2;
                $runsSummaryReports->samples = $reportRow['samples'];
                $runsSummaryReports->averageTime = $reportRow['averageResponse'];
                $runsSummaryReports->maxTime = $reportRow['maxResponse'];
                $runsSummaryReports->minTime = $reportRow['minResponse'];
                $runsSummaryReports->errorCount = $reportRow['errorCount'];
                $runsSummaryReports->errorPercent = $reportRow['errorPercent'];
                $runsSummaryReports->dCreate = date('Y-m-d H:i:s');
                $runsSummaryReports->dUpdate = date('Y-m-d H:i:s');

                if ($runsSummaryReports->status == 2) {
                    $runStatus = 2;
                }

                $runsSummaryReportsId = R::store($runsSummaryReports);

                if (isset($reportRow['requests'])) {
                    foreach ($reportRow['requests'] as $request) {
                        $runsSamples = R::xdispense('runs_samples');
                        $runsSamples->summReportId = $runsSummaryReportsId;
                        $runsSamples->url = $request->getUrl();
                        $runsSamples->method = $request->getMethod();
                        $runsSamples->responseTime = $request->getResponseTime();
                        $runsSamples->status = $request->getStatus() ? 1 : 2;
                        $runsSamples->requestBody = $request->getQueryString();
                        $runsSamples->requestHeaders = $request->getRequestHeaders();
                        $runsSamples->responseCode = $request->getStatusCode();
                        $runsSamples->responseBody = $request->getResponseData();
                        $runsSamples->responseHeaders = $request->getResponseHeaders();

                        $errorMessage = '';
                        foreach ($request->getFailureMessages() as $failureMessage) {
                            $errorMessage .= $failureMessage . "\n\n";
                        }
                        $runsSamples->errorMessage = $errorMessage;
                        $runsSamples->dCreate = date('Y-m-d H:i:s');
                        $runsSamples->dUpdate = date('Y-m-d H:i:s');

                        $runsSamplesId = R::store($runsSamples);
                    }
                }
            }
        }

        $run->status = $runStatus;
        return R::store($run);
    }
}