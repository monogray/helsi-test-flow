<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;

class StagesModel extends CoreModel
{
    public function get($stageId)
    {
        return R::load('project_stages', $stageId);
    }

    public function add($projectId)
    {
        $run = R::xdispense('project_stages');
        $run->pId = (int)$projectId;
        $run->name = "New stage";
        $run->fields = '';
        $run->dCreate = date('Y-m-d H:i:s');
        $run->dUpdate = date('Y-m-d H:i:s');

        return R::store($run);
    }

    public function createByApplicationId($appId)
    {
        $run = R::xdispense('project_stages');
        $run->appId = (int)$appId;
        $run->name = 'New stage';
        $run->fields = '';
        $run->dCreate = date('Y-m-d H:i:s');
        $run->dUpdate = date('Y-m-d H:i:s');

        return R::store($run);
    }

    public function getListByProjectId($projectId)
    {
        $projectId = (int)$projectId;

        return R::find('project_stages', ' p_id = ? AND status = ? ORDER BY name ', [$projectId, 1]);
    }

    public function getListByApplicationId($appId)
    {
        $appId = (int)$appId;

        return R::find('project_stages', ' app_id = ? AND status = ? ORDER BY name ', [$appId, 1]);
    }

    public function getDefaultByProjectId($projectId)
    {
        return R::findOne('project_stages', ' p_id = ? AND is_default = 1 AND status = ? LIMIT 1 ', [$projectId, 1]);
    }

    public function update($stageId)
    {
        $stageId = (int)$stageId;

        $variables = $_POST;
        unset($variables['name']);

        $stage = R::load('project_stages', $stageId);
        $stage->name = $_POST['name'];
        $stage->fields = json_encode($variables, JSON_NUMERIC_CHECK);
        $stage->dUpdate = date('Y-m-d H:i:s');
        R::store($stage);

        return $stage;
    }

    public function updateDefault($projectId, $stageId)
    {
        $projectId = (int)$projectId;
        $stageId = (int)$stageId;

        $defaultStages = R::find('project_stages', ' is_default = ? AND status = ? AND p_id = ?', [1, 1, $projectId]);

        foreach ($defaultStages as $defaultStage) {
            $defaultStage->isDefault = 0;
            $defaultStage->dUpdate = date('Y-m-d H:i:s');
            R::store($defaultStage);
        }

        $stage = R::load('project_stages', $stageId);
        $stage->isDefault = 1;
        $stage->dUpdate = date('Y-m-d H:i:s');
        R::store($stage);

        return $stage;
    }

    public function getStatistic($projectId)
    {
        $projectId = (int)$projectId;

        $statistics = R::getAll('
            SELECT stage_id, r_id, status, error_count, error_percent FROM (
                SELECT * FROM (
                    SELECT * FROM runs_summary_reports WHERE r_id IN (
                        SELECT id FROM runs WHERE stage_id IN (
                            SELECT id FROM project_stages WHERE p_id = ? AND d_delete = \'0000-00-00 00:00:00\'
                        ) AND d_delete = \'0000-00-00 00:00:00\'
                    ) AND label = \'Total\' AND d_delete = \'0000-00-00 00:00:00\' ORDER BY id DESC LIMIT 10
                ) as t1
                    LEFT JOIN
                    (
                        SELECT id AS run_id, stage_id FROM runs WHERE stage_id IN (
                            SELECT id FROM project_stages WHERE p_id = 2 AND d_delete = \'0000-00-00 00:00:00\'
                        ) AND d_delete = \'0000-00-00 00:00:00\'
                    ) AS t2
                        ON t1.r_id = t2.run_id
                ) AS t3
        ', [$projectId]);

        $results = array();
        foreach ($statistics as $statisticEntry) {
            $currentStageId = (int)$statisticEntry['stage_id'];
            if (!isset($results[$currentStageId])) {
                $results[$currentStageId] = array(
                    'runCount'  => 0,
                    'errorCount'  => 0,
                    'errorPercent' => 0,
                    'statisticStatus' => 0,
                );
            }

            if ($statisticEntry['status'] == 2) {
                $results[$currentStageId]['errorCount'] += 1;
            }
            $results[$currentStageId]['runCount'] += 1;
        }

        foreach ($results as $stageId => &$result) {
            $result['errorPercent'] = 100 - round($result['errorCount']/$result['runCount'] * 100, 2);

            if ($result['errorPercent'] < 33) {
                $result['statisticStatus'] = 4;
            } else if ($result['errorPercent'] >= 33 && $result['errorPercent'] < 66) {
                $result['statisticStatus'] = 3;
            } else if ($result['errorPercent'] >= 66 && $result['errorPercent'] < 100) {
                $result['statisticStatus'] = 2;
            } else if ($result['errorPercent'] == 100) {
                $result['statisticStatus'] = 1;
            }
        }

        return $results;
    }
}