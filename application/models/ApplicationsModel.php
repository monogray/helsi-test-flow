<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;

class ApplicationsModel extends CoreModel
{
    public function get($appId)
    {
        $appId = (int)$appId;
        $app = R::load('applications', $appId);

        return $app;
    }

    public function getList()
    {
        return R::find('applications', ' status = ? ', [1]);
    }

    public function getByProjectId($projectId)
    {
        $projectId = (int)$projectId;

        $project = R::getRow('
                SELECT app_id FROM projects WHERE id = ? AND status = ?
            ', [$projectId, 1]);

        if ($project && $project['app_id']) {
            return $this->get($project['app_id']);
        } else {
            return NULL;
        }
    }

    public function getTestProjects($appId, $assoc = FALSE)
    {
        $appId = (int)$appId;

        $projects = R::getAll('
                SELECT * FROM projects WHERE app_id = ? AND status = ?
            ', [$appId, 1]);

        if (!$assoc) {
            return $projects;
        } else {
            return $this->crateAssocArray($projects, 'id', array('id', 'd_delete', 'app_id', 'status'));
        }
    }

    public function getProjectsCount($appId)
    {
        $appId = (int)$appId;

        return R::getAll('
            SELECT COUNT(id) AS projectsCount FROM projects WHERE app_id = ? AND status = ?
        ', [$appId, 1])[0]['projectsCount'];
    }

    public function getProjectsVariables($appId)
    {
        $appId = (int)$appId;

        $projects = $this->getTestProjects($appId);

        $variables = array();
        foreach ($projects as $project) {
            $projectPath = APP_DIR . "/jmeter-src/scripts/{$project['name']}";

            if (file_exists($projectPath)) {
                $projectContent = @file_get_contents($projectPath, TRUE);

                $pattern = '/\${__P\((.+)\)}/';
                preg_match_all($pattern, $projectContent, $matches);


                foreach ($matches[1] as $match) {
                    $results = explode(',', $match);

                    if (!isset($variables[$results[0]])) {
                        $variables[$results[0]] = array();
                    }

                    if (!in_array($results[1], $variables[$results[0]])) {
                        array_push($variables[$results[0]], $results[1]);
                    }
                }
            }

        }

        return $variables;
    }

    public function update($appId, $data)
    {
        // TODO Add $data validation

        $appId = (int)$appId;

        $app = R::load('applications', $appId);
        $app->name = $data['name'];
        $app->description = $data['description'];
        $app->dUpdate = date('Y-m-d H:i:s');
        R::store($app);

        return $app;
    }
}