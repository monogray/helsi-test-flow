<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;
use Stringy\Stringy as S;

class ExecutorsModel extends CoreModel
{
    public function get($executorId)
    {
        $executorId = (int)$executorId;

        return R::load('executors', $executorId);
    }

    public function createByApplicationId($appId)
    {
        $run = R::xdispense('executors');
        $run->appId = (int)$appId;
        $run->name = "New executor";
        $run->dCreate = date('Y-m-d H:i:s');
        $run->dUpdate = date('Y-m-d H:i:s');

        return R::store($run);
    }

    public function getByApplicationId($appId)
    {
        $appId = (int)$appId;

        $executors = R::getAll('
            SELECT id, name, ico, is_default, d_create, d_update FROM executors WHERE app_id = ? AND status = ? ORDER BY name
        ', [$appId, 1]);

        $executors = $this->crateAssocArray($executors, 'id', ['id']);

        return $executors;
    }

    public function update($executorId)
    {
        // TODO Add $_POST validation

        $executorId = (int)$executorId;

        $executor = R::load('executors', $executorId);
        $executor->name = $_POST['name'];
        $executor->ico = $_POST['ico'];
        $executor->description = $_POST['description'];
        $executor->config = $_POST['config'];
        $executor->dUpdate = date('Y-m-d H:i:s');
        R::store($executor);

        return $executor;
    }

    public function getTestsList()
    {
        function get_string_between($string, $start, $end){
            $string = ' ' . $string;
            $ini = strpos($string, $start);
            if ($ini == 0) return '';
            $ini += strlen($start);
            $len = strpos($string, $end, $ini) - $ini;
            return substr($string, $ini, $len);
        }

        $testsList = array();

        $tests = scandir(ROBOT_TESTS_PATH);
        foreach ($tests as $test) {
            if ($test != '.' && $test != '..' && !S::create($test)->startsWith('resource' )&& S::create($test)->endsWith('.robot')) {

                $testContent = file_get_contents(ROBOT_TESTS_PATH . DIRECTORY_SEPARATOR . $test);
                $testDescription = get_string_between($testContent, '*** Settings ***', "\nResource");

                array_push($testsList, array(
                    'name' => $test,
                    'description' => $testDescription
                ));
            }
        }

        return $testsList;
    }

    public function performRun($appId, $executorId, $params)
    {
        $executorId = (int)$executorId;
        $executor = $this->get($executorId);

        $cmd = $executor->config;

        $executorRun = R::xdispense('executors_runs');
        $executorRun->appId = $executor->appId;
        $executorRun->execId = $executorId;
        $executorRun->status = 0;
        $executorRun->dCreate = date('Y-m-d H:i:s');
        $executorRun->dUpdate = date('Y-m-d H:i:s');
        $executorRunId = R::store($executorRun);

        $cmd = str_replace("\${appId}", $appId, $cmd);
        $cmd = str_replace("\${executorId}", $executorId, $cmd);
        $date = "id_" . $executorRunId . "__" . date('Y_m_d__H_i_s');
        $cmd = str_replace("\${currentDate}", $date, $cmd);

        if (count($params) == 1 && $params[0] == 'all') {
            $params[0] = ROBOT_TESTS_PATH;
        } else {
            foreach ($params as $id => &$param) {
                if ($param != 'all') {
                    $param = ROBOT_TESTS_PATH . DIRECTORY_SEPARATOR . $param;
                } else {
                    unset($params[$id]);
                }
            }
        }
        $params = implode(" ", $params);
        $cmd = str_replace("\${tests}", $params, $cmd);

        if (STAGE == 'Dev') {
            $cmd .= ' > /dev/null &';
        } else {
            $cmd = "start /b " . $cmd;
        }

        $executorRunData = R::xdispense('executors_runs_data');
        $executorRunData->execRunId = $executorRunId;
        $executorRunData->runConfig = $cmd;
        // $executorRunData->consoleOutput = $execOutput;
        $executorRunData->reportPath = "$appId\\$executorId\\$date";
        $executorRunData->dCreate = date('Y-m-d H:i:s');
        $executorRunData->dUpdate = date('Y-m-d H:i:s');
        $executorRunDataId = R::store($executorRunData);

        $execOutput = '';
        if (STAGE == 'Dev') {
            $exec = new \AutoQa\Libs\Executor();
            $exec->Executor();
            $execOutput = $exec->run($cmd);
        } else {
            pclose(popen($cmd, "r"));
        }

        /* $status = 1;
        $pos = strpos($execOutput, '| FAIL |');
        if ($pos) {
            $status = 0;
        }

        $executorRun = R::xdispense('executors_runs');
        $executorRun->id = $executorRunId;
        $executorRun->status = $status;
        $executorRun->dUpdate = date('Y-m-d H:i:s');
        $executorRunId = R::store($executorRun); */

        return $executorRunId;
    }

    public function delete($execId)
    {
        $execId = (int) $execId;

        $executor = R::xdispense('executors');
        $executor->id = $execId;
        $executor->status = 0;
        $executor->dDelete = date('Y-m-d H:i:s');

        return R::store($executor);
    }
}