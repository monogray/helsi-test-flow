<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;

class UsersModel extends CoreModel
{
    public function getByEmail($email)
    {
        $email = strtolower($email);

        $user = R::findOne('users', ' email = ? AND status = ? LIMIT 1 ', [$email, 1]);
        if ($user) {
            return $user->export();
        } else {
            return NULL;
        }
    }

    public function getCurrent()
    {
        if (isset($_SESSION['Auth_Auto_App_User']['id'])) {
            $userID = $_SESSION['Auth_Auto_App_User']['id'];

            $user = R::findOne('users', ' id = ? AND status = ? LIMIT 1 ', [$userID, 1]);
            if ($user) {
                return $user->export();
            } else {
                return NULL;
            }
        }

        return NULL;
    }

    public function createNonVerifiedUser($userData)
    {
        $userData['email'] = strtolower($userData['email']);

        $company = R::dispense('companies');
        $company->name = $userData['company'];
        $company->description = '';
        $company->status = 1;
        $company->dCreate = date('Y-m-d H:i:s');
        $company->dUpdate = date('Y-m-d H:i:s');
        $companyId = R::store($company);

        $user = R::dispense('users');
        $user->name = $userData['name'];
        $user->login = '';
        $user->email = $userData['email'];
        $user->description = '';
        $user->password = md5($userData['password']);
        $user->companyId = $companyId;
        $user->token = md5(rand(0, 500) . rand(0, 500) . date('Y-m-d H:i:s'));
        $user->isVerified = 0;
        $user->tokenExpired = date('Y-m-d H:i:s');
        $user->status = 1;
        $user->dCreate = date('Y-m-d H:i:s');
        $user->dUpdate = date('Y-m-d H:i:s');
        $usersId = R::store($user);

        // TODO Send email

        return $usersId;
    }

    public function verifyEmail($data)
    {
        $user = $this->getByEmail($data['email']);

        if ($user) {
            if ($user['is_verified'] == 0 && $user['token'] == $data['token']) {
                $userEntity = R::dispense('users');
                $userEntity->id = (int)$user['id'];
                $userEntity->token = md5(rand(0, 500) . rand(0, 500) . date('Y-m-d H:i:s'));
                $userEntity->isVerified = 1;
                $userEntity->dUpdate = date('Y-m-d H:i:s');
                $usersId = R::store($userEntity);

                return $usersId;
            }
        }

        return FALSE;
    }
}