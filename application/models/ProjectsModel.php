<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;
use Stringy\Stringy as S;

class ProjectsModel extends CoreModel
{
    public function get($projectId)
    {
        return R::load('projects', $projectId);
    }

    public function getList()
    {
        // Get projects files
        $dir = APP_DIR . '/jmeter-src/scripts/';
        $files = scandir($dir);

        $projects = array();

        // Delete project in DB if file not available
        R::exec('UPDATE projects SET status = 0');

        foreach ($files as $file) {
            if ($file != '.' && $file != '..' && S::create($file)->endsWith('.jmx') ) {
                $project = R::findOne('projects', ' name = ? ', [$file]);

                if (is_null($project)) {
                    // Create new project in DB
                    $newProject = R::dispense('projects');
                    $newProject->name = $file;
                    $newProject->status = 1;
                    $newProject->dCreate = date('Y-m-d H:i:s');
                    $newProject->dUpdate = date('Y-m-d H:i:s');

                    $id = R::store($newProject);

                    if (is_null($newProject['app_id'])) {
                        $newProject['app_id'] = 0;
                    }

                    if (!isset($projects[$newProject['app_id']])) {
                        $projects[$newProject['app_id']] = array();
                    }
                    $projects[$newProject['app_id']][$id] = $newProject;
                } else {
                    $currentProject = R::dispense('projects');
                    $currentProject->id = $project['id'];
                    $currentProject->status = 1;
                    R::store($currentProject);

                    if (is_null($project['app_id'])) {
                        $project['app_id'] = 0;
                    }
                    if (!isset($projects[$project['app_id']])) {
                        $projects[$project['app_id']] = array();
                    }
                    $projects[$project['app_id']][$project->id] = $project;
                }
            }
        }

        return $projects;
    }

    public function getListByAppId($appId)
    {
        $appId = (int)$appId;

        return R::find('projects', ' app_id = ? AND status = ? ORDER BY id DESC', [$appId, 1]);
    }

    public function getFavoritesList()
    {
        return R::find('projects', ' is_favorite = ? AND d_delete = ? ', [1, '0000-00-00 00:00:00']);
    }

    public function getByName($name)
    {
        return R::findOne('projects', ' name = ? AND d_delete = ? ', [$name, '0000-00-00 00:00:00']);
    }

    public function getProjectVariables($projectId)
    {
        $project = $this->get($projectId);
        $scriptName = $project->name;

        $projectPath = APP_DIR . "/jmeter-src/scripts/{$scriptName}";
        $projectContent = @file_get_contents($projectPath, TRUE);

        $pattern = '/\${__P\((.+)\)}/';
        preg_match_all($pattern, $projectContent, $matches);

        $variables = array();
        foreach ($matches[1] as $match) {
            $results = explode(',', $match);
            $variables[$results[0]] = $results[1];
        }

        return $variables;
    }

    public function update($projectId, $data)
    {
        // TODO Add $data validation

        $projectId = (int)$projectId;

        $project = R::load('projects', $projectId);
        $project->description = $data['description'];
        $project->appId =  $data['app'] == 'null' ? NULL : $data['app'];
        $project->dUpdate = date('Y-m-d H:i:s');
        R::store($project);

        return $project;
    }
}