<?php

namespace AutoQa\Models;

use RedBeanPHP\Facade as R;

class RunsModel extends CoreModel
{
    public function getListByProjectsId($projectId, $stageId = NULL)
    {
        $projectId = (int)$projectId;

        if (is_null($stageId) || (int)$stageId == -1) {
            return R::find('runs', ' p_id = ? AND d_delete = ? ORDER BY id DESC LIMIT 100 ', [$projectId, '0000-00-00 00:00:00']);
        } else {
            $stageId = (int)$stageId;
            if ($stageId != 0) {
                return R::find('runs', ' p_id = ? AND stage_id = ? AND d_delete = ? ORDER BY id DESC LIMIT 100 ', [$projectId, $stageId, '0000-00-00 00:00:00']);
            } else {
                return R::find('runs', ' p_id = ? AND stage_id IS NULL AND d_delete = ? ORDER BY id DESC LIMIT 100 ', [$projectId, '0000-00-00 00:00:00']);
            }
        }
    }

    public function getAppRunsListByAppId($appId, $stageId = NULL)
    {
        $appId = (int)$appId;

        if (is_null($stageId) || (int)$stageId == -1) {
            return R::find('app_runs', ' app_id = ? AND d_delete = ? ORDER BY id DESC LIMIT 100 ', [$appId, '0000-00-00 00:00:00']);
        } else {
            $stageId = (int)$stageId;
            if ($stageId != 0) {
                return R::find('app_runs', ' app_id = ? AND stage_id = ? AND d_delete = ? ORDER BY id DESC LIMIT 100 ', [$appId, $stageId, '0000-00-00 00:00:00']);
            } else {
                return R::find('app_runs', ' app_id = ? AND stage_id IS NULL AND d_delete = ? ORDER BY id DESC LIMIT 100 ', [$appId, '0000-00-00 00:00:00']);
            }
        }
    }

    public function getListByAppRunId($appRunId)
    {
        $appRunId = (int)$appRunId;

        $runs = R::getAll('
            SELECT * FROM runs WHERE id IN (
                SELECT run_id FROM app_runs_to_runs WHERE app_run_id = ?
            )
        ', [$appRunId]);

        return $runs;
    }

    public function getByAppId($appId, $runId)
    {
        $appId = (int)$appId;
        $runId = (int)$runId;

        return R::findOne('app_runs', ' id = ? AND app_id = ? ', [$runId, $appId]);
    }


    public function getStatisticListByProjectsId($projectId)
    {
        $projectId = (int)$projectId;

        $statistics = R::getAll('
            SELECT r_id, samples, error_count FROM runs_summary_reports WHERE r_id IN (
                SELECT id FROM (
                    SELECT id FROM runs WHERE p_id = ? AND d_delete = ? ORDER BY id DESC LIMIT 100
                ) AS t
            ) AND label = ? GROUP BY r_id
        ', [$projectId, '0000-00-00 00:00:00', 'Total']);

        $results = array();
        foreach ($statistics as $statisticEntry) {
            if (!isset($results[$statisticEntry['r_id']])) {
                $results[$statisticEntry['r_id']] = array(
                    'all'  => 0,
                    'pass' => 0,
                    'fail' => 0
                );
            }

            $results[$statisticEntry['r_id']]['all'] = $statisticEntry['samples'];
            $results[$statisticEntry['r_id']]['pass'] += $statisticEntry['samples'] - $statisticEntry['error_count'];
            $results[$statisticEntry['r_id']]['fail'] += $statisticEntry['error_count'];
        }

        return $results;
    }

    public function getStatisticListByAppRunId($appRunId)
    {
        $appRunId = (int)$appRunId;

        $statistics = R::getAll('
            SELECT r_id, samples, error_count, average_time, min_time, max_time, error_percent, d_create FROM runs_summary_reports WHERE r_id IN (
                SELECT run_id FROM (
                    SELECT run_id FROM app_runs_to_runs WHERE app_run_id = ? AND d_delete = ? ORDER BY id DESC LIMIT 100
                ) AS t
            ) AND label = ? GROUP BY r_id
        ', [$appRunId, '0000-00-00 00:00:00', 'Total']);

        $results = array();
        foreach ($statistics as $statisticEntry) {
            if (!isset($results[$statisticEntry['r_id']])) {
                $results[$statisticEntry['r_id']] = array(
                    'all'  => 0,
                    'pass' => 0,
                    'fail' => 0,
                );
            }

            $results[$statisticEntry['r_id']]['all'] = $statisticEntry['samples'];
            $results[$statisticEntry['r_id']]['pass'] += $statisticEntry['samples'] - $statisticEntry['error_count'];
            $results[$statisticEntry['r_id']]['fail'] += $statisticEntry['error_count'];

            $results[$statisticEntry['r_id']]['average_time'] = $statisticEntry['average_time'];
            $results[$statisticEntry['r_id']]['min_time'] = $statisticEntry['min_time'];
            $results[$statisticEntry['r_id']]['max_time'] = $statisticEntry['max_time'];
            $results[$statisticEntry['r_id']]['error_percent'] = $statisticEntry['error_percent'];
            $results[$statisticEntry['r_id']]['d_create'] = $statisticEntry['d_create'];
        }

        return $results;
    }

    public function get($reportId)
    {
        $reportId = (int)$reportId;

        return R::load('runs', $reportId);
    }

    public function getReportData($reportId)
    {
        $reportId = (int)$reportId;

        return R::findOne('runs_data', ' r_id = ? AND d_delete = ? ', [$reportId, '0000-00-00 00:00:00']);
    }

    public function getLastByProjectsId($projectId, $params)
    {
        $projectId = (int)$projectId;

        if (isset($params['host'])) {
            return R::getRow('
                    SELECT * FROM runs WHERE id = (
                        SELECT MAX(r_id) FROM runs_data WHERE exec LIKE \'%' . $params['host'] . '%\'
                    ) LIMIT 1;
                '
            );
        }

        return R::findOne('runs', ' p_id = ? AND d_delete = ? ORDER BY id DESC LIMIT 1 ', [$projectId, '0000-00-00 00:00:00'])->export();
    }
}