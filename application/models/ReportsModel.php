<?php

namespace AutoQa\Models;

use Jmeter\Reports\HttpMethods;
use RedBeanPHP\Facade as R;

class ReportsModel extends CoreModel
{
    public function getSummaryReport($reportId)
    {
        $reportId = (int)$reportId;

        $summaryRows = R::find('runs_summary_reports', ' r_id = ? AND d_delete = ? ', [$reportId, '0000-00-00 00:00:00']);

        $result = array();
        foreach ($summaryRows as $row) {
            $result[$row->label] = $row;
        }

        return $result;
    }

    public function getSamplesReport($reportId)
    {
        $reportId = (int)$reportId;

        $summaryRows = R::find(
            'runs_samples',
            " summ_report_id IN (
                SELECT id FROM runs_summary_reports WHERE r_id = {$reportId} AND d_delete = '0000-00-00 00:00:00')
            AND d_delete = ? ",
            ['0000-00-00 00:00:00']
        );

        require_once __DIR__ . '/../../libs/jmeter/reports/HttpMethods.php';
        $httpMethods = new HttpMethods();

        $samplesResult = array();
        $samplesResponsesTimeList = array();
        foreach ($summaryRows as $row) {
            if (!isset($samplesResult[$row->summ_report_id])) {
                $samplesResult[$row->summ_report_id] = array();
            }

            $row->method = $httpMethods->getMethodIds()[$row->method];

            array_push($samplesResult[$row->summ_report_id], $row);
            array_push($samplesResponsesTimeList, $row->responseTime);
        }

        $result = array(
            'samples'                  => $samplesResult,
            'samplesResponsesTimeList' => $samplesResponsesTimeList,
        );

        return $result;
    }

}