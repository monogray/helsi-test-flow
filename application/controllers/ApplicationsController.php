<?php

namespace AutoQa\Controllers;

use AutoQa\Models\ProjectsModel;
use AutoQa\Models\RunsModel;
use AutoQa\Models\ReportsModel;
use AutoQa\Models\StagesModel;
use AutoQa\Models\ApplicationsModel;
use AutoQa\Models\ScriptsModel;

class ApplicationsController extends BaseAppController
{

    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ProjectsModel
     */
    protected $projectsModel = NULL;

    /**
     * @var RunsModel
     */
    protected $runsModel = NULL;

    /**
     * @var ReportsModel
     */
    protected $reportsModel = NULL;

    /**
     * @var StagesModel
     */
    protected $stagesModel = NULL;

    /**
     * @var ScriptsModel
     */
    protected $scriptsModel = NULL;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->applicationsModel = new ApplicationsModel();
        $this->projectsModel = new ProjectsModel();
        $this->runsModel = new RunsModel();
        $this->reportsModel = new ReportsModel();
        $this->stagesModel = new StagesModel();
        $this->scriptsModel = new ScriptsModel();
    }

    public function view($appId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['runs'] = $this->runsModel->getAppRunsListByAppId($appId);
        $this->data['stages'] = $this->stagesModel->getListByApplicationId($appId);
        $this->data['stagesCount'] = count($this->data['stages']);
        $this->data['testProjectsCount'] = $this->applicationsModel->getProjectsCount($appId);

        return $this->render('applications/view.php');
    }

    public function viewMap($appId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['stages'] = $this->stagesModel->getListByApplicationId($appId);
        $this->data['stagesCount'] = count($this->data['stages']);
        $this->data['testProjectsCount'] = $this->applicationsModel->getProjectsCount($appId);
        $this->data['projects'] = $this->projectsModel->getListByAppId($appId);

        return $this->render('applications/map-view.php');
    }

    public function editView($appId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);

        return $this->render('applications/edit-view.php');
    }

    public function update($appId)
    {
        $this->applicationsModel->update($appId, $_POST);

        $this->goToRout("applications/$appId/edit");
    }

    public function runView($appId, $runId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['appRun'] = $this->runsModel->getByAppId($appId, $runId);
        $this->data['runs'] = $this->runsModel->getListByAppRunId($runId);
        $this->data['runsStatistics'] = $this->runsModel->getStatisticListByAppRunId($runId);
        $this->data['stages'] = $this->stagesModel->getListByApplicationId($appId);
        $this->data['projects'] = $this->applicationsModel->getTestProjects($appId, TRUE);

        return $this->render('applications/run-view.php');
    }

    public function reportView($appId, $runId, $reportId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['appRun'] = $this->runsModel->getByAppId($appId, $runId);

        $this->data['report'] = $this->runsModel->get($reportId);
        $this->data['project'] = $this->projectsModel->get($this->data['report']['p_id']);
        $this->data['reportData'] = $this->runsModel->getReportData($reportId);

        $this->data['stage'] = $this->stagesModel->get($this->data['report']->stageId);

        $this->data['summaryReport'] = $this->reportsModel->getSummaryReport($reportId);
        $this->data['samplesData'] = $this->reportsModel->getSamplesReport($reportId);

        $this->data['reportMode'] = 'AppView';

        return $this->render('scripts/view-report.phtml');
    }

    public function stagesView($appId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['stages'] = $this->stagesModel->getListByApplicationId($appId);

        return $this->render('applications/stages.php');
    }

    public function createStage($appId)
    {
        $stageId = $this->stagesModel->createByApplicationId($appId);
        if ($stageId) {
            $this->goToRout("applications/$appId/stages/$stageId/edit");
        } else {
            $this->goToRout("applications/$appId/stages");
        }
    }

    public function editStage($appId, $stageId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['stage'] = $this->stagesModel->get($stageId);
        $this->data['projectsVariables'] = $this->applicationsModel->getProjectsVariables($appId);

        return $this->render('applications/stage-edit.php');
    }

    public function performRun($appId, $stageId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['stage'] = $this->stagesModel->get($stageId);
        $this->data['runConfig'] = $this->scriptsModel->getStageConfigByApplicationStageId($stageId, $this->data['stage']);

        $appRunId = $this->scriptsModel->runByApplicationConfig($appId, $stageId, $this->data['runConfig']);

        $this->goToRout("applications/{$appId}/runs/{$appRunId}");
    }
}