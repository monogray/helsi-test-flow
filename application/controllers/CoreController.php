<?php

namespace AutoQa\Controllers;

class CoreController
{
    private $app;
    private $controllerClass;

    /**
     * CoreController constructor.
     * @param \Slim\App $app
     * @param $controllerClass
     */

    public function __construct(\Slim\App $app, $controllerClass)
    {
        $this->app = $app;
        $this->controllerClass = $controllerClass;
    }

    /**
     * @param $actionName
     * @return \Closure
     */
    public function __invoke($actionName)
    {
        $app = $this->app;
        $controllerClass = $this->controllerClass;

        $callable = function ($request, $response, $args) use ($app, $actionName, $controllerClass) {
            $controller = new $controllerClass($app);

            $controllerCallable = $controller($actionName);
            $controllerCallable($request, $response, $args);
        };

        return $callable;
    }
}