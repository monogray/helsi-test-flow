<?php

namespace AutoQa\Controllers;

use AutoQa\Models\ApplicationsModel;

class QaDashboardController extends BaseAppController
{

    /**
     * @var ApplicationsModel
     */
    //protected $applicationsModel = NULL;

    public function __construct(\Slim\App $app)
    {
        $this->isPublic = TRUE;

        parent::__construct($app);

        //$this->applicationsModel = new ApplicationsModel();
    }

    public function index()
    {
        return $this->render('qa-dashboard/index.php');
    }
}