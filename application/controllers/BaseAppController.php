<?php

namespace AutoQa\Controllers;

use MartynBiz\Slim3Controller\Controller;

class BaseAppController extends Controller
{
    protected $isPublic = FALSE;
    protected $data = array();

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);
        $this->checkAccess();
    }

    /**
     * View render
     *
     * @param string $viewPath
     * @param array $args
     * @return mixed
     */
    protected function render($viewPath, $args = array())
    {
        if (isset($_GET['format']) && $_GET['format'] == 'json') {
            $this->apiRender();
            return TRUE;
        }

        return $this->app->getContainer()->renderer->render($this->response, $viewPath, $this->data);
    }

    /**
     * Render for API
     *
     * @param string $format
     * @return bool
     */
    protected function apiRender($format = 'JSON')
    {
        if ($format == 'JSON') {
            header('Content-Type: application/json; charset=utf-8');

            $result = array(
                'data' => $this->data
            );

            echo json_encode($result, JSON_NUMERIC_CHECK);
            exit;
        } else if ($format == 'XML') {
            // TODO Implement XML output format
        }

        return FALSE;
    }

    protected function checkAccess()
    {
        if (!$this->isPublic) {

            $headers = getallheaders();

           //print_r($_SERVER);
           //print_r($_SESSION); die;

            if (isset($headers['AccessToken'])) {
                $_SESSION['Auth_Auto_App_Status'] = $headers['AccessToken'];
            } else if (isset($headers['Accesstoken'])) {
                $_SESSION['Auth_Auto_App_Status'] = $headers['Accesstoken'];
            }

            if (!isset($_SESSION['Auth_Auto_App_Status']) || $_SESSION['Auth_Auto_App_Status'] == '') {
                header('Location: ' . BASE_PATH . '/auth/login');
                exit;
            } else {
                if (strval($_SESSION['Auth_Auto_App_Status']) == 'e418ac9394a7cd8342973041b2aa3615') {
                    // User authorized;
                } else {
                    header('Location: ' . BASE_PATH . '/auth/login');
                    exit;
                }

                /* $user = R::getRow(
                    'SELECT * FROM users WHERE token = ? LIMIT 1;',
                    [$_SESSION['Auth_Auto_App_Status']]
                );

                if (isset($user['id'])) {
                    $datetime1 = strtotime(date('Y-m-d h:i:s'));
                    $datetime2 = strtotime($user['token_expired']);

                    $differenceInSeconds = $datetime2 - $datetime1;

                    if ($differenceInSeconds <= 1) {
                        $_SESSION['Auth_Auto_App_Status'] = '';

                        header('Location: ' . BASE_PATH . '/auth/login');
                        exit;
                    } else {
                        $this->data['user'] = $user;
                    }
                } else {
                    header('Location: ' . BASE_PATH . '/auth/login');
                    exit;
                } */
            }

        }

        return TRUE;
    }

    protected function goToRout($rout) {
        header('Location: ' . BASE_PATH . '/' . $rout);
        exit;
    }
}