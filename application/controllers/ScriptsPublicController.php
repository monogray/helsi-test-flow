<?php

namespace AutoQa\Controllers;

class ScriptsPublicController extends ScriptsController
{
    public function __construct(\Slim\App $app)
    {
        $this->isPublic = TRUE;
        parent::__construct($app);
    }

    public function lastReport($projectId, $key)
    {
        if ($key == APP_PUBLIC_KEY) {
            $params = array();
            $getVars = $this->request->getQueryParams();
            if (isset($getVars['host'])) {
                $params['host'] = $getVars['host'];
            }

            $lastRun = $this->runsModel->getLastByProjectsId($projectId, $params);
            $this->viewReport($projectId, $lastRun['id'], $getVars);
        } else {
            $this->goToRout('auth/login');
        }
    }
}