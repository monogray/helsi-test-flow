<?php

namespace AutoQa\Controllers;

use AutoQa\Models\ProjectsModel;
use AutoQa\Models\StagesModel;
use AutoQa\Models\ApplicationsModel;

class StagesController extends BaseAppController
{
    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ProjectsModel
     */
    protected $projectsModel = NULL;

    /**
     * @var StagesModel
     */
    protected $stagesModel = NULL;


    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->projectsModel = new ProjectsModel();
        $this->stagesModel = new StagesModel();
        $this->applicationsModel = new ApplicationsModel();
    }

    public function add($projectId)
    {
        $this->stagesModel->add($projectId);

        $this->goToRout('projects/' . $projectId . '/stages');
    }

    public function viewListFromProject($projectId)
    {
        $this->data['project'] = $this->projectsModel->get($projectId);
        $this->data['application'] = $this->applicationsModel->get($this->data['project']->appId);

        $this->data['stages'] = $this->stagesModel->getListByProjectId($projectId);

        // $this>$this->apiRender(); die;

        return $this->render('projects/stages.php');
    }

    public function editView($projectId, $stageId)
    {
        $this->data['project'] = $this->projectsModel->get($projectId);
        $this->data['application'] = $this->applicationsModel->get($this->data['project']->appId);
        $this->data['stage'] = $this->stagesModel->get($stageId);
        $this->data['projectVariables'] = $this->projectsModel->getProjectVariables($projectId);

        return $this->render('stages/view.php');
    }

    public function edit($projectId, $stageId)
    {
        $this->data['stage'] = $this->stagesModel->update($stageId);

        $this->goToRout('projects/' . $projectId . '/stages/' . $stageId);
    }

    /**
     * Edit stages of application
     *
     * @param $appId
     * @param $stageId
     */
    public function applicationEdit($appId, $stageId)
    {
        $this->data['stage'] = $this->stagesModel->update($stageId);

        $this->goToRout('applications/' . $appId . '/stages/' . $stageId . '/edit');
    }

    public function updateDefault($projectId, $stageId)
    {
        $this->data['stage'] = $this->stagesModel->updateDefault($projectId, $stageId);
    }

}