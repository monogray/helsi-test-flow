<?php

namespace AutoQa\Controllers;

use AutoQa\Models\ApplicationsModel;
use AutoQa\Models\ExecutorsModel;
use AutoQa\Models\ExecutorRunsModel;

class ExecutorsController extends BaseAppController
{
    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ExecutorsModel
     */
    protected $executorsModel = NULL;

    /**
     * @var ExecutorRunsModel
     */
    protected $executorRunsModel = NULL;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->applicationsModel = new ApplicationsModel();
        $this->executorsModel = new ExecutorsModel();
        $this->executorRunsModel = new ExecutorRunsModel();
    }

    public function viewList($appId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['executors'] = $this->executorsModel->getByApplicationId($appId);

        return $this->render('robot/executors-list.php');
    }

    public function create($appId)
    {
        $executorId = $this->executorsModel->createByApplicationId($appId);
        if ($executorId) {
            $this->goToRout("applications/$appId/robot/executors/$executorId/edit");
        } else {
            $this->goToRout("applications/$appId/robot/executors");
        }
    }

    public function update($appId, $executorId)
    {
        $this->executorsModel->update($executorId);

        $this->goToRout("applications/$appId/robot/executors/$executorId/run");
    }

    public function viewExecutorRun($appId, $executorId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['executor'] = $this->executorsModel->get($executorId);
        $this->data['tests'] = $this->executorsModel->getTestsList();

        return $this->render('robot/executor-run.php');
    }

    public function performRun($appId, $executorId)
    {
        if (isset($_GET['params'])) {
            // TODO Add $_GET['params'] validation
        } else {
            $_GET['params'] = array("all");
        }

        $this->data['appId'] = $appId;
        $this->data['execId'] = $executorId;
        $this->data['execRunId'] = $this->executorsModel->performRun($appId, $executorId, $_GET['params']);

        $this->apiRender();
    }

    public function viewRun($appId, $runId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['run'] = $this->executorRunsModel->get($runId);

        return $this->render('robot/run-view.php');
    }

    public function delete($appId, $executorId)
    {
        $this->executorsModel->delete($executorId);

        $this->apiRender();
    }
}