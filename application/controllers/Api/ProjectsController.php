<?php

namespace AutoQa\Controllers\Api;

use AutoQa\Controllers\BaseAppController;

use AutoQa\Models\ProjectsModel;
use AutoQa\Models\ScriptsModel;
use AutoQa\Models\RunsModel;
use AutoQa\Models\ReportsModel;
use AutoQa\Models\StagesModel;
use AutoQa\Models\ApplicationsModel;

class ProjectsController extends BaseAppController
{

    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ProjectsModel
     */
    protected $projectsModel = NULL;

    /**
     * @var ScriptsModel
     */
    protected $scriptsModel = NULL;

    /**
     * @var RunsModel
     */
    protected $runsModel = NULL;

    /**
     * @var ReportsModel
     */
    protected $reportsModel = NULL;

    /**
     * @var StagesModel
     */
    protected $stagesModel = NULL;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->applicationsModel = new ApplicationsModel();
        $this->projectsModel = new ProjectsModel();
        $this->scriptsModel = new ScriptsModel();
        $this->runsModel = new RunsModel();
        $this->reportsModel = new ReportsModel();
        $this->stagesModel = new StagesModel();
    }

    public function runProject($projectId)
    {
        $stageId = NULL;
        if (isset($_GET['stageId'])) {
            $stageId = (int)$_GET['stageId'];
        }

        $this->scriptsModel->runProject($projectId, $stageId);

        $this->apiRender();
    }
}