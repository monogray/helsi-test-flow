<?php

namespace AutoQa\Controllers\Api;

use AutoQa\Controllers\BaseAppController;

use AutoQa\Models\ApplicationsModel;
use AutoQa\Models\ExecutorsModel;
use AutoQa\Models\ExecutorRunsModel;

class ExecutorsController extends BaseAppController
{
    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ExecutorsModel
     */
    protected $executorsModel = NULL;

    /**
     * @var ExecutorRunsModel
     */
    protected $executorRunsModel = NULL;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->applicationsModel = new ApplicationsModel();
        $this->executorsModel = new ExecutorsModel();
        $this->executorRunsModel = new ExecutorRunsModel();
    }

    public function getStatus($appId, $runId)
    {
        $this->data['status'] = $this->executorRunsModel->getRunStatus($runId, $runId);


        return $this->apiRender();
    }
}