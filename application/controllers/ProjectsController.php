<?php

namespace AutoQa\Controllers;

use AutoQa\Models\ProjectsModel;
use AutoQa\Models\RunsModel;
use AutoQa\Models\ReportsModel;
use AutoQa\Models\StagesModel;
use AutoQa\Models\ApplicationsModel;

class ProjectsController extends BaseAppController
{

    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ProjectsModel
     */
    protected $projectsModel = NULL;

    /**
     * @var RunsModel
     */
    protected $runsModel = NULL;

    /**
     * @var ReportsModel
     */
    protected $reportsModel = NULL;

    /**
     * @var StagesModel
     */
    protected $stagesModel = NULL;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->applicationsModel = new ApplicationsModel();
        $this->projectsModel = new ProjectsModel();
        $this->runsModel = new RunsModel();
        $this->reportsModel = new ReportsModel();
        $this->stagesModel = new StagesModel();
    }

    public function viewDashboard($projectId)
    {
        $this->data['project'] = $this->projectsModel->get($projectId);
        $this->data['application'] = $this->applicationsModel->get($this->data['project']->appId);

        $this->data['stages'] = $this->stagesModel->getListByProjectId($projectId);
        $this->data['stagesStatistics'] = $this->stagesModel->getStatistic($projectId);

        return $this->render('projects/dashboard.php');
    }

    public function editView($projectId)
    {
        $this->data['project'] = $this->projectsModel->get($projectId);
        $this->data['application'] = $this->applicationsModel->get($this->data['project']->appId);
        $this->data['applications'] = $this->applicationsModel->getList();

        return $this->render('projects/edit-view.php');
    }

    public function update($projectId)
    {
        $this->projectsModel->update($projectId, $_POST);

        $this->goToRout("projects/$projectId/edit");
    }

}