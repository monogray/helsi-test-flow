<?php

namespace AutoQa\Controllers;

use AutoQa\Models\UsersModel;

class LoginController extends BaseAppController
{
    /**
     * @var UsersModel
     */
    protected $usersModel = NULL;

    public function __construct(\Slim\App $app)
    {
        $this->isPublic = TRUE;
        parent::__construct($app);

        $this->usersModel = new UsersModel();
    }

    public function login()
    {
        return $this->render('auth/login.php');
    }

    public function signUp()
    {
        return $this->render('auth/sign-up.php');
    }

    public function performSignUp()
    {
        $data = $this->request->getParsedBody();

        if ($this->validateSignUpData($data)) {
            $existUser = $this->usersModel->getByEmail($data['email']);

            if ($existUser) {
                // User exist
                // TODO render view with error
            } else {
                // User not exist
                $this->usersModel->createNonVerifiedUser($data);
            }
        }

        $this->data['errors'] = array(
            'status' => FALSE,
        ); // TODO Render view with errors

        $this->goToRout('dashboard');
    }

    private function validateSignUpData($data)
    {
        if (
            isset($data['email']) &&
            isset($data['password']) &&
            isset($data['repeat-password']) &&
            isset($data['name']) &&
            isset($data['company']) &&

            $data['email'] != '' &&

            $data['password'] != '' &&
            strlen($data['password']) >= 6 &&

            $data['repeat-password'] != '' &&
            strlen($data['repeat-password']) >= 6 &&

            $data['name'] != '' &&
            $data['company'] != '' &&

            $data['password'] == $data['repeat-password']
        ) {
            return TRUE;
        }

        return FALSE;
    }

    public function verifyEmail()
    {
        $getVars = $this->request->getQueryParams();
        $this->usersModel->verifyEmail($getVars);

        // TODO render view
    }

    public function performLogin()
    {
        $data = $this->request->getParsedBody();

        if (isset($data['login']) && isset($data['password']) && $data['login'] != '' && strlen($data['password']) >= 6) {
            $data['login'] = strtolower($data['login']);

            $user = $this->usersModel->getByEmail($data['login']);

            if ($user && $user['is_verified'] == 1 && $user['status'] == 1) {
                if ($data['login'] == $user['email'] && md5($data['password']) == $user['password']) {

                    $_SESSION['Auth_Auto_App_Status'] = 'e418ac9394a7cd8342973041b2aa3615';
                    $_SESSION['Auth_Auto_App_User'] = array(
                        'id'    => $user['id'],
                        'login' => $user['login'],
                    );

                    $this->goToRout('dashboard');
                } else {
                    $this->data['login'] = $data['login'];

                    return $this->login();
                }
            }
        }

        $this->data['login'] = $data['login'];

        return $this->login();
    }

    public function logout()
    {
        $_SESSION['Auth_Auto_App_Status'] = '';
        $this->goToRout('auth/login');
    }
}