<?php

namespace AutoQa\Controllers;

use AutoQa\Models\ProjectsModel;
use AutoQa\Models\RunsModel;
use AutoQa\Models\ReportsModel;
use AutoQa\Models\StagesModel;
use AutoQa\Models\ApplicationsModel;
use AutoQa\Models\ScriptsModel;

class TestProjectsController extends BaseAppController
{

    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ScriptsModel
     */
    protected $scriptsModel = NULL;

    /**
     * @var ProjectsModel
     */
    protected $projectsModel = NULL;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->applicationsModel = new ApplicationsModel();
        $this->scriptsModel = new ScriptsModel();
        $this->projectsModel = new ProjectsModel();
    }

    public function view($appId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['testProjectsCount'] = $this->applicationsModel->getProjectsCount($appId);
        $this->data['projects'] = $this->projectsModel->getListByAppId($appId);

        return $this->render('test-projects/view.php');
    }
}