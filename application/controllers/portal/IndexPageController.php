<?php

namespace AutoQa\Portal\Controllers;

use AutoQa\Controllers\BaseAppController;

class IndexPageController extends BaseAppController
{
    public function __construct(\Slim\App $app)
    {
        $this->isPublic = TRUE;
        parent::__construct($app);
    }

    public function index()
    {
        return $this->render('portal/index.php');
    }
}