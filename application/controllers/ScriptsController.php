<?php

namespace AutoQa\Controllers;

use RedBeanPHP\Facade as R;
use Jmeter\Reports\JmeterReport;
use AutoQa\Models\ApplicationsModel;
use AutoQa\Models\ProjectsModel;
use AutoQa\Models\RunsModel;
use AutoQa\Models\ReportsModel;
use AutoQa\Models\StagesModel;

class ScriptsController extends BaseAppController
{

    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ProjectsModel
     */
    protected $projectsModel = NULL;

    /**
     * @var RunsModel
     */
    protected $runsModel = NULL;

    /**
     * @var ReportsModel
     */
    protected $reportsModel = NULL;

    /**
     * @var StagesModel
     */
    protected $stagesModel = NULL;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->applicationsModel = new ApplicationsModel();
        $this->projectsModel = new ProjectsModel();
        $this->runsModel = new RunsModel();
        $this->reportsModel = new ReportsModel();
        $this->stagesModel = new StagesModel();
    }

    public function index()
    {
        $this->data['applications'] = $this->applicationsModel->getList();
        $this->data['projects'] = $this->projectsModel->getList();
        $this->data['favoriteProjects'] = $this->projectsModel->getFavoritesList();

        return $this->render('index.phtml');
    }

    public function view($projectId)
    {
        $this->data['project'] = $this->projectsModel->get($projectId);
        $this->data['application'] = $this->applicationsModel->get($this->data['project']->appId);

        $stageId = NULL;
        if (isset($_GET['stage'])) {
            $stageId = (int)$_GET['stage'];
        }
        $this->data['runs'] = $this->runsModel->getListByProjectsId($projectId, $stageId);

        $this->data['runsStatistic'] = $this->runsModel->getStatisticListByProjectsId($projectId);
        $this->data['stages'] = $this->stagesModel->getListByProjectId($projectId);
        $this->data['defaultStage'] = $this->stagesModel->getDefaultByProjectId($projectId);

        return $this->render('scripts/view.phtml');
    }

    public function runScript($projectId)
    {
        $this->data['project'] = $this->projectsModel->get($projectId);
        $this->data['projectVariables'] = $this->projectsModel->getProjectVariables($projectId);
        $this->data['stages'] = $this->stagesModel->getListByProjectId($projectId);

        return $this->render('scripts/run.phtml');
    }

    public function performRunScriptNow($projectId)
    {
        $_POST = array();
        $this->data['defaultStage'] = $this->stagesModel->getDefaultByProjectId($projectId);

        if (!is_null($this->data['defaultStage'])) {
            $_POST = json_decode($this->data['defaultStage']->fields, TRUE);
            $_POST['stageId'] = $this->data['defaultStage']->id;
        } else {
            $_POST = $this->projectsModel->getProjectVariables($projectId);
        }

        $this->performRunScript($projectId);
    }

    public function performRunScript($projectId)
    {
        $this->data['project'] = $this->projectsModel->get($projectId);

        // TODO. Move to models
        $scriptName = $this->data['project']->name;
        $jmeterProjectName = $scriptName;

        $reportsDir = APP_DIR . DIRECTORY_SEPARATOR . 'jmeter-src' . DIRECTORY_SEPARATOR . 'results';
        $currentReportDir = $reportsDir . DIRECTORY_SEPARATOR . $scriptName;
        $reportName = 'report_' . date('Y_m_d__h_i_s') . '.jtl';
        $reportPath = $currentReportDir . DIRECTORY_SEPARATOR . $reportName;

        if (!file_exists($reportsDir)) {
            if (!mkdir($reportsDir, 0773, TRUE)) {
                echo 'Error on directory creation: ' . $reportsDir;
                die;
            }
        }

        if (!file_exists($currentReportDir)) {
            if (!mkdir($currentReportDir, 0773, TRUE)) {
                echo 'Error on directory creation: ' . $currentReportDir;
                die;
            }
        }

        // Get run parameters
        $userParams = '';
        $stageId = NULL;
        foreach ($_POST as $key => $value) {
            if ($key != 'stageId') {
                $userParams .= "-J{$key}=\"{$value}\" ";
            } else {
                if ($value != '0') {
                    $stageId = $value;
                }
            }
        }

        $testSrcPath = APP_DIR . DIRECTORY_SEPARATOR . "jmeter-src" . DIRECTORY_SEPARATOR . "scripts" . DIRECTORY_SEPARATOR . "{$jmeterProjectName}";
        $cmd =
            JMETER_PATH . " " .
            "-n -t {$testSrcPath} " .
            "-l {$reportPath} " .
            "{$userParams} " .
            "-Jjmeter.save.saveservice.output_format=xml " .
            "-Jjmeter.save.saveservice.data_type=true " .
            "-Jjmeter.save.saveservice.label=true " .
            "-Jjmeter.save.saveservice.response_code=true " .
            "-Jjmeter.save.saveservice.response_data=true " .
            "-Jjmeter.save.saveservice.response_message=true " .
            "-Jjmeter.save.saveservice.successful=true " .
            "-Jjmeter.save.saveservice.thread_name=true " .
            "-Jjmeter.save.saveservice.assertion_results_failure_message=true " .
            "-Jjmeter.save.saveservice.responseHeaders=true " .
            "-Jjmeter.save.saveservice.time=true " .
            "-Jjmeter.save.saveservice.requestHeaders=true " .
            "-Jjmeter.save.saveservice.samplerData=true " .
            "-Jjmeter.save.saveservice.url=true ";

        $exec = new \AutoQa\Libs\Executor();
        $exec->Executor();
        $execOutput = $exec->run2($cmd);

        $projects = $this->projectsModel->getByName($scriptName);

        $run = R::dispense('runs');
        $run->pId = $projects->id;
        $run->name = $reportName;
        $run->stageId = $stageId;
        $run->dCreate = date('Y-m-d H:i:s');
        $run->dUpdate = date('Y-m-d H:i:s');
        $runId = R::store($run);

        $runsData = R::xdispense('runs_data');
        $runsData->rId = $runId;
        $runsData->exec = json_encode($_POST, JSON_NUMERIC_CHECK);
        $runsData->consoleOutput = $execOutput;
        $runsData->dCreate = date('Y-m-d H:i:s');
        $runsData->dUpdate = date('Y-m-d H:i:s');
        $runsDataId = R::store($runsData);

        // Save Summary report
        require_once APP_DIR . '/libs/jmeter/reports/JmeterReport.php';

        $jmeterReport = new JmeterReport();
        $jmeterReport->parseReport($reportPath);
        $this->data['summaryReport'] = $jmeterReport->getSummaryReport();

        $runStatus = 1;
        foreach ($this->data['summaryReport'] as $label => $reportRow) {
            if ($label != 'GraphResult') {
                $runsSummaryReports = R::xdispense('runs_summary_reports');
                $runsSummaryReports->rId = $runId;
                $runsSummaryReports->label = $label;
                $runsSummaryReports->status = $reportRow['errorPercent'] == 0 ? 1 : 2;
                $runsSummaryReports->samples = $reportRow['samples'];
                $runsSummaryReports->averageTime = $reportRow['averageResponse'];
                $runsSummaryReports->maxTime = $reportRow['maxResponse'];
                $runsSummaryReports->minTime = $reportRow['minResponse'];
                $runsSummaryReports->errorCount = $reportRow['errorCount'];
                $runsSummaryReports->errorPercent = $reportRow['errorPercent'];
                $runsSummaryReports->dCreate = date('Y-m-d H:i:s');
                $runsSummaryReports->dUpdate = date('Y-m-d H:i:s');

                if ($runsSummaryReports->status == 2) {
                    $runStatus = 2;
                }

                $runsSummaryReportsId = R::store($runsSummaryReports);

                foreach ($reportRow['requests'] as $request) {
                    $runsSamples = R::xdispense('runs_samples');
                    $runsSamples->summReportId = $runsSummaryReportsId;
                    $runsSamples->url = $request->getUrl();
                    $runsSamples->method = $request->getMethod();
                    $runsSamples->responseTime = $request->getResponseTime();
                    $runsSamples->status = $request->getStatus() ? 1 : 2;
                    $runsSamples->requestBody = $request->getQueryString();
                    $runsSamples->requestHeaders = $request->getRequestHeaders();
                    $runsSamples->responseCode = $request->getStatusCode();
                    $runsSamples->responseBody = $request->getResponseData();
                    $runsSamples->responseHeaders = $request->getResponseHeaders();

                    $errorMessage = '';
                    foreach ($request->getFailureMessages() as $failureMessage) {
                        $errorMessage .= $failureMessage . "\n\n";
                    }
                    $runsSamples->errorMessage = $errorMessage;
                    $runsSamples->dCreate = date('Y-m-d H:i:s');
                    $runsSamples->dUpdate = date('Y-m-d H:i:s');

                    $runsSamplesId = R::store($runsSamples);
                }
            }
        }

        $run->status = $runStatus;
        $runId = R::store($run);

        header('Location: ' . BASE_PATH . '/projects/' . $projectId . '/reports/' . $runId);
        exit;
    }

    public function viewReport($projectId, $reportId)
    {
        $this->data['project'] = $this->projectsModel->get($projectId);
        $this->data['application'] = $this->applicationsModel->get($this->data['project']->appId);

        $this->data['report'] = $this->runsModel->get($reportId);
        $this->data['reportData'] = $this->runsModel->getReportData($reportId);

        $this->data['stage'] = $this->stagesModel->get($this->data['report']->stageId);

        $this->data['summaryReport'] = $this->reportsModel->getSummaryReport($reportId);
        $this->data['samplesData'] = $this->reportsModel->getSamplesReport($reportId);

        return $this->render('scripts/view-report.phtml');
    }
}