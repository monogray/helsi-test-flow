<?php

namespace AutoQa\Controllers;

use AutoQa\Models\ApplicationsModel;
use AutoQa\Models\ExecutorsModel;
use AutoQa\Models\ExecutorRunsModel;

class RobotController extends BaseAppController
{
    /**
     * @var ApplicationsModel
     */
    protected $applicationsModel = NULL;

    /**
     * @var ExecutorRunsModel
     */
    protected $executorRunsModel = NULL;

    /**
     * @var ExecutorsModel
     */
    protected $executorModel = NULL;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);

        $this->applicationsModel = new ApplicationsModel();
        $this->executorRunsModel = new ExecutorRunsModel();
        $this->executorModel = new ExecutorsModel();
    }

    public function index($appId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['runs'] = $this->executorRunsModel->getByApplicationId($appId);
        $this->data['executorsData'] = $this->executorRunsModel->getDataByApplicationId($appId);
        $this->data['executors'] = $this->executorModel->getByApplicationId($appId);

        return $this->render('robot/view.php');
    }

    public function dashboard($appId)
    {
        $this->data['application'] = $this->applicationsModel->get($appId);
        $this->data['executorsData'] = $this->executorRunsModel->getDasboardDataByApplicationId($appId);

        return $this->render('robot/dashboard.php');
    }
}