<?php

namespace AutoQa\Controllers;

use AutoQa\Models\UsersModel;
use Stringy\Stringy as S;

class MisController extends BaseAppController
{
    /**
     * @var UsersModel
     */
    protected $usersModel = NULL;

    public function __construct(\Slim\App $app)
    {
        $this->isPublic = TRUE;
        parent::__construct($app);

        $this->usersModel = new UsersModel();
    }

    public function index()
    {
        ini_set('max_execution_time', 300);
        echo ini_get('max_execution_time') . '<br>';
        ini_set('memory_limit', '1100M');
        echo ini_get('memory_limit') . '<br>';
        echo '------------------------------<br>';

        $resultPath = __DIR__ . '/../../jmeter-src/results/result.tsv';
        file_put_contents($resultPath, "");

        //$inputFileName = __DIR__ . '/../../jmeter-src/results/result_dubl.tsv';
        //file_put_contents($inputFileName, "");

        // ------------------------------

        for ($i = 1; $i <= 8; $i++) {
            echo "Лист #" . ($i + 1) . '<br>';
            $inputFileName = __DIR__ . '/../../public/Все 00' . $i . '.tsv';
            file_put_contents($resultPath, $this->loadFile($inputFileName), FILE_APPEND);

            echo '<br>';
        }

        echo '------------------------------<br>';

        $inputFileName = __DIR__ . '/../../jmeter-src/results/result.tsv';
        file_put_contents($resultPath, $this->loadFile($inputFileName));

        echo '------------------------------<br>';

        $this->anayzePnones($inputFileName);
    }

    public function anayzePnones($inputFileName)
    {
        $doc = file_get_contents($inputFileName);
        $data = explode("\n", $doc);
        unset($doc);

        $rows = array();
        $i = 1;
        foreach ($data as $dat) {
            $rows[$i] = explode("\t", $dat);
            $i++;
        }
        unset($data);

        $fioId = array();
        //$rowsFin = array();

        foreach ($rows as $datId => $dat) {

            $curId = isset($dat[10]) ? S::create($dat[10])->toLowerCase()->trim() . '' : '';

            if (!isset($fioId[$curId])) {

                $fioId[$curId] = array(
                    'id'   => $datId,
                    'dIds' => array(),
                );

            } else {
                // Duplicate find
                array_push($fioId[$curId]['dIds'], $datId);

                if (isset($rows[$datId][10]) && isset($rows[$datId][11]) && S::create($dat[11]) . '' == '') {
                    $rows[$datId][11] = $rows[$datId][10];
                    $rows[$datId][10] = '';
                }
            }
        }

        $result = '';
        foreach ($rows as $datId => $dat) {
            $result .= implode("\t", $rows[$datId]) . "\n";
            //$result .= implode("\t", $rowsFin[$dat['id']]) . "\n";
        }

        $inputFileName = __DIR__ . '/../../jmeter-src/results/result_fin.tsv';
        file_put_contents($inputFileName, $result);
    }

    public function loadFile($inputFileName)
    {
        $doc = file_get_contents($inputFileName);
        $data = explode("\n", $doc);
        unset($doc);

        $rows = array();
        $i = 1;
        foreach ($data as $dat) {
            $rows[$i] = explode("\t", $dat);

            //if ($i == 1) {
                //echo 'row len: ' . count($rows[$i]) . '<br>';
            //}

            $i++;
        }
        unset($data);

        echo 'Src count: ' . count($rows) . '<br>';

        $fioId = array();
        $rowsFin = array();

        //$duplicats = '';
        foreach ($rows as $datId => $dat) {
            $i = isset($dat[0]) ? S::create($dat[0])->toLowerCase()->trim() : '';
            $f = isset($dat[1]) ? S::create($dat[1])->toLowerCase()->trim() : '';
            $o = isset($dat[2]) ? S::create($dat[2])->toLowerCase()->trim() : '';

            $d = isset($dat[4]) ? S::create($dat[4])->toLowerCase()->trim() : '';

            $curId = $i . ' ' . $f . ' ' . $o . ' ' . $d;

            if (!isset($fioId[$curId])) {
                $fioId[$curId] = array(
                    'id'   => $datId,
                    'dIds' => array(),
                );

                $rowsFin[$datId] = $dat;
            } else {
                // Duplicate find
                array_push($fioId[$curId]['dIds'], $datId);

                $dddtId = $fioId[$curId]['id'];

                $j = 0;
                foreach ($dat as $dd) {

                    if (
                        S::create($dd)->toLowerCase()->trim() . '' != '' &&
                        S::create($rowsFin[$dddtId][$j])->toLowerCase()->trim() . '' == ''
                    ) {
                        $rowsFin[$dddtId][$j] = $dd;
                    }

                    /*if (
                        S::create($dd)->toLowerCase()->trim() . '' != '' &&
                        S::create($rowsFin[$dddtId][$j])->toLowerCase()->trim() . '' != '' &&
                        S::create($dd)->toLowerCase()->trim() . '' != S::create($rowsFin[$dddtId][$j])->toLowerCase()->trim() . ''
                    ) {
                        //echo $curId . '<br/>';
                        $duplicats .= "1\t" . implode("\t", $dat) . "\n";
                        $duplicats .= "0\t" . implode("\t", $rowsFin[$dddtId]) . "\n";
                        $duplicats .= "\n";
                    }*/

                    $j++;
                }

            }
        }

        //$inputFileName = __DIR__ . '/../../jmeter-src/results/result_dubl.tsv';
        //file_put_contents($inputFileName, $duplicats, FILE_APPEND);

        unset($duplicats);
        unset($rows);

        echo 'Unic count: ' . count($fioId) . '<br>';

        $result = '';
        foreach ($fioId as $datId => $dat) {
            //$result .= implode("\t", $rows[$dat['id']]) . "\n";
            $result .= implode("\t", $rowsFin[$dat['id']]) . "\n";
        }

        return $result;
    }







    public function loadXlsxFile($inputFileName)
    {
        // Read your Excel workbook
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        // Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        //$highestRow = $sheet->getHighestRow();
        $highestRow = 10;
        //$highestColumn = $sheet->getHighestColumn();
        $highestColumn = 'D';

        // Loop through each row of the worksheet in turn

        $data = array();
        for ($row = 2; $row <= $highestRow; $row++) {
            // Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            array_push($data, $rowData);
        }

        return $data;
    }

    public function index3()
    {
        $adrrId = 0;

        //$inputFileName = __DIR__ . '/../../public/л7.xlsx';
        $inputFileName = __DIR__ . '/../../public/даті.xlsx';

        // Read your Excel workbook
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        // Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        // Loop through each row of the worksheet in turn

        $this->data['doctors'] = array();
        for ($row = 1; $row <= $highestRow; $row++) {
            // Read a row of data into an array

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);
            // Insert row data array into your database of choice here

            array_push($this->data['doctors'], $rowData);
        }

        foreach ($this->data['doctors'] as &$doctor) {

            $pieces = explode(".", $doctor[0][$adrrId]);

            if (count($pieces) == 3) {
                echo $pieces[2] . '.' . $pieces[1] . '.' . $pieces[0] . '<br>';
            } else {
                echo $doctor[0][$adrrId] . '<br>';
            }

            //echo $doctor[0][$birdthNameCount].'<br/>';

            /*$arr = S::create($doctor[0][$adrrId])->split(' ');
            $street = isset($arr[0]) ? $arr[0] : '';

            unset($arr[0]);

            $dom = '';
            if (count($arr) > 0) {
                $dom = implode(' ', $arr);
            }

            echo $dom . '<br/>';
            //echo $street . ' | ' . $dom . '<br/>';*/

            //echo $doctor[0][$adrrId] . '<br/>';

            /*if(S::create($doctor[0][$adrrId])->length() == 7) {
                echo '044' . S::create($doctor[0][$adrrId]) . '<br/>';
            } else {
                echo '<br/>';
            }*/

            /*if (S::create($doctor[0][$adrrId])->length() == 11 && S::create($doctor[0][$adrrId])->startsWith(8)) {
                echo substr(S::create($doctor[0][$adrrId]), 1) . '<br/>';
            } else if(S::create($doctor[0][$adrrId])->length() == 7) {
                echo '<br/>';
            } else if(S::create($doctor[0][$adrrId])->length() == 9 && (S::create($doctor[0][$adrrId])->startsWith(5) || S::create($doctor[0][$adrrId])->startsWith(6) || S::create($doctor[0][$adrrId])->startsWith(9))) {
                echo '0' . $doctor[0][$adrrId] . '<br/>';
            } else if(S::create($doctor[0][$adrrId])->length() <= 6 ) {
                echo '<br/>';
            } else {
                echo $doctor[0][$adrrId] . '<br/>';
            }*/

            /*if (S::create($doctor[0][$adrrId])->length() == 8 && S::create($doctor[0][$adrrId])->startsWith(0)) {
                echo '<br/>';
            } else {
                echo $doctor[0][$adrrId] . '<br/>';
            }*/

            /*if(S::create($doctor[0][$adrrId])->endsWith('вич')) {
                echo 'ч<br/>';
            } else if(S::create($doctor[0][$adrrId])->endsWith('вна')) {
                echo 'ж<br/>';
            } else {
                echo '<br/>';
            }*/
        }
    }

    public function index2()
    {
        $specNameCount = 19;
        $specIdCount = 20;
        $positionNameCount = 17;
        $positionIdCount = 18;
        $birdthNameCount = 15;
        $phone1IdCount = 22;
        $phone2IdCount = 23;


        $inputFileName = __DIR__ . '/../../public/doctors_.xlsx';

        // Read your Excel workbook
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        // Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        // Loop through each row of the worksheet in turn

        $this->data['doctors'] = array();
        for ($row = 2; $row <= $highestRow; $row++) {
            // Read a row of data into an array

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);
            // Insert row data array into your database of choice here

            array_push($this->data['doctors'], $rowData);
        }

        unset($sheet);
        unset($highestRow);
        unset($highestColumn);

        // Get specialities
        $specialities = file_get_contents(__DIR__ . '/../../public/profession2');
        $specialities = json_decode($specialities, TRUE);

        // Get specialities
        $professions = file_get_contents(__DIR__ . '/../../public/profession.js');
        $professions = json_decode($professions, TRUE);

        foreach ($this->data['doctors'] as &$doctor) {
            //echo $doctor[0][$birdthNameCount].'<br/>';

            $doctor[0][$birdthNameCount] = S::create($doctor[0][$birdthNameCount])->replace('/', '.');
            $bstr = explode('.', $doctor[0][$birdthNameCount]);
            if (isset($bstr[0]) && strlen($bstr[0]) == 1) {
                $bstr[0] = '0' . $bstr[0];
            }
            if (isset($bstr[1]) && strlen($bstr[1]) == 1) {
                $bstr[1] = '0' . $bstr[1];
            }
            if (isset($bstr[1])) {
                $doctor[0][$birdthNameCount] = $bstr[0] . '.' . $bstr[1] . '.' . $bstr[2];
            }

            $p1 = $doctor[0][$phone1IdCount];
            $p2 = $doctor[0][$phone2IdCount];

            /*if ($doctor[0][$phone1IdCount] && $doctor[0][$phone1IdCount] != '') {
                $doctor[0][$phone1IdCount] = S::create($doctor[0][$phone1IdCount])->replace('(', '')
                    ->replace(')', '')
                    ->replace(' ', '')
                    ->replace('-', '');

                if (S::create($doctor[0][$phone1IdCount])->length() == 7) {
                    $doctor[0][$phone1IdCount] = '044' . $doctor[0][$phone1IdCount];
                } else if (!S::create($doctor[0][$phone1IdCount])->startsWith('0')) {
                    $doctor[0][$phone1IdCount] = '0' . $doctor[0][$phone1IdCount];
                }
            }*/
            if ($doctor[0][$phone1IdCount] && $doctor[0][$phone1IdCount] != '') {
                $doctor[0][$phone1IdCount] = S::create($doctor[0][$phone1IdCount])->replace('(', '')
                    ->replace(')', '')
                    ->replace(' ', '')
                    ->replace('-', '');

                if (S::create($doctor[0][$phone1IdCount])->length() == 8 && S::create($doctor[0][$phone1IdCount])->startsWith('0')) {
                    $doctor[0][$phone1IdCount] = substr($doctor[0][$phone1IdCount], 1);
                    $doctor[0][$phone1IdCount] = '044' . $doctor[0][$phone1IdCount];
                }
            }

            /*if ($doctor[0][$phone2IdCount] && $doctor[0][$phone2IdCount] != '') {
                $doctor[0][$phone2IdCount] = S::create($doctor[0][$phone2IdCount])->replace('(', '')
                    ->replace(')', '')
                    ->replace(' ', '')
                    ->replace('-', '');

                if (S::create($doctor[0][$phone2IdCount])->length() == 7) {
                    $doctor[0][$phone2IdCount] = '044' . $doctor[0][$phone2IdCount];
                } else if (!S::create($doctor[0][$phone2IdCount])->startsWith('0')) {
                    $doctor[0][$phone2IdCount] = '0' . $doctor[0][$phone2IdCount];
                }
            }*/
            if ($doctor[0][$phone2IdCount] && $doctor[0][$phone2IdCount] != '') {
                $doctor[0][$phone2IdCount] = S::create($doctor[0][$phone2IdCount])->replace('(', '')
                    ->replace(')', '')
                    ->replace(' ', '')
                    ->replace('-', '');

                if (S::create($doctor[0][$phone2IdCount])->length() == 8 && S::create($doctor[0][$phone2IdCount])->startsWith('0')) {
                    $doctor[0][$phone2IdCount] = substr($doctor[0][$phone2IdCount], 1);
                    $doctor[0][$phone2IdCount] = '044' . $doctor[0][$phone2IdCount];
                }
            }

            /*if (!(strlen($doctor[0][$phone1IdCount]) == 10 || strlen($doctor[0][$phone2IdCount]) == 10 || strlen($doctor[0][$phone2IdCount]) == 8 || strlen($doctor[0][$phone2IdCount]) == 8) && strlen($doctor[0][$phone1IdCount]) != 0 && strlen($doctor[0][$phone2IdCount]) != 0) {
                echo $doctor[0][8] . ' ' . $doctor[0][9] . ' ' . $doctor[0][10] . '; ' . $p1 . '; ' . $p2 . '<br/>';
            }*/


            if ((strlen($doctor[0][$phone2IdCount]) != 0 && strlen($doctor[0][$phone2IdCount]) != 10) || (strlen($doctor[0][$phone1IdCount]) != 0 && strlen($doctor[0][$phone1IdCount]) != 10)) {
                echo $doctor[0][1] . '. ' . $doctor[0][9] . ' ' . $doctor[0][10] . ' ' . $doctor[0][11] . '<br/>';// . '; ' . $p1 . '; ' . $p2 . '<br/>';
            }

            $specialityId = $this->getSpeciality($specialities, $doctor[0][$specNameCount]);
            $doctor[0][$specIdCount] = $specialityId;

            $profId = $this->getProfession($professions, $doctor[0][$positionNameCount]);
            $doctor[0][$positionIdCount] = $profId;
        }


        $objPHPExcel = new \PHPExcel();

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);

        $i = 1;
        foreach ($this->data['doctors'] as $doctor) {
            $j = 0;

            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $i, $doctor[0][$j++]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $i, $doctor[0][$j++]);

            $i++;
        }


        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Result');

        // Save Excel 2007 file
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(__DIR__ . '/../../jmeter-src/results/result.xlsx');

        // $this->apiRender();
    }

    public function getSpeciality($specialities, $data)
    {
        $result = NULL;
        foreach ($specialities as $speciality) {
            if (S::create($speciality['name'])->toLowerCase() == S::create($data)->toLowerCase()) {
                $result = $speciality['specialityId'];

                return $result;
            }
        }

        return $result;
    }

    public function getProfession($professions, $data)
    {
        $result = NULL;
        $dat = S::create($data)->toLowerCase()->trim()->replace(' - ', '-')->replace(' -', '-')->replace('  ', '-')->replace(' ', '-');

        foreach ($professions as $profession) {
            $src = S::create($profession['name'])->toLowerCase()->trim()->replace(' - ', '-')->replace(' -', '-')->replace('  ', '-')->replace(' ', '-');

            if ($src == $dat) {
                $result = $profession['professionId'];

                return $result;
            }
        }

        return $result;
    }
}