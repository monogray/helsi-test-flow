<?php

namespace AutoQa\Controllers;

use AutoQa\Models\UsersModel;
use Mono\Fixtures\MonoFixtures;
use Stringy\Stringy as S;

class FixturesController extends BaseAppController
{
    /**
     * @var UsersModel
     */
    protected $usersModel = NULL;

    public function __construct(\Slim\App $app)
    {
        $this->isPublic = TRUE;
        parent::__construct($app);

        $this->usersModel = new UsersModel();

        // Load libs
        require_once APP_DIR . '/libs/MonoFixtures/MonoFixtures.php';
    }

    // http://localhost/~user/helsi-test-flow/fixtures/generate/collection?data=[test=name,lastname]&count=5
    public function generateCollection()
    {
        if (isset($_GET['data']) &&  $_GET['data'] != '') {
            $dataSet = S::create($_GET['data'])->replace('[', '')->replace(']', '')->replace(' ', '');
            $dataSet = explode(",", $dataSet);

            $count = 1;
            if (isset($_GET['count']) && $_GET['count'] != '') {
                $count = (int)$_GET['count'];
                if ($count <= 0) {
                    $count = 1;
                }
            }

            $fixturesModule = new MonoFixtures();
            $this->data = $fixturesModule->generateData(
                array(
                    'dataSet' => $dataSet,
                    'count' => $count
                )
            );

            $this->apiRender();
        }
    }
}