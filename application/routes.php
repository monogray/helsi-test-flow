<?php
use AutoQa\Controllers\CoreController as CoreController;

// -------------------------------------------------------------------------------------------------------------
// App Routs
// -------------------------------------------------------------------------------------------------------------

// ---------------------
// Init controllers
// ---------------------

// App controllers
$indexPageController = new CoreController($app, AutoQa\Portal\Controllers\IndexPageController::class);
$loginController = new CoreController($app, AutoQa\Controllers\LoginController::class);
$scriptsController = new CoreController($app, AutoQa\Controllers\ScriptsController::class);
$applicationsController = new CoreController($app, AutoQa\Controllers\ApplicationsController::class);
$testProjectsController = new CoreController($app, AutoQa\Controllers\TestProjectsController::class);
$projectsController = new CoreController($app, AutoQa\Controllers\ProjectsController::class);
$scriptsPublicController = new CoreController($app, AutoQa\Controllers\ScriptsPublicController::class);
$stagesController = new CoreController($app, AutoQa\Controllers\StagesController::class);
$fixturesController = new CoreController($app, AutoQa\Controllers\FixturesController::class);
$robotController = new CoreController($app, AutoQa\Controllers\RobotController::class);
$executorsController = new CoreController($app, AutoQa\Controllers\ExecutorsController::class);
$qaDashboardController = new CoreController($app, AutoQa\Controllers\QaDashboardController::class);

// ---------------------
// Define routs
// ---------------------

// Index page
$app->group('', function () use ($app, $indexPageController) {
    $app->get('/', $indexPageController('index'));
});

// TODO Temp
/*$misController = new CoreController($app, AutoQa\Controllers\MisController::class);
$app->group('/mis', function () use ($app, $misController) {
    $app->get('/xls', $misController('index'));
});*/

// Login
$app->group('/auth', function () use ($app, $loginController) {
    $app->get('/login', $loginController('login'));
    $app->post('/login', $loginController('performLogin'));
    $app->get('/logout', $loginController('logout'));
    $app->get('/sign-up', $loginController('signUp'));
    $app->post('/sign-up', $loginController('performSignUp'));
    $app->get('/verify-email', $loginController('verifyEmail'));
});

// Base routs
$app->group('', function () use ($app, $scriptsController) {
    $app->get('/dashboard', $scriptsController('index'));
});

// Applications
$app->group('/applications', function () use ($app, $applicationsController, $stagesController, $robotController, $executorsController) {
    $app->get('/{appId:[0-9]+}', $applicationsController('view'));
    $app->get('/{appId:[0-9]+}/edit', $applicationsController('editView'));
    $app->post('/{appId:[0-9]+}/edit', $applicationsController('update'));
    $app->get('/{appId:[0-9]+}/map', $applicationsController('viewMap'));

    // Stages
    $app->get('/{appId:[0-9]+}/stages', $applicationsController('stagesView'));
    $app->get('/{appId:[0-9]+}/stages/create', $applicationsController('createStage'));
    $app->get('/{appId:[0-9]+}/stages/{stageIdId:[0-9]+}/edit', $applicationsController('editStage'));
    $app->post('/{appId:[0-9]+}/stages/{stageId:[0-9]+}/update', $stagesController('applicationEdit'));
    $app->get('/{appId:[0-9]+}/stages/{stageId:[0-9]+}/run', $applicationsController('performRun'));

    // Runs
    $app->get('/{appId:[0-9]+}/runs/{runId:[0-9]+}', $applicationsController('runView'));
    $app->get('/{appId:[0-9]+}/runs/{runId:[0-9]+}/reports/{reportId:[0-9]+}', $applicationsController('reportView'));

    // Robot
    $app->get('/{appId:[0-9]+}/robot', $robotController('index'));
    $app->get('/{appId:[0-9]+}/robot/dashboard', $robotController('dashboard'));
    $app->get('/{appId:[0-9]+}/robot/executors', $executorsController('viewList'));
    $app->get('/{appId:[0-9]+}/robot/executors/create', $executorsController('create'));
    $app->get('/{appId:[0-9]+}/robot/executors/{executorId:[0-9]+}/edit', $executorsController('viewEdit'));
    $app->post('/{appId:[0-9]+}/robot/executors/{executorId:[0-9]+}/update', $executorsController('update'));
    $app->get('/{appId:[0-9]+}/robot/executors/{executorId:[0-9]+}/run', $executorsController('viewExecutorRun'));
    $app->get('/{appId:[0-9]+}/robot/executors/{executorId:[0-9]+}/performRun', $executorsController('performRun'));
    $app->get('/{appId:[0-9]+}/robot/executors/{executorId:[0-9]+}/delete', $executorsController('delete'));
    $app->get('/{appId:[0-9]+}/robot/runs/{runId:[0-9]+}', $executorsController('viewRun'));
});

$app->group('/applications/{appId:[0-9]+}/test-projects', function () use ($app, $testProjectsController) {
    $app->get('', $testProjectsController('view'));
});

// Projects
$app->group('/projects', function () use ($app, $scriptsController, $scriptsPublicController, $projectsController) {
    // Manage projects
    $app->get('/{projectId:[0-9]+}', $scriptsController('view'));
    $app->get('/{projectId:[0-9]+}/edit', $projectsController('editView'));
    $app->post('/{projectId:[0-9]+}/edit', $projectsController('update'));

    // Dashboard
    $app->get('/{projectId:[0-9]+}/dashboard', $projectsController('viewDashboard'));

    // Runs
    $app->get('/{projectId:[0-9]+}/run', $scriptsController('runScript'));
    $app->get('/{projectId:[0-9]+}/perform-run', $scriptsController('performRunScriptNow'));
    $app->post('/{projectId:[0-9]+}/perform-run', $scriptsController('performRunScript'));

    // Results
    $app->get('/{projectId:[0-9]+}/reports/{reportId:[0-9]+}', $scriptsController('viewReport'));
    $app->get('/{projectId:[0-9]+}/reports/last/{key}', $scriptsPublicController('lastReport'));
});

// Project's stages
$app->group('/projects/{projectId:[0-9]+}/stages', function () use ($app, $stagesController) {
    $app->get('', $stagesController('viewListFromProject'));
    $app->get('/add', $stagesController('add'));
    $app->get('/{stageId:[0-9]+}', $stagesController('editView'));
    $app->post('/{stageId:[0-9]+}/update', $stagesController('edit'));
    $app->post('/{stageId:[0-9]+}/set-default', $stagesController('updateDefault'));
});

$app->group('/qa-dashboard', function () use ($app, $qaDashboardController) {
    $app->get('', $qaDashboardController('index'));
});

$app->group('/fixtures', function () use ($app, $fixturesController) {
    // $app->get('/generate/collection', $fixturesController('generateCollection'));
});

// -------------------------------------------------------------------------------------------------------------
// API Routs
// -------------------------------------------------------------------------------------------------------------

// ---------------------
// Init controllers
// ---------------------

// App controllers
$apiExecutorsController = new CoreController($app, AutoQa\Controllers\Api\ExecutorsController::class);
$apiProjectsController = new CoreController($app, AutoQa\Controllers\Api\ProjectsController::class);

// ---------------------
// Define routs
// ---------------------

// Projects
$app->group(API_PREFIX . '/projects/{appId:[0-9]+}', function () use ($app, $apiProjectsController) {
    $app->get('/perform-run', $apiProjectsController('runProject'));
});

// Robot Methods
$app->group(API_PREFIX . '/applications/{appId:[0-9]+}/robot/runs', function () use ($app, $apiExecutorsController) {
    $app->get('/{runId:[0-9]+}/get-status', $apiExecutorsController('getStatus'));
});