<?php
return [
    'settings' => [
        'displayErrorDetails' => TRUE, // set to false in production

        // Renderer settings
        'renderer'            => [
            'template_path' => __DIR__ . '/views/',
        ],

        // Monolog settings
        'logger'              => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],
    ],
];
